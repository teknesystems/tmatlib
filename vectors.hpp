// Vector data structures, vector spaces and vector operations.


// Header guard. Skips over entire header if already included
#ifndef _T_VEC_INCLUDED_
#define _T_VEC_INCLUDED_

// Include statement block
#include <iostream> // Mainly used for scaffolding purposes as of now
#include <vector> // Mainly used for data storage and assignment.
#include <deque>
#include <valarray> // Mainly used for fast vector operations, especially on simple vectors
#include <sstream> // For certain utility functions such as vector/polynomial/matrix printers
#include <string> // Similar uses to above
#include <cmath> // For computation, such as evaluating polynomial vectors, etc.
#include <bitset>
#include <limits>
#include <numeric>
#include <algorithm>
#include "symbolics.hpp" // Symbolics library
#include "utility.hpp" // Utilities library

// Operations
#include "vector_ops.hpp"
#include "tensor.hpp"
#include "data.hpp"
#include "printers.hpp"

// Scaffolding definitions

// Enable all scaffolding by defining this:
#ifdef _T_VEC_ENABLE_ALL_SCAFFOLDING_
#define _T_VEC_RREF_SCAFFOLDING_
#define _T_VEC_SYMB_SCAFFOLDING_
#define _T_VEC_DET_SCAFFOLDING_
#define _T_VEC_OPS_SCAFFOLDING_
#endif

// Individually enabled scaffolding:

//#define _T_VEC_RREF_SCAFFOLDING_
//#define _T_VEC_SYMB_SCAFFOLDING_
//#define _T_VEC_DET_SCAFFOLDING_
//#define _T_VEC_OPS_SCAFFOLDING_

// Type definitions

// Defines the set of tensors of rank (vectors, covectors), but also pseudovectors such as vectors obtained through the cross product, hence tensorLIKE
class tensorLikeSet: public Superset
{
public:
  dimi vectors;
  dimi covectors;
  //tensorLikeSet(dimi index1, dimi index2);
  tensorLikeSet() {}

  // Value setter function
  void set(dimi input1, dimi input2);
};

//NOTE: This is what is called, in this library, the SYMBOLIC SCALAR FIELD. Something with a symbolic scalar field of Polynomials, for example, has polynomial multiplication defined, but also IS either
// a polynomial or BIGGER than one (like, say, the vectorspace of differentiable functions). This is to make coding a little easier both in terms of data storage and in defining special cases like
// polynomial multiplication
//NOTE: Whilst matrices can be multiplied by matrices, the vectorspace of matrices does not have matrices for a scalar field, as matrix multiplication is defined in a different way.
class scalarField: public tensorLikeSet, public field // Notice, scalar fields are themselves tensorlikesets, like polynomials (1,0), real numbers (0,0) and matrices of complex numbers (1,1)
{
  Set *scalar_set;
public:
  scalarField() {}
  scalarField(Set *input); 
};

// Predefined, useful scalar fields.
scalarField Rn; // Real numbers
scalarField Cn; // Complex numbers
scalarField Pn; // Positive integer exponent polynomials
scalarField Poly; // Infinite dimensional polynomials (i.e. with any power, including negative powers)
scalarField Zn; // Integers
scalarField Qn; // Rational numbers
scalarField Dn; // Differentiable Functions (here defined as functions with differentiate() defined and potentially a member function derivative()
scalarField Arb; // Arbitrary scalar field. For example, a general matrix, which can hold anything and be multiplied by anything in its most general sense, like say, operators.

// Special scalar fields, used for specific forms of vector and error handling:
scalarField ALLOC_ERROR;

// The basic class for vectorspaces, taking into account both matrices and arbitrary tensors as well
class vectorSpace: public space
{
  std::bitset<8> properties; // Defined to take up the same space as 1 bool, but hold 8
  // The properties are as follows:
  // bit #1: is_lesser_tensor_vector, that is, is this vector actually a 1D array of tensors with a rank one lower (e.g. a matrix as an array of column vectors (1,0) or row vectors (0, 1) (vs. (1,1)))
  // bit #2: swap-point
  // If bit #1 is true, bit #2 determines whether VECTORS (false) or COVECTORS (true) is reduced in the resulting tensor
  // If bit #1 is false, bit #2 determines UNDEFINED (TODO: find use)
  // bit #3: pseudovector, that is, is this vector a REAL vector or a PSEUDOVECTOR (which can "flip" directions)
  // bit #4: negative, that is, was the vector multiplied by -1 (makes unsigned int vectors and such useful, and can potentially be used for stuff). OPTIONAL. Either multiply by -1 or flip this
  // bits #5, 6, 7, 8 currently have no use, and the use for bit #4 is tentative. TODO: find uses
public:
  tensorLikeSet rank;
  std::vector<dimi> dimensions;
  scalarField* scalar_field;
  // Value setters:
  //void setSpace(vectorSpaceSet* input_set);
  //void setSpace(vectorSpaceSet input_set);
  void setSpace(scalarField* scalar_field_used, dimi length);
  void setSpace(scalarField* scalar_field_used, dimi rows, dimi cols);
  //void setSpace(vectorSpaceSet* input_set, dimi* indexdimensions, dimi length);
  void setSpace(dimi realdim);
  //void setSpace(vectorSpace input);
  // Property get-set functions
  bool is_lesser_tensor_vector() {return properties[0];}
  void set_ltv() {properties.flip(0);};
  bool swap_point() {return properties[1];}
  bool pseudo() {return properties[2];}
  void pseudo(bool input) {properties[2] = input;}
  bool negative() {return properties[3];}
  void negate() {properties.flip(3);}
  // Rank-checking functions
  dimi covarrank();
  dimi contrarank();
  dimi getrank() {return (covarrank() + contrarank());}
  // Equality-checking functions
  bool operator==(vectorSpace input);
};

template <>
struct elementOf<&Vector_Space>: public elementOf<&Vectors>
{
  basis* basis_used; // The basis of an arbitrary vector
  // For advanced features such as special handling through virtual functions, inner/outer products beyond the Cartesian dot product and multiple indices, use a tensor!
  // Advantages of using vector: all you have to do is copy ONE pointer, basis->size() gives the true dimension (and if you are using an std::vector, the size() function of that gives the length of the array)
  /*const vectorSpace& getSpace()
  {
    
  };*/
};

// The code for an arbitrary element of a vectorspace
class vector: public elementOf<&Vector_Space>
{
};

// Defining tensorLikeSet value setter
void tensorLikeSet::set(dimi input1, dimi input2)
{
  vectors = input1;
  covectors = input2;
}

// Defining Set based scalar field constructor
scalarField::scalarField(Set *input)
{
  scalar_set = input;
}
// Vectorspace value-setters

void vectorSpace::setSpace(dimi realdim) // Quickly declare a 1D vector in Rn
{
  rank.set(1,0);
  dimensions.push_back(realdim);
  scalar_field = &Rn;
}

void vectorSpace::setSpace(scalarField *scalar_field_used, dimi length) // Quickly declare a 1D vector in an arbitrary space
{
  rank.set(1,0);
  dimensions.push_back(length);
  scalar_field = scalar_field_used;
}

void vectorSpace::setSpace(scalarField* scalar_field_used, dimi rows, dimi cols) // Quickly declare a matrix
{
  rank.set(1,1);
  dimensions.reserve(2);
  dimensions.push_back(cols);
  dimensions.push_back(rows);
  scalar_field = scalar_field_used;
}

// Vectorspace checking functions
dimi vectorSpace::covarrank() {return rank.vectors;}
dimi vectorSpace::contrarank() {return rank.covectors;}

// Vectorspace equality checker
bool vectorSpace::operator==(vectorSpace inputs)
{
  return true; //TEMPORARY!
}

// The code for an arbitrary tensor, the class mainly being for virtual inner/outer products.
class tensor: public vector // Because tensors are a type of vector. Vectors as a special case of tensors ((1,0) tensors to be precise) are to be dealt with below.
{
public:
  vectorSpace space;
  // Where |r| = vectors + covectors, an arbitrary vector can be copied by copying: 2 + |r| dimi types, 2 pointer and 1 bitset of length 8, or approximately 5 + |r| "operations"
  // If the vector is of length n, copying a vector's data is generally linear, MINUS pointer access, so if the vector is stored continuously, (5 or 6) + |r| + n operations are generally needed
  // Whether 5 or 6 "operations" are necessary depends on the type of vector being passed around, namely, an arbitrary vector or 1D vector (including 1D vectors with 2D bases)
  // If the vector is NOT stored continuously (is_lesser_tensor_vector = 1), the amount necessary is variable, but is generally greater than 6 + |r| + n, however, for CONST copying, it just takes
  // 6 + |r| + dimensions[swap-pointer] "operations", where swap_pointer is a value stored by general (but NOT 1D) vector structs, and adds one to the time necessary.
};

// The code for an arbitrary basis of a vectorspace
template <class vectorType>
class vectorBasis: public vector, public symbolicObject<vectorBasis<vectorType>>, public elementOf<&Vectors> // Because bases can be added, subtracted and scalar multiplied too!
{
public:
  std::deque<vectorType> elements;
  vectorSpace resultSpace;
  size_t dim() {return elements.size();}
  dimi dimension() {return dim();}
};

// Data-array functors:

// Resolve array to basis:
template <class containerType, class vectorContainerType>
auto resolveBasis(const containerType& A, const vectorContainerType& B) -> decltype((A[0]*(*(B.begin())))+(A[0]*(*(B.begin()))))
{
  decltype((A[0]*(*(B.begin())))+(A[0]*(*(B.begin())))) result = A[0] * B[0];
  for(dimi iter = 1; iter < std::min(A.size(),B.size()); iter++)
    {
      result+= A[iter] * B[iter];
    }
  return result;
}

// The code for non-arbitrary vectors
/*
Overview:
The basic vectors are realvector<elementType>. Their scalar fields are simply the union of their type, whatever will convert to it and real numbers. Generally real or complex strings of numbers.
They carry a basis* pointer, allowing them to represent other vector types, such as matrices.
PROS: they copy and operate easily, with simple operators, easy intercompatibility, easy basis checking
CONS: No support for advanced features such as special handling through virtual functions, inner/outer products beyond the Cartesian dot product, multiple indices (except the special case of a matrix,
having rank (1,1)) and custom defined vectorspaces (beyond handling classes and elementType implicit spaces, for example, a realvector<double> is Rn, a realvector<complex> is Cn, etc, a polynomial<double>
is Poly, etc.
The more advanced vector, avector (Array VECTOR) is an array vector with completely abstract (but (relatively) slow) and virtual addition and multiplication functions
This vector is actually an arbitrary tensor, and unlike a realvector, can be passed around as tensor* and generated by functions which output tensor.
rvector (Real-isomorphic array VECTOR) is a child class with much faster addition and scalar multiplication functions, using those built in to the elementType
tvector (Tuple VECTOR) is an alternative to Array VECTOR and holds arbitrary types as pointers. Slower, but more versatile, and useful for generalized matrices
Some specialized forms of vector are implemented as child classes of avector for utility purposes. Note that in most cases, these will work just fine as avectors, but require costly virtual functions
Some specialized forms of vector, on the other hand, are implemented as child classes of rvector. This is if their addition and scalar multiplication are elementwise, but functions not necessarily
defined for tensors (like, say, evaluating a function) should be defined as well
Some specialized forms of vector are implemented as child classes of realvector, and include functions to cast them to their proper avector/rvector/tvector/tensor* /etc. forms
This is generally for quick manipulation where much copying may be necessary, as well as much handling, may be necessary, and full tensor capabilities will probably be used very rarely.
Finally, valvector, vmatrix and vtensor are valarray based vectors, matrices and tensors respectively, and are even more specialized than rvector. Note these, along with rvector and realvector,
have a function pointer in their templates for addition, subtraction, scalar multiplication, inner products and outer products: if any of these are NULL, they become defined in the standard way for
individual elements or, in the case of inner and outer products, standard real-valued tensors/matrices/vectors, whereas if arbitrary treatment is desired, one should instead use the classes avalvector,
avmatrix and avtensor (and avector)
NOTE: another type of vector, ivector, a child class of smallvector, exists, and treats an iterator of length l as a vector. They do not handle the memory, only modify what is within their range
Be careful using these, they are mainly to make things like extracting a vector for quick numerical manipulations from an avector object or such.
Note that ivector is designed to simulate the functions inherent in valarrays, as well as to place valarray slices into the broader context of the program at the same time
*/

// Basic vector types:

// The simplevector family: realvector and its slightly more specialized derivative, smatrix, as well as ivector and its slightly more specialized derivative, pmatrix.
// The class arrayvector is also included here, which defines a few important vector objects without the tensor overhead (even if they ARE tensors), which they can be used to do in other parts of the    
// library and programs using the library as well. Those defined here include polynomial (for PIF, polynomial instruction format, strings).
class simplevector: public vector // realvectors can be CAST to tensors, but are not tensors themselves and do not generally implement tensor functionality
{
};

template <class elementType, class containerType = std::vector<elementType>>
struct arrayvector: public simplevector//, public symbolicObject<arrayvector<elementType,containerType>>
{
  containerType elements;
  // Constructors
  arrayvector() {}
  arrayvector(elementType *input, dimi length, basis* reference_basis = NULL)
  {
    try
      {
	reserve_container((this->elements),length);
	for(int iter = 0; iter < length; iter++)
	  {
	    push_to_container((this->elements),(input[iter]),iter);
	  }
      }
    catch (const std::exception& e)
      {
	(this->elements).clear();
	(this->elements).assign(((this->elements).size()), 0);
      }
    (this->basis_used) = reference_basis;
  }
  template<class inputType>
  arrayvector(const arrayvector<inputType> &input)
  {
    (this->elements) = input.elements; //TODO: fix this...
    (this->basis_used) = input.basis_used;
  }
  template<class inputType>
  arrayvector(std::initializer_list<inputType>& input, basis* reference_basis = NULL)
  {
    reserve_container(elements,input.size());
    for(auto value : input)
      {
	elements.push_back(value);
      }
    (this->basis_used) = reference_basis;
  }
  arrayvector(std::initializer_list<elementType>& input, basis* reference_basis = NULL)
  {
    elements = input;
    (this->basis_used) = reference_basis;
  }
  // Simple vector printer
  template<class streamType>
  streamType& print(streamType& printer)
  const {
    return arrayPrinter::print_to<vectorStyle>(printer,(this->elements),(this->elements).size());
  }
  std::string print()
  const {
    std::ostringstream printer;
    return (print(printer)).str();
  }
  void swapcontainer(containerType& input)
  {
    using std::swap;
    swap(elements,input);
  }
  void swapcontainer(containerType input)
  {
    using std::swap;
    swap(elements,input);
  }
  void assigncontainer(const containerType& input)
  {
    elements = input;
  }
  auto addElements() -> decltype((*(std::begin(elements)))+(*(std::begin(elements))))
  {
    decltype((*elements.begin())+(*elements.begin())) result = 0;
    for(auto value : elements)
      {
	result+=value;
      }
    return result;
  }
  size_t size()
  const {
    return elements.size();
  }
  dimi dimension()
  const {
    //return basis->dimension();
    return size();
  }
};

template<class ET, class CT>
std::ostream& operator<<(std::ostream& O, const arrayvector<ET,CT>& V)
{
  return V.print(O);
};

// Standard static vector template parameters
#define _NULL_STATIC_VECTOR_PARAMETERS_ class vectorAddition_fnct = elementwiseAddition, class scalarMultiplication_fnct = elementwiseScalarMultiplication, class innerProduct_fnct = elementwiseDotProduct, uintptr_t outerProduct_ptr = 0
#define _STATIC_VECTOR_PARAMETERS_ vectorAddition_fnct,scalarMultiplication_fnct,innerProduct_fnct,outerProduct_ptr

template <class elementType = double, class containerType = std::vector<elementType>, _NULL_STATIC_VECTOR_PARAMETERS_>
struct realvector: public arrayvector<elementType, containerType>, public symbolicObject<realvector<elementType,containerType,_STATIC_VECTOR_PARAMETERS_>>, public vectorAddition_fnct, public scalarMultiplication_fnct, public innerProduct_fnct
{
  // Constructors
  realvector() {(this->basis_used) = NULL;}
  realvector(basis* reference_basis) {(this->basis_used) = reference_basis;}
  realvector(elementType *input, dimi length, basis* reference_basis = NULL)
  {
    try
      {
	reserve_container((this->elements),length);
	for(dimi iter = 0; iter < length; iter++)
	  {
	    (this->elements).push_back(input[iter]);
	  }
      }
    catch (const std::exception& e)
      {
	(this->elements).clear();
	(this->elements).assign(((this->elements).size()), 0);
	(this->basis_used) = &VECTOR_ERROR;
      }
    (this->basis_used) = reference_basis;
  }
  template<class inputType>
  realvector(realvector<inputType> input)
  {
    (this->elements) = input.elements; //TODO: fix this...
    (this->basis_used) = input.basis_used;
  }
  template<class inputType>
  realvector(std::initializer_list<inputType> input, basis* reference_basis = NULL)
  {
    reserve_container((this->elements),input.size());
    for(auto value: input)
      {
	(this->elements).push_back(value);
      }
    (this->basis_used) = reference_basis;
  }
  realvector(std::initializer_list<elementType> input, basis* reference_basis = NULL)
  {
    (this->elements) = input;
    (this->basis_used) = reference_basis;
  }
  realvector(const containerType& C, basis* reference_basis = NULL)
  {
    (this->elements) = C;
    (this->basis_used) = reference_basis;
  }
  // Basic vector manipulation functions
  // Vector addition
  template<class inputType>
  void operator+=(realvector<inputType> input)
  {
    vectorAddition_fnct::op_to((this->elements),input.elements);
  }
  template<class inputType>
  void operator-=(realvector<inputType> input)
  {
    if((this->basis_used) == input.basis_used)
      {
	try
	  {
	    for(int iter = 0; iter < input.elements.size(); iter++)
	      {
		(this->elements)[iter]-=input.elements[iter];
	      }
	  }
	catch (const std::exception& e)
	  {
	    //TODO: implement exception handling
	  }
      }
    else
      {
	//TODO: Implement basis handling
      }
  }
  template<class scalarType>
  void operator*=(scalarType scalar_multiplier)
  {
    //std::cout<<"MULTIPLICATION CALLED W/"<<scalar_multiplier<<"\n";  //Scaffolding
    scalarMultiplication_fnct::op((this->elements),scalar_multiplier);
  }
  template<class vectorType>
  bool operator==(const vectorType& input)
  {
    return elementwiseEquality::equal_to((this->elements),(this->basis_used),input.elements,input.basis_used);
  }
  template<class vectorType>
  bool operator!=(const vectorType& input)
  {
    return !(operator==(input));
  }
  // Operational constructors
  // "+" operator (addition)
  // Copy operator (containers specified)
  template <class contType2>
  const realvector<elementType, containerType>& operator=(const staticBinaryOperation<realvector<elementType,containerType>,realvector<elementType,contType2>,&add>& X)
  {
    (this->elements).clear();
    const realvector<elementType, containerType>& right = X.right;
    const realvector<elementType, containerType>& left = X.left;
    if((right.size() == left.size()))
      {
	// Inherited value initialization
	if(right.basis_used == left.basis_used)
	  {
	    (this->basis_used) = right.basis_used;
	    vectorAddition_fnct::op((this->elements),right.elements,left.elements);
	  }
	else
	  {
	    (this->basis_used) = left.basis_used;
	    vectorAddition_fnct::op(right.elements,left.elements,right.basis_used,left.basis_used,(this->basis_used));
	  }
      }
      else
	{
	  (this->basis_used) = &VECTOR_ERROR;
	}
    return *this;
  }
  // Extensible-right copy operator WIP
  template <class contType2, class elementType2, class inputType1, class inputType2>
  const realvector<elementType, containerType>& operator=(const staticBinaryOperation<staticBinaryOperation<inputType1,inputType2,&add>,realvector<elementType2,contType2>,&add>& X)
  {
    #ifdef _T_VEC_SYMB_SCAFFOLDING_
    std::cout<<"Beginning conversion from symbolic to static form\n";
    #endif
    (this->elements).clear();
    dimi depth = op_rdepth<staticBinaryOperation<staticBinaryOperation<inputType1,inputType2,&add>,realvector<elementType2,contType2>,&add>,&add>::num;
    //operator=(X.right);
    const realvector<elementType2,contType2>* ptrs[depth];
    op_ptrs<staticBinaryOperation<inputType1,inputType2,&add>,realvector<elementType2,contType2>,realvector<elementType2,contType2>>::get_ptrs(ptrs,X);
    #ifdef _T_VEC_SYMB_SCAFFOLDING_
    std::cout<<"Depth calculated to be "<<depth<<", array initialized\n";
    #endif
    //TODO: Implement basis pointer handling (e.g. for INVALID_MATRIX errors)
    (this->basis_used) = X.right.basis_used;
    reserve_container((this->elements),X.right.size());
    #ifdef _T_VEC_SYMB_SCAFFOLDING_
    std::cout<<"Element vector initialized\n";
    #endif
    for(dimi iter = 0; iter < X.right.size(); iter++)
      {
	(this->elements).push_back(X.right.elements[iter]);
      }
    for(dimi miniiter = 0; miniiter < depth; miniiter++)
      {
	vectorAddition_fnct::add_to((this->elements),(this->basis_used),ptrs[miniiter]->elements,ptrs[miniiter]->basis_used);
      }
    return *this;
  }
  template <class contType2, class elementType2, class inputType1, class inputType2>
  realvector (const staticBinaryOperation<staticBinaryOperation<inputType1,inputType2,&add>,realvector<elementType2,contType2>,&add>& X)
  {
    operator=(X);
  }
    
  // Copy constructor
 template <class contType2>
 realvector (const staticBinaryOperation<realvector<elementType,containerType>,realvector<elementType,contType2>,&add>& X)
  {
    operator=(X);
  }
};


  


/*
template<class elementType = double, class matrixType = asmatrix<elementType>, class indeterminateType = std::vector<elementType>, class arrayHandlerUsed = solveForVariables>
struct linearSystem
{
  matrixType& representation;
  linearSystem(matrixType& representation_in, indeterminateType& variables_in)
  {
    representation_in = representation_in;
    variables_in = variables_in;
  }
  const matrixType& solve() const
  {
    matrixType temporary = representation.rref();
    return temporary;
  }
  void solverep()
  {
    representation.row_reduce();
    representation.echelonize();
  }
};
*/

// Tensor class family: the avector, rvector and tvector, which can all be passed around using tensor* AND vector* (former recommended for full functionality)
//NOTE: There are no special classes here, either for handling or otherwise. 

template <class elementType, class containerType = std::vector<elementType>>
struct avector: public tensor // avectors and rvectors are tensors, as are tvectors. See above
{
  containerType elements;
  dimi index_div;
  // Constructors
  avector() {} //TODO: fill in default constructor!
  // Simple vector constructors
  avector(double *input, dimi length)
  {
    try
      {
	reserve_components((this->elements),length);
	for(int iter = 0; iter < length; iter++)
	  {
	    (this->elements).push_back(input[iter]);
	  }
      }
    catch (const std::exception& e)
      {
	(this->elements).clear();
	(this->elements).assign(((this->elements).size()), 0);
	(this->space).setSpace(&ALLOC_ERROR, (this->elements).size());
      }
    (this->space).setSpace(length);
    (this->basis_used) = NULL;
  }
  template<class inputType>
  avector(const avector<inputType> &input)
  {
    (this->elements) = input.elements;
    (this->space) = input.space;
    (this->basis_used) = input.basis_used;
  }
  // Avector operations. These fetch function pointers from their symbolic scalar fields, OR have function pointers specified.
  // Rvector, on the other hand, uses elementwise, standard functions (that is, for an rvector of double, addition is simply double + double, versus f(double,double)) OR function pointers given
  // Vector 
  std::string print()
  {
    std::stringstream printer;
    //printer<<"COVARIANT RANK: "<<(this->space).covarrank()<<"\n"; // Scaffolding
    switch((this->space).covarrank())
      {
      case 0:
	//printer<<"0"; // Scaffolding
	switch ((this->space).contrarank())
	  {
	  case 0:
	    printer<<",0";
	    break;
	  case 1:
	    //printer<<",1"; // Scaffolding
	    printer<<"["<<elements[0];
	    for(dimi iter = 1; iter < elements.size(); iter++)
	      {
		printer<<","<<elements[iter];
	      }
	    printer<<"]";
	    return printer.str();
	  case 2:
	    printer<<",2";
	    break;
	  default:
	    printer<<",default";
	  }
	break;
      case 1:
	//printer<<"1"; // Scaffolding
	switch ((this->space).contrarank())
	  {
	  case 0:
	    //printer<<",0"; // Scaffolding
	    printer<<"["<<elements[0];
	    for(dimi iter = 1; iter < elements.size(); iter++)
	      {
		printer<<","<<elements[iter];
	      }
	    printer<<"]^T";
	    return printer.str();
	  case 1:
	    //printer<<",1"; // Scaffolding
	    if((this->space).is_lesser_tensor_vector())
	      {
		printer<<"{"<<elements[0];
		for(dimi iter = 1; iter < elements.size(); iter++)
		  {
		    printer<<","<<elements[iter];
		  }
		printer<<"}";
	      }
	    else
	      {
		printer<<"{";
		for(dimi iter = 0; iter < (this->space).dimensions[1]; iter++)
		  {
		    if(!(iter == 0))
		      {
			printer<<"\n,";
		      }
		    int calibrator = iter*((this->space).dimensions[0]);
		    printer<<"{"<<elements[calibrator];
		    //printer<<"Minisize: "<<(iter + (this->space).dimensions[1])<<","<<(this->space).dimensions[0]<<","<<iter<<"/"; // Scaffolding
		    for(dimi miniiter = 1; miniiter < (this->space).dimensions[0]; miniiter++)
		      {
			printer<<","<<elements[calibrator+miniiter];
		      }
		    printer<<"}";
		  }
		printer<<"}";
	      }
	    break;
	  default:
	    printer<<",default";
	  }
	break;
      case 2:
	printer<<"2"; // Scaffolding
	switch ((this->space).contrarank())
	  {
	  case 0:
	    printer<<",0";
	    break;
	  default:
	    printer<<",default";
	  }
	break;
      default:
	printer<<"default"; // Scaffolding
      }
    return printer.str();
  }
  std::string printdetails()
  {
    std::stringstream printer;
    printer<<"Values:\n"<<(this->print())<<"\n"
	   <<"Tensor rank: ("<<(this->space).covarrank()<<","<<(this->space).contrarank()<<")\n"
	   <<"Scalar field: "<</*(this->space).scalar_field->print()*/"NOT YET IMPLEMENTED\n"
	   <<"Dimensions: ";
    for(dimi iter = 0; iter < (this->space).dimensions.size(); iter++)
      {
	printer<<(this->space).dimensions[iter]<<" ";
      }
    printer<<"\n";
    return printer.str();
  }
};

template <class elementType, class containerType = std::vector<elementType>, class scalarField = elementType, _NULL_STATIC_VECTOR_PARAMETERS_>
struct rvector: public avector<elementType, containerType>, public symbolicObject<rvector<elementType,containerType,scalarField,_STATIC_VECTOR_PARAMETERS_>>
{
  // Constructors
  rvector() {} //TODO: improve default constructor
  // Simple vector constructors
  rvector(elementType *input, dimi length)
  {
    try
      {
	reserve_container((this->elements),length);
	for(dimi iter = 0; iter < length; iter++)
	  {
	    (this->elements).push_back(input[iter]);
	  }
      }
    catch (const std::exception& e)
      {
	(this->elements).clear();
	(this->elements).assign(((this->elements).size()), 0);
	(this->space).setSpace(&ALLOC_ERROR, (this->elements).size());
      }
    (this->space).setSpace(length);
    (this->basis_used) = NULL;
  }
  rvector(elementType* input, dimi rows, dimi cols)
  {
    try
      {
	reserve_container((this->elements),rows*cols);
	for(dimi iter= 0; iter < (rows*cols); iter++)
	  {
	    (this->elements).push_back(input[iter]);
	  }
      }
    catch (const std::exception& e)
      {
	(this->elements).clear();
	(this->elements).assign(((this->elements).size()), 0);
	(this->space).setSpace(&ALLOC_ERROR, (this->elements).size());
      }
    (this->space).setSpace(&Rn, rows, cols);
    (this->basis_used) = NULL;
  }
  template<class inputType>
  rvector(const rvector<inputType> &input)
  {
    (this->elements) = input.elements;
    (this->space) = input.space;
    (this->basis_used) = input.basis_used;
  }
  template<class inputType>
  rvector(const realvector<inputType> &input)
  {
    (this->elements) = input.elements;
    (this->space).setSpace(&Rn, input.elements.size());
    (this->basis_used) = input.basis_used;
  }
  template<class inputType>
  rvector(const std::initializer_list<inputType>& input)
  {
    reserve_components((this->elements),input.size());
    auto input_pointer = input.begin();
    for(dimi iter = 0; iter < input.size(); iter++)
      {
	(this->elements).push_back(input_pointer[iter]);
      }
    (this->space).setSpace(&Rn, input.size());
    (this->basis_used) = NULL;
  }
  elementType det() // The hyperdeterminant
  const {
    //TODO: implement!
    return (this->elements).at(0);
  }
  template<class inputType>
  void operator+=(const rvector<inputType> &input)
  {
    if((this->basis_used) == input.basis_used)
      {
	if((this->space) == input.space)
	  {
	    for(dimi iter = 0; iter < input.elements.size(); iter++)
	      {
		(this->elements)[iter] += input.elements[iter]; //TODO: Implement the += operator for vector-pointers, (vectors and vectorpointers) and (vectorpointers and vectors)
	      }
	  }
	else
	  {
	    //TODO: Implement error handling. Probably return a vector in ERRORspace
	    // Perhaps SPACE_ERROR?
	    // Also, check for compatible vector spaces (e.g. Cn + Rn = Cn)
	    // Perhaps a compatible function which takes in two scalar fields?
	    // Also, make sure to check for compatible spaces with different values of is_lesser_tensor_vector, and format to the value of the vector on the left of the operator.
	  }
      }
    else
      {
	//TODO: Implement basis handling...
      }
  }
  template<class inputType>
  void operator-=(const rvector<inputType> &input)
  {
    if((this->basis_used) == input.basis_used)
      {
	if((this->space) == input.space)
	  {
	    for(int iter = 0; iter < input.elements.size(); iter++)
	      {
		(this->elements)[iter] -= input.elements[iter]; //TODO: Implement the += operator for vector-pointers, (vectors and vectorpointers) and (vectorpointers and vectors)
	      }
	  }
	else
	  {
	    //TODO: Implement error handling. Probably return a vector in ERRORspace
	    // Perhaps SPACE_ERROR?
	    // Also, check for compatible vector spaces (e.g. Cn + Rn = Cn)
	    // Perhaps a compatible function which takes in two scalar fields?
	    // Also, make sure to check for compatible spaces with different values of is_lesser_tensor_vector, and format to the value of the vector on the left of the operator.
	  }
      }
    else
      {
	//TODO: Implement basis handling...
      }
  }
  template<class inputType>
  void operator*=(inputType input)
  {
    for(int iter = 0; iter < (this->elements).size(); iter++)
      {
	(this->elements)[iter] *= input; //TODO: Implement the *= operator for vector-pointers
      }
  }
  // The outer/tensor product for rvectors.
  //TODO: implement.
  template<class inputType>
  void operator*= (rvector<inputType> input)
  {
    
  }
  
};

// rvector operator definitions

// Template for operator definitions:
//template<class elementType, class containerType, class inputType, class inputCType, class scalarField, uintptr_t vectorAdditon_ptr, uintptr_t scalarMultiplication_ptr, uintptr_t innerProduct_ptr, uintptr_t outerProduct_ptr>

struct tvector: public tensor
{
  std::vector<void*> elements; //TODO: change this...
};

#endif
