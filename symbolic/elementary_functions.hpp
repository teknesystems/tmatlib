// Basic elementary function types and their various compositions, including:
// Polynomials and binomials
// Exponentials
// Complex exponentials and/or trigonometric functions
// Logarithms
// Rational functions

// Header guard. Skips over entire header if already included
#ifndef _T_ELEM_FUNC_INCLUDED_
#define _T_ELEM_FUNC_INCLUDED_

// Include statement block
#include <cmath>
#include "../vectors.hpp"
#include "../sparse_vector_ops.hpp"

// Monomials
// Defined here mainly for polynomial basis purposes...
template <class elementone, class elementtwo>
struct monomial
{
  // Defined as (multiplier)(*indeterminate)^(exponent)
  elementone multiplier;
  elementtwo exponent;
  variable* indeterminate;
  elementone evaluate(elementone input)
  {
    return multiplier*pow(input, exponent);
  }
  monomial(elementone input, elementtwo inputtwo) {multiplier = input; exponent = inputtwo;}
  monomial() {}
  std::string print(std::string varname)
  {
    std::stringstream printer;
    if(multiplier == 0)
      {
	printer<<"0";
	return printer.str();
      }
    else if(!(multiplier == 1))
      {
	printer<<multiplier;
      }
    if(exponent == 0)
      {
	return printer.str();
      }
    printer<<varname;
    if(!(exponent==1))
      {
	printer<<"^"<<exponent;
      }
    return printer.str();
  }
  std::string print()
  {
    return print("x"); //TODO: implement indeterminate checking
  }
};

template <class elementType, class containerType = std::vector<elementType>, class order = std::less<void>>
struct polynomial: public realvector<elementType, containerType, sortedSegmentedAddition<order>, segmentedScalarMultiplication<2>> //PIF Container (Polynomial Instruction Format)
{
  variable* indeterminate; //If NULL, treat as an arbirary function of an input "x", and treat the integral and differential as with respect to this "x" (as integrate and differentiate are w/ res to this func)
  polynomial() {};
  template<class IT>
  void writePolynomial(IT B, IT E)
  {
    auto EXP = std::next(B,1);
    while(EXP < E)
      {
	sorted_insert_first((this->elements),*B,*EXP,order());
	std::advance(B,2);
	std::advance(EXP,2);
      }
    std::advance(B,2);
    if(B < E)
      {
	sorted_insert_first((this->elements),*B,0,order());
      }
  }
  template<class IT>
  polynomial(IT input, dimi length): indeterminate(NULL)
  {
    writePolynomial(input, std::next(input,length));
  }
  template<class IT>
  polynomial(IT input, dimi length, variable &varinput): indeterminate(&varinput)
  {
    writePolynomial(input, std::next(input,length));
  }
  template<class CT>
  polynomial(CT container)
  {
    writePolynomial(container_beginning(container),container_end(container));
  }
  template<class CT>
  polynomial(CT container, variable &varinput): indeterminate(&varinput)
  {
    writePolynomial(container_beginning(container),container_end(container));
  }
  template<class CT2>
  polynomial(polynomial<elementType,CT2,order> input)
  {
    (this->elements) = input->elements;
    (this->basis_used) = input->basis_used;
    (this->indeterminate) = input->indeterminate;
  }
  void setVariable(variable* input)
  {
    indeterminate = input;
  }
  void setVariable(variable &input)
  {
    indeterminate = &input;
  }
  std::string print(std::string varname)
  const {
    std::stringstream printer;
    dimi iter = 0;
    while(iter < (this->elements).size())
      {
 	if(!((this->elements)[iter]==0))
	  {
	    if(!((this->elements)[iter]==1))
	      {
		printer<<(this->elements)[iter];
	      }
	    iter++;
	    if(!((this->elements)[iter]==0))
	      {
		printer<<varname;
		printer<<"^"<<(this->elements)[iter];
	      }
	    if((iter+1) < (this->elements).size())
	      {
		printer<<" + ";
	      }
	    iter++;
	  }
	else
 	  {
	    iter+=2;
 	  }
      }
    return printer.str();
  }
  elementType degree()
  const {
    elementType result = (this->elements)[1];
    for(dimi iter = 1; iter < (this->elements).size(); iter+=2)
      {
	if((this->elements)[iter] > result)
	  {
	    result = (this->elements)[iter];
	  }
      }
    return result;
  }
  void differentiate()
  {
    //TODO: make pretty
    for(dimi iter = 0; iter < (this->elements).size(); iter+=2)
      {
	if((this->elements)[iter+1]!=0)
	  {
	    (this->elements)[iter]*=(this->elements)[iter+1];
	    (this->elements)[iter+1]+=(-1);
	  }
	else
	  {
	    (this->elements)[iter] = 0;
	  }
      }
  }
  polynomial<elementType> derivative()
  {
    polynomial<elementType> output (*this);
    output.differentiate();
    return output;
  }
  decltype(std::cbegin(containerType())) getConstIterator() const
  {
    return std::cbegin(this->elements);
  }
  template<class T>
  T evaluate(T input)
  const {
    decltype(std::pow(input,0)) result = 0;
    auto IT = getConstIterator();
    auto I2 = std::next(IT,1);
    //std::cout<<(this->elements).size(); // Scaffolding
    for(dimi iter = 0; iter < (this->elements).size(); iter+=2)
      {
	//std::cout<<"Loop #"<<iter; // Scaffolding
	result+= (*IT)*pow(input, (*I2));
	std::advance(IT,2);
	std::advance(I2,2);
      }
    return result;
  }
  template<class IT1, class IT2>
  void evaluate(IT1 A, IT2 B, dimi N)
  const {
    for(dimi k = 0; k < N; k++)
      {
	*B = evaluate(*A);
	std::advance(B,1);
	std::advance(A,1);
      }
  }
  template<class T>
  bool is_root(T A)
  const {
    return (almost_zero(evaluate(A)));
  }
  template<class IT>
  bool is_root(IT A, dimi N)
  const {
    auto END = std::next(A,N);
    while(A < END)
      {
	if(!is_root(*A))
	  {
	    return false;
	  }
      }
    std::cout<<"ALLTRUE";
    return true;
  }
  template<class T>
  auto operator()(const T& input) -> decltype(evaluate(input))
  const {
    return evaluate(input);
  }
  template<class IT, class IT2>
  void operator()(const IT& A, const IT2& B, dimi N)
  const {
    evaluate(A,B,N); 
  }
  std::string print()
  const {
    if(indeterminate == NULL)
      {
	return this->print("x");
      }
    else
      {
	//return this->print(indeterminate->name);
	return this->print(indeterminate->name); //TEMPORARY!
      }
  }
  std::string printdetails()
  const {
    std::stringstream printer;
    printer<<"Object type: Polynomial"<<"\n"
	   <<"Value: "<<(this->print())<<"\n"
	   <<"Degree: "<<degree()<<"\n";
    return printer.str();
  }
  monomial<elementType, elementType> term(dimi input)
  {
    monomial<elementType, elementType> result((this->elements).at(input*2),(this->elements).at(1+(input*2)));
    return result;
  }
};

#endif

