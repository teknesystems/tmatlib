// Utilities for working with arrays, such as:
// Sorted insertion
// Sorting blocks of n elements

// Useful for polynomials and other such functions

// Header guard. Skips over entire header if already included
#ifndef _T_AUTIL_INCLUDED_
#define _T_AUTIL_INCLUDED_

#include "utility.hpp"

struct firstLargest
{
  template<class IT1, class IT2>
  inline static bool Compare(IT1 A, IT2 B, dimi drag = 0)
  {
    return (*A) > (*B);
  }
  template<class IT1, class IT2>
  bool operator() (IT1 A, IT2 B, dimi drag = 0)
  {
    return Compare(A,B,drag);
  }
};

struct firstSmallest
{
  template<class IT1, class IT2>
  inline static bool Compare(IT1 A, IT2 B, dimi drag = 0)
  {
    return (*A) < (*B);
  }
  template<class IT1, class IT2>
  bool operator() (IT1 A, IT2 B, dimi drag = 0)
  {
    return Compare(A,B,drag);
  }
};

template<int N>
struct largest
{
  template<class IT1, class IT2>
  inline static bool Compare(IT1 A, IT2 B, dimi drag = 1)
  {
    return *(std::next(A,drag-1)) > *(std::next(B,drag-1));
  }
  template<class IT1, class IT2>
  bool operator() (IT1 A, IT2 B, dimi drag = 0)
  {
    return Compare(A,B,drag);
  }
};

template<class IT, class IT2>
void segmented_copy(IT A, IT B, IT2 D)
{
  while(A < B)
    {
      *D = *A;
      //std::cout<<*D<<"-";
      std::advance(A,1);
      std::advance(D,1);
    }
  //std::cout<<"\n";
}

template<class comparator, class IT1, class IT2, class IT3>
void merge_segmented(IT1 A, IT1 AE, IT2 B, IT2 BE, IT3 D, dimi drag)
{
  auto AEND = std::next(A,drag);
  auto BEND = std::next(B,drag);
  while((AEND <= AE)&&(BEND <= BE))
    {
      if(comparator::Compare(A,B,drag))
	{
	  segmented_copy(A,AEND,D);
	  std::advance(A,drag);
	  std::advance(AEND,drag);
	  std::advance(D,drag);
	}
      else
	{
	  segmented_copy(B,BEND,D);
	  std::advance(B,drag);
	  std::advance(BEND,drag);
	  std::advance(D,drag);
	}
      //std::cout<<"\n";
    }
  if(!(A < AE))
    {
      segmented_copy(B,BE,D);
    }
  else
    {
      segmented_copy(A,AE,D);
    }
  //std::cout<<"FIN\n";
}

// Sorts blocks of length "drag". Only works where drag divides into the length of the array! If drag = 1, acts like mergesort.
template<class comparator = firstSmallest, class IT, class IT2>
void swapSort(IT A, std::size_t N, IT2 W, std::uint8_t drag = 1, bool fromwork = true)
{
  if(N > drag)
    {
      // Getting an iterator/pointer to the second half of the array
      auto B = std::next(A,N/2);
      auto WB = std::next(W,N/2);
      auto BE = std::next(A,N);
      auto WE = std::next(W,N);
      
      // Swapping the two halves of the input array
      swapSort<comparator>(A,N/2,W,drag,!fromwork);
      swapSort<comparator>(B,(N/2+N%2),WB,drag,!fromwork); // B should always be either longer (if N is odd) or the same length as A

      // Merging two recieved arrays into work array:
      if(fromwork)
      {
        merge_segmented<comparator>(W,WB,WB,WE,A,drag);
      }
      else
      {
	merge_segmented<comparator>(A,B,B,BE,W,drag);
      }

      // Copying work array to active array:
      //segmented_copy(W,WE,A);
    }
}


template<bool enable>
struct fillArray
{
  template<class CT, class ST, class MT, class VT = double>
  inline static void moduloArray(CT& A, const ST& S, const MT& M, const VT& V = 0)
  {
    resize_container(A,(S+(S%M)),V);
  }
};

template<>
struct fillArray<false>
{
  template<class CT, class ST, class MT>
  inline static void moduloArray(const CT& A, const ST& S, const MT& M) {/*Do nothing*/}
};

template<bool safety = true, class comparator = firstSmallest, class CT, class CT2 = CT>
void swapSort(CT& A, std::uint8_t drag = 1)
{
  // Safety (makes drag divide into the length of the array)
  fillArray<safety>::moduloArray(A,A.size(),drag);

  // Copies A into work array
  CT2 W = A;

  // Performs dragged-mergesort with work array
  swapSort<comparator>(container_beginning(A),A.size(),container_beginning(W),drag);
};

template<class VT, class KT>
struct keypair
{
  VT val;
  KT key;
  bool operator==(KT K) const {return key == K;}
  bool operator<(KT K) const {return key < K;}
  keypair(VT valin, KT keyin): val(valin), key(keyin) {}
};

template<class T>
T getKeypairVal(T A)
{
  return A;
}

template<class VT, class KT>
VT getKeypairVal(keypair<VT,KT> A)
{
  return A.val;
}

struct nullAction
{
  template<class T1, class T2, class T3, class T4>
  inline static void process(const T1& A, const T2& B, const T3& C, const T4& D) {} // Do nothing
  template<class T1, class T2, class T3, class T4>
  void operator()(const T1& A, const T2& B, const T3& C, const T4& D) {} // Do nothing
};

struct additionAction
{
  template<class T1, class T2, class T3, class T4>
  inline static void process(T1& ADD_TO, const T2& VAL, const T3& C, const T4& D) {ADD_TO+=getKeypairVal(VAL);}
  template<class T1, class T2, class T3, class T4>
  void operator() (T1& ADD_TO, const T2& VAL, const T3& C, const T4& D) {process(ADD_TO,VAL,C,D);}
};

template<class IT = dimi, IT val = std::numeric_limits<IT>::max()>
struct nullReturn
{
  template<class T1, class T2>
  inline static IT get_return(const T1& A, const T2& B) {return val;}
  template<class T1, class T2>
  IT operator()(const T1& A, const T2& B) {return val;}
};

struct indexReturn
{
  template<class IT1, class IT2>
  inline static IT1 get_return(const IT1& index, const IT2& drag) {return drag*index;}
  template<class IT1, class IT2>
  IT1 operator() (const IT1& index, const IT2& drag) {return get_return(index,drag);}
};


struct approx_equal
{
  template<class T1, class T2>
  inline static bool compare(T1 A, T2 B)
  {
    _TMATLIB_CONST_IF_ ((!(std::numeric_limits<T1>::epsilon()))&&(!(std::numeric_limits<T2>::epsilon())))
      {
	return A == B;
      }
    else
      {
	return almost_equal(A,B);
      }
  }
  template<class T1, class T2, class T3>
  inline static bool compare(keypair<T1,T2> K, T3 B)
  {
    return compare(K.key,B);
  }
  template<class T1, class T2>
  bool operator() (T1 A, T2 B)
  {
    return compare(A,B);
  }
};

template<int drag = 1, int shift = 0, int wshift = 0, class CT, class VT,
	 class sorting_order = std::less<void>, class equalityCompare = approx_equal, class equalityAction = nullAction, class success_return = indexReturn>
dimi log_search(CT& A, const VT& V, sdimi lower, sdimi upper,
		sorting_order compare = std::less<void>(), equalityAction on_equality = nullAction(), success_return return_on_success = indexReturn(), equalityCompare check_equality = approx_equal())
{
  //std::cout<<lower<<","<<upper<<"\n";
  if(upper >= lower)
    {
      dimi index = (lower+upper)/2;
      if(check_equality(V,A[drag*index+shift]))
	{
	  on_equality(A[drag*index+wshift],V,lower,upper);
	  return return_on_success(index,drag);
	}
      else if(compare(V,A[drag*index+shift]))
	{
	  upper = index-1;
	  _TMATLIB_CONST_IF_ (drag > 1) // This check should be optimized out, as drag is constexpr
	    {
	      if(index == 1)
		{
		  if(V == A[shift])
		    {
		      on_equality(A[wshift],V,lower,upper);
		      return return_on_success(0,drag);
		    }
		}
	    }
	  return log_search<drag,shift>(A,V,lower,upper,compare,on_equality,return_on_success,check_equality);
	}
      else
	{
	  lower = index+1;
	  return log_search<drag,shift>(A,V,lower,upper,compare,on_equality,return_on_success,check_equality);
	}
    }
  else
    {
      return drag*lower;
    }
};

template<class equalityAction = nullAction, class success_return = indexReturn, class sorting_order = std::less<void>, class equalityCheck = approx_equal, class IT = dimi, class containerType, class VT>
IT sorted_insert(containerType& A, const VT& V,
		 sorting_order compare = std::less<void>(), equalityAction on_equality = nullAction(), success_return return_successful = indexReturn(), equalityCheck check_equality = approx_equal())
{
  //TODO: Fix bug with large blocks of equal entities
  IT N = container_size(A);
  if(N!=0)
    {
      auto P = container_beginning(A);
      IT L = log_search(A,V,0,(N-1),compare,on_equality,return_successful,check_equality);
      if((L!=std::numeric_limits<IT>::max()))
	{
	  auto ACTIVE = std::next(P,L);
	  if(L < (N-1))
	    {
	      A.insert(ACTIVE,V);
	      return L;
	    }
	  else
	    {
	      if (*ACTIVE > V)
		{
		  A.insert(ACTIVE,V);
		  return L;
		}
	    }
	}
    }
  A.push_back(V);
  return 0;
}

template<dimi shift = 1, class IT = dimi, class containerType, class VT, class KT,
	 class sorting_order = std::less<void>, class equalityAction = additionAction, class success_return = nullReturn<IT>, class equalityCheck = approx_equal>
IT sorted_insert_first(containerType& A, const VT& V, const KT& K,
		       sorting_order compare = std::less<void>(), equalityAction on_equality = additionAction(),
		       success_return return_successful = nullReturn<IT>(), equalityCheck check_equality = approx_equal())
{
  IT N = container_size(A);
  keypair<VT,KT> kp (V,K);
  if(N>1)
    {
      if(K < A[N-1])
	{
	  dimi uN = N/2;
	  //std::cout<<uN<<","<<(uN-1)<<"\n";
	  auto P = container_beginning(A);
	  IT L = log_search<2,1>(A,kp,0,(uN-1),compare,on_equality,return_successful,check_equality);
	  if((L!=std::numeric_limits<IT>::max()))
	    {
	      if(L < (N-1))
		{
		  A.insert(std::next(P,L),K);
		  A.insert(std::next(container_beginning(A),(L)),V);
		  return L;
		}
	      else if(*(std::next(P,L+1)) > K)
		{
		  A.insert(std::next(P,L),K);
		  A.insert(std::next(container_beginning(A),(L)),V);
		  return L;
		}
	    }
	  else
	    {
	      return L;
	    }
	}
      else if(K == A[N-1])
	{
	  equalityAction::process(A[N-2],V,N-2,N-2);
	  return (N-2);
	}
    }
  A.push_back(V);
  A.push_back(K);
  return 0;
}


template<dimi drag = 1, dimi shift = 0, class ICT, class DCT, class ORD = std::less<void>, class EQA = nullAction, class SR = indexReturn, class EQC = approx_equal>
void sorted_insert_array(DCT& OUT, ICT& IN, ORD sorting_order = std::less<void>(), EQA equal_action = nullAction(), SR success_return = indexReturn(), EQC equal_check = approx_equal())
{
  for(auto val: IN)
    {
      sorted_insert<drag,shift>(OUT,val,sorting_order,equal_action,success_return,equal_check);
    }
}

template<dimi drag = 2, dimi shift = 1, class IT, class DCT, class ORD = std::less<void>, class EQA = nullAction, class SR = indexReturn, class EQC = approx_equal>
void sorted_insert_pair_array(DCT& OUT, IT PB, IT PEND, ORD sorting_order = std::less<void>(), EQA equal_action = nullAction(), SR success_return = indexReturn(), EQC equal_check = approx_equal())
{
  auto PE = std::next(PB,drag);
  while(PE<PEND)
    {
      sorted_insert_first<shift>(OUT,*PB,*PE,sorting_order,equal_action,success_return,equal_check);
      std::advance(PB,drag);
      std::advance(PE,drag);
    }
  std::advance(PB,1);
  if(PB<PEND)
    {
      sorted_insert_first<shift>(OUT,*PB,0,sorting_order,equal_action,success_return,equal_check);
    }
}

template<dimi drag = 2, dimi shift = 1, class ICT, class DCT, class ORD = std::less<void>, class EQA = additionAction, class SR = indexReturn, class EQC = approx_equal>
void sorted_insert_pair_array(DCT& OUT, ICT& IN, ORD sorting_order = std::less<void>(), EQA equal_action = additionAction(), SR success_return = indexReturn(), EQC equal_check = approx_equal())
{
  auto PB = container_beginning(IN);
  auto PEND = container_end(IN);
  sorted_insert_pair_array<drag,shift>(OUT,PB,PEND,sorting_order,equal_action,success_return,equal_check);
}

#endif
