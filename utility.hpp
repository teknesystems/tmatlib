// Header guard, skips over entire header if already included
#ifndef _T_MAT_UTIL_INCLUDED_
#define _T_MAT_UTIL_INCLUDED_

#include <vector>
#include <numeric>
#include <array>
#include <cstddef>
#include <algorithm>
#include <initializer_list>
#include <complex>
#include <sstream>
#include <string>
#include <type_traits>

// Macros:

#define _TMATLIB_USE_CPPZ_IF_AVAILABLE_

#if __cplusplus > 201402L
#ifdef _TMATLIB_USE_CPPZ_IF_AVAILABLE_
#define _TMATLIB_CPPZ_ENABLED_
#endif
#endif

#ifdef _cpp_if_constexpr
#if _cpp_if_constexpr>=201606
#define _TMATLIB_CONST_IF_ if constexpr
#else
#define _TMATLIB_CONST_IF_ if
#endif
#else
#define _TMATLIB_CONST_IF_ if
#endif

// Dimensional types
typedef long unsigned int dimi; //TODO: determine if it is better to use std::size_t instead of unsigned int. If size_t is better, consider using a standard C array for vectorSpace dimensions...
typedef std::ptrdiff_t sdimi;


// Polymorphic interfaces
struct polymorphicInterface
{
};

template <class T>
struct polymorphicContainerInterface
{
  virtual T& at(dimi index);
  virtual const T& const_at(dimi index);
  //virtual T& get(dimi index);
  //virtual T& const_get(dimi index);
  virtual T& operator[](dimi index);
  virtual bool reserve(dimi index);
  virtual bool resize(dimi index);
  virtual bool clear(dimi index);
  virtual bool empty();
  virtual T& begin(dimi index);
  virtual T& end(dimi index);
  virtual T& cbegin(dimi index);
  virtual T& cend(dimi index);
  virtual void swap(dimi index);
  virtual dimi size();
  template<class T1>
  void apply(T1& applied)
  {
    for(dimi iter = 0; iter < size(); iter++)
      {
	applied(*this[iter]);
      }
  }
  template<class T1>
  void const_apply(T1& applied) const
  {
    for(dimi iter = 0; iter < size(); iter++)
      {
	applied(*this[iter]);
      }
  }
};

// Size-pointer pair
template<class PT, class ST>
struct size_pointer
{
  PT pointer;
  ST length;
  inline ST size() {return length;}
  inline PT data() {return pointer;}
  inline PT begin() {return pointer;}
  inline PT end() {return std::next(pointer,length);}
  inline static void reserve(ST n) {} // No-op
  template<class VT>
  inline void assign(ST n, VT val)
  {
    auto tp = pointer;
    for(dimi k = 0; k < std::min(n,length); k++)
      {
	*tp = val;
	std::advance(tp);
      }
  }
};

// Multidimensional container interface
template <class T, dimi N, class P = T, class P2 = P, class... C>
struct polymorphicMCI: public polymorphicContainerInterface<P2>, public C...
{
  virtual P& at(std::initializer_list<dimi> indices);
  virtual P& at(dimi index1, dimi index2);
  virtual dimi size(dimi index);
};

// Container operations
template<class T>
void reserve_container(T& container, std::size_t size)
{
}

template<class T>
void reserve_container(std::vector<T>& container, std::size_t size)
{
  container.reserve(size);
}

template<class T>
void reserve_container(std::valarray<T>& container, std::size_t size)
{
  container.resize(size);
}

template<class T>
void resize_container(T& container, std::size_t size)
{
  container.resize(size);
}

template<class T>
void resize_container(T* pointer, std::size_t size) {} //No-op


template<class T, class V>
void resize_container(T& container, std::size_t size, const V& val)
{
  container.resize(size,val);
}

template<class CT>
std::size_t get_fill_size(const CT& container, std::size_t size) {return std::max(container.size(),size);}

template<class T>
std::size_t get_fill_size(T* pointer, std::size_t size) {return 0;} // Regardless of pointer input. Should optimize out.

template<class T, class VT>
void resize_container(T* pointer, std::size_t size, const VT& val) {} // No-op

template<class T>
auto container_beginning(T& container) -> decltype(std::begin(container))
{
  return std::begin(container);
}

template<class T>
T* container_beginning(T* pointer)
{
  return pointer;
}

template<class T>
T* container_beginning(const T* pointer)
{
  return pointer;
}

template<class T>
auto advanced_container_beginning(T& container, dimi shift) -> decltype(std::advance(container_beginning(container),shift))
  {
    auto R = container_beginning(container);
    std::advance(R,shift);
    return R;
  }

template<class P>
P return_advanced(const P& pointer, dimi shift)
{
  P R = pointer;
  std::advance(R,shift);
  return R;
}

/*
template<class T>
auto container_beginning(const T& container) -> decltype(container.cbegin())
const {
  return container.cbegin();
}
*/

template<class T>
auto container_end(T& container) -> decltype(std::end(container))
{
  return std::end(container);
}

/*
template<class T>
auto container_end(const T& container) -> decltype(container.cbegin())
const {
  return container.cend();
}
*/

template<class T>
auto container_end(T& container, dimi index) -> decltype(container.begin())
{
  return container.begin();
}

template<class T>
T* container_end(T* pointer, dimi index)
{
  return (pointer+index);
}

template<class T>
T* container_end(const T* pointer, dimi index)
{
  return (pointer+index);
}

template<class T>
auto container_end(const T& container, dimi index) -> decltype(container.cbegin())
const {
  return container.cbegin();
}

template<class T>
auto container_at(T& container, dimi index) -> decltype(container.begin()+index)
{
  return container.begin() + index;
}

template<class T>
T* container_at(T* pointer, dimi index)
{
  return (pointer+index);
}

template<class T>
T* container_at(const T* pointer, dimi index)
{
  return (pointer+index);
}

template<class T>
auto container_at(const T& container, dimi index) -> decltype(container.cbegin()+index)
const {
  return container.cbegin() + index;
}

template<class T>
void clear_container(T& container)
{
  container.clear();
}

template<class T, std::size_t N>
void clear_container(const std::array<T,N>& container)
{
}

template<class T>
void clear_container(const std::valarray<T>& container)
{
  container.resize(0);
}

template<class containerType, class T>
void push_to_container(containerType& container, const T& input, std::size_t index)
{
  container.push_back(input);
}
// TODO: Implement iterator support, as well as for pointers!
template<class T, class T2, std::size_t N>
void push_to_container(std::array<T2,N>& container, const T& input, std::size_t index)
{
  container[index] = input;
}

template<class T, class T2>
void push_to_container(std::valarray<T2>& container, const T& input, std::size_t index)
{
  container[index] = input;
}

template<class T, class T2>
void push_to_container(T* pointer, const T2& input, std::size_t index)
{
  pointer[index] = input;
}

template<class CT>
std::back_insert_iterator<CT> rewrite_front_iterator(CT& container) {return std::back_insert_iterator<CT>(container);}

template<class T>
auto rewrite_front_iterator(std::valarray<T>& container) -> decltype(std::begin(container)) {return std::begin(container);}

template<class T>
T* rewrite_front_iterator(T* pointer) {return pointer;}

template<class T, dimi N>
auto rewrite_front_iterator(std::array<T,N>& array) -> decltype(std::begin(array)) {return std::begin(array);}

template<class CT>
struct advanced_rewrite_iterator
{
  CT& container;
  // Movement operators
  inline void operator*() {} //No-op
  inline void operator++() {} //No-op
  inline void operator++(int x) {} //No-op

  // Assignment operators (assigns end of array)
  template<class T>
  inline void operator=(const T& val) {container.push_back(val);}
  inline void operator=(advanced_rewrite_iterator<CT> iter) {container = iter.container;}
  
  // Manipulation operators (manipulates the end of the array)
  template<class T>
  inline void operator+=(const T& val) {*(--container.end())+=val;} // Causes an error if the container is empty, which would happen with a regular C-array.
  template<class T>
  inline void operator-=(const T& val) {*(--container.end())-=val;} // Causes an error if the container is empty, which would happen with a regular C-array.
  template<class T>
  inline void operator*=(const T& val) {*(--container.end())*=val;} // Causes an error if the container is empty, which would happen with a regular C-array.
  template<class T>
  inline void operator/=(const T& val) {*(--container.end())/=val;} // Causes an error if the container is empty, which would happen with a regular C-array.

  // Equality/inequality checks (checks on end of the array), cause an error if the container is empty, which would happen with a regular C-array.
  template<class T>
  inline bool operator==(const T& val) {return *(--container.end()) == val;}
  template<class T>
  inline bool operator!=(const T& val) {return *(--container.end()) != val;}
  template<class T>
  inline bool operator<=(const T& val) {return *(--container.end()) <= val;}
  template<class T>
  inline bool operator>=(const T& val) {return *(--container.end()) >= val;}
  template<class T>
  inline bool operator>(const T& val) {return *(--container.end()) > val;}
  template<class T>
  inline bool operator<(const T& val) {return *(--container.end()) < val;}

  // Constructors
  advanced_rewrite_iterator(CT& input): container(input) {}
};

template<class CT>
advanced_rewrite_iterator<CT> advanced_rewrite_front_iterator(CT& container) {return advanced_rewrite_iterator<CT>(container);}

template<class T>
auto advanced_rewrite_front_iterator(std::valarray<T>& container) -> decltype(std::begin(container)) {return std::begin(container);}

template<class T>
T* advanced_rewrite_front_iterator(T* pointer) {return pointer;}

template<class T, dimi N>
auto advanced_rewrite_front_iterator(std::array<T,N>& array) -> decltype(std::begin(array)) {return std::begin(array);}


template<class T>
dimi container_size(const T& input)
{
  return input.size();
}

template<class T>
void zero_assign_container(T& container, std::size_t N)
{
  container.assign(N,0);
}

template<class T>
void zero_assign_container(std::valarray<T>& container, std::size_t N) {container.resize(N);}

template<class T, class S>
void assign_to_container(T& container, std::size_t N, const S& val) {container.assign(N,val);}

template<class T, class S>
void assign_to_container(std::valarray<T>& container, std::size_t N, const S& val)
{
  container.resize(N);
  auto I = std::begin(container);
  auto E = std::end(container);
  while(I < E)
    {
      *I = val;
      I++;
    }
}

template<class T, class S>
void assign_to_container(T* pointer, std::size_t N, const S& val)
{
  auto end_pointer = pointer + N;
  while(pointer < end_pointer)
    {
      *pointer = val;
      pointer++;
    }
}

template<class T>
void zero_assign_container(T* container, std::size_t N) {assign_to_container(container,0,N);}
			 
template<class T>
dimi container_size(T* input)
{
  return 0;
}

template<class T>
void empty_init(T& A)
{
}

template<class T>
void empty_init(T *&A)
{
  A = NULL;
}

template<class P>
P& jump_decay(P& pointer)
{
  return pointer;
}

template<class P, bool transpose = false>
struct jump_pointer
{
  // Iterator boilerplate
  using iterator_category = std::random_access_iterator_tag;
  using value_type = typename std::remove_reference<decltype(*P())>::type;
  using pointer = value_type*;
  using reference = value_type&;
  using difference_type = decltype(P() - P());
  
  P points;
  dimi jump;
  // Access
  auto operator[] (dimi n) -> decltype(points[n*jump]) {return points[n*jump];}
  auto operator* () -> decltype(*points) {return *points;}

  // Increment/decrement
  // Standard (w/ jump)
  void operator++() {std::advance(points, jump);}
  void operator++(int) {operator++();}
  void operator--() {points = points - jump;}
  void operator--(int) {operator--();}
  void operator+=(dimi n) {std::advance(points, n*jump);}
  void operator-=(dimi n) {points-= (n*jump);} 
  // Custom (w/out jump)
  void operator>>(dimi n) {std::advance(points, n);}
  void operator<<(dimi n) {std::advance(points, -n);}

  // Addition and subtraction
  jump_pointer operator+(dimi n) {jump_pointer R = *this; R+=n; return R;}
  jump_pointer operator-(dimi n) {jump_pointer R = *this; R-=n; return R;}

  // Difference
  decltype(P() - P()) operator-(jump_pointer J) {return points - J.points;}

  // Equality
  template<class P2>
  void operator=(jump_pointer<P2> J) {points = J.points; jump = J.jump;}
  template<class AP>
  void operator=(AP input) {points = input; jump = 1;}
  
  // Utility
  void negate() {jump*=-1;}

  // Comparison
  // TODO: add a macro...
  template<class PIN>
  bool operator==(PIN& pointer) {return points == jump_decay(pointer);}
  template<class PIN>
  bool operator<(PIN& pointer) {return points < jump_decay(pointer);}
  template<class PIN>
  bool operator>(PIN& pointer) {return points > jump_decay(pointer);}
  template<class PIN>
  bool operator>=(PIN& pointer) {return points >= jump_decay(pointer);}
  template<class PIN>
  bool operator<=(PIN& pointer) {return points <= jump_decay(pointer);}
  template<class PIN>
  bool operator!=(PIN& pointer) {return points != jump_decay(pointer);}

  // Constructor
  jump_pointer(P pointer, dimi jumper): points(pointer), jump(jumper) {}
  jump_pointer(P pointer): points(pointer), jump(1) {}
  template<class P2>
  jump_pointer(jump_pointer<P2> J):points(J.points),jump(J.jump) {}
  template<class AP>
  jump_pointer(AP input): points(input),jump(1) {}
};

template<class P>
struct jump_pointer<P,true>
{
  P points;
  dimi jump;
  // Access
  auto operator[] (dimi n) -> decltype(points[n*jump]) {return points[n*jump];}
  auto operator* () -> decltype(*points) {return *points;}

  // Increment/decrement
  // Standard (w/ jump)
  void operator++() {points++;}
  void operator++(int) {operator++();}
  void operator--() {points--;}
  void operator--(int) {operator--();}
  void operator+=(dimi n) {std::advance(points, n);}
  void operator-=(dimi n) {points-= (n);}
  // Custom (w/out jump)
  void operator>>(dimi n) {std::advance(points, n * jump);}
  void operator<<(dimi n) {std::advance(points, -n * jump);}

  // Addition and subtraction
  jump_pointer operator+(dimi n) {jump_pointer R = *this; R+=n; return R;}
  jump_pointer operator-(dimi n) {jump_pointer R = *this; R-=n; return R;}

  // Equality
  template<class P2>
  void operator=(jump_pointer<P2> J) {points = J.points; jump = J.jump;}
  template<class AP>
  void operator=(AP input) {points = input; jump = 1;}
  
  // Utility
  void negate() {jump*=-1;}

  // Comparison
  // TODO: add a macro...
  bool operator==(P& pointer) {return points == jump_decay(pointer);}
  bool operator<(P& pointer) {return points < jump_decay(pointer);}
  bool operator>(P& pointer) {return points > jump_decay(pointer);}
  bool operator>=(P& pointer) {return points >= jump_decay(pointer);}
  bool operator<=(P& pointer) {return points <= jump_decay(pointer);}
  bool operator!=(P& pointer) {return points != jump_decay(pointer);}

  // Constructor
  jump_pointer(P pointer, dimi jumper): points(pointer), jump(jumper) {}
  jump_pointer(P pointer): points(pointer), jump(1) {}
  template<class P2>
  jump_pointer(jump_pointer<P2> J):points(J.points),jump(J.jump) {}
  template<class AP>
  jump_pointer(AP input): points(input),jump(1) {}
};


template<class P, bool transpose>
P& jump_decay(jump_pointer<P,transpose>& pointer)
{
  return pointer.points;
}

template<class P>
jump_pointer<P> make_jump_pointer(const P& input,dimi jump)
{
  jump_pointer<P> J (input, jump);
  return J;
}

template<class P>
jump_pointer<P> make_jump_pointer(jump_pointer<P> input, dimi jump)
{
  input.jump = jump;
  return input;
}

template<class T>
struct underlyingFloat
{
  typedef T FT;
};

template<class CT>
struct underlyingFloat<std::complex<CT>>
{
  typedef CT FT;
};

// Status containers. Note that a status enum converts to a bool, with 0 being FALSE/INVALID and other values potentially meaning a certain thing in a certain context.
enum status_enum: char {
  invalid_false = 0,
  valid_true = 1,
  valid_false = 10,
  signal_caught = 2,
  signal_error = 3,
  not_applicable = 4,
  does_not_exist = 5,
  extra_set = 6,
  extra_vals = 7,
  test_not_set = 8,
  general_exception = 9,
  none = 127
};

template<class T>
struct status_container
{
  T container;
  T& getref() {return container;}
  const T& getconstref() {return container;}
  status_enum status;
  status_container(const T& input, status_enum input_status = none): container(input), status(input_status)
  {
  }
};

template<class T>
struct status_container<T&>
{
  T& container;
  T& getref() {return container;}
  const T& getconstref() {return container;}
  status_enum status;
  status_container(T& input, status_enum input_status = none): container(input), status(input_status)
  {
  }
};

template<class T>
struct status_container<const T&>
{
  const T& container;
  const T& getref() {return container;}
  const T& getconstref() {return container;}
  status_enum status;
  status_container(const T& input, status_enum input_status = none): container(input), status(input_status)
  {
  }
};

template<dimi precision_factor = 1, class FT, class FT2, class NLT = FT>
bool almost_equal(FT a, FT2 b)
  {
    return (std::abs(a-b) < std::abs(a+b)*precision_factor*std::abs(std::numeric_limits<NLT>::epsilon())); //TODO: Generalize in a complex-friendly way
  }

template<dimi precision_factor = 1, class IT1, class IT2>
bool array_almost_equal(IT1 a, IT1 ae, IT2 b)
{
  while(a < ae)
    {
      //std::cout<<(*a)<<"~~"<<(*b)<<"\n";
      if(!almost_equal<precision_factor>(*a,*b))
	{
	  return false;
	}
      b++;
      a++;
    }
  return true;
}

template<dimi precision_factor = 1, class C1, class C2>
bool array_almost_equal(const C1& a, const C2& b)
{
  auto A = container_beginning(a);
  auto AE = container_end(a);
  auto B = container_beginning(b);
  return array_almost_equal<precision_factor>(A,AE,B);
}

struct double_equality
{
  template<class C1, class C2>
  bool operator() (const C1& a, const C2& b) {return array_almost_equal(a,b);}
};

template<dimi precision_factor = 2, class C1, class ET = typename underlyingFloat<C1>::FT>
bool almost_zero(const C1& input)
{
  if(std::abs(input) <= static_cast<ET>(precision_factor)*std::numeric_limits<ET>::epsilon())
    {
      std::cout<<"ZERO!\n";
    }
  return std::abs(input) <= static_cast<ET>(precision_factor)*std::numeric_limits<ET>::epsilon();
}

template<class T>
status_container<const T&> associate_status(const T& input, status_enum input_status)
{
  status_container<const T&> result (input, input_status);
  return result;
}

struct operationUndefinedType
{
};

template<class T1, class T2 = T1>
struct intermediate_division
{
  typedef T1 resultType;
  typedef T2 inverseType;
  typedef T1 resultTypeHP;
  typedef T2 inverseTypeHP;
};

template<>
struct intermediate_division<int,int>
{
  typedef float resultType;
  typedef float inverseType;
  typedef double resultTypeHP;
  typedef double inverseTypeHP;
};

template<>
struct intermediate_division<char,char>
{
  typedef float resultType;
  typedef float inverseType;
  typedef double resultTypeHP;
  typedef double inverseTypeHP;
};

template<>
struct intermediate_division<dimi,dimi>
{
  typedef float resultType;
  typedef float inverseType;
  typedef double resultTypeHP;
  typedef double inverseTypeHP;
};

template<class NT>
std::string numtostring(const NT& input)
{
#ifdef _TMATLIB_MOBILE_SUPPORT_MODE_
  return std::to_string(input);
#else
  std::ostringstream converter;
  converter<<input;
  return converter.str();
#endif
}

template<class IT1, class IT2, class IT3>
auto gFMA(IT1 A, IT2 B, IT3 C) -> decltype(std::fma(A,B,C))
{
  return std::fma(A,B,C);
}

template<class IT1, class IT2, class IT3>
auto gFMA(std::complex<IT1> A, std::complex<IT2> B, std::complex<IT3> C) -> decltype((A*B) + C)
{
  return (A*B) + C;
}



/*
namespace properties
{
  enum class direction: char {
    left = 0, right = 1, // Used for boolean values
    front, back, up, down, // Inactive directions
    rightwards, leftwards, forwards, backwards, upwards, downwards, // Active directions
    zero, towards, away, // Special directions
    i, j, k, x, y, z, lateral, real, negative_lateral, negative_real, // Mathematical directions
    stop, very_slow, slow, medium, fast, very_fast, // Speed
    latitude, longitude, altitude, // Navigation
    positive, negative, complex_positive, complex_negative,
    horizontal, vertical
  }
}
*/

#endif
