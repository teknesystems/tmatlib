#include <iostream>
#include <chrono>
#include <random>

#include "unit_tests.hpp"
#include "array_utils.hpp"

polynomial<double> simplepolynomialwriter(dimi length)
{
  dimi setter;
  dimi toggle_fill = 0;
  double* testvectortwo = new double[length];
  std::cout<<"Input number of terms to manually set at the beginning: ";
  std::cin>>setter;
  for(dimi iter = 0; iter < length; iter+=2)
    {
      if(iter < setter)
	{
	  std::cout<<"Term #"<<iter<<" multiplier: ";
	  std::cin>>testvectortwo[iter];
	  std::cout<<"Degree: ";
	  std::cin>>testvectortwo[iter+1];
	}
      else
	{
	  if(toggle_fill < iter)
	    {
	      toggle_fill = 0;
	      std::cout<<"Set number of elements to fill: ";
	      std::cin>>toggle_fill;
	      toggle_fill+=setter;
	      std::cout<<"Set starting degree: ";
	      std::cin>>testvectortwo[iter+1];
	      std::cout<<"Set multiplier: ";
	      std::cin>>testvectortwo[iter];
	      if(toggle_fill==0)
		{
		  std::cout<<"New number of values to set manually: ";
		  std::cin>>setter;
		}
	    }
	  else
	    {
	      testvectortwo[iter] = testvectortwo[iter-2];
	      testvectortwo[iter+1] = testvectortwo[iter-1]+1;
	    }
	}
    }
  polynomial<double> writtenpoly(testvectortwo, length);
  delete[] testvectortwo;
  return writtenpoly;
}

template<class T = double, class distribution = std::uniform_real_distribution<double>, class engine = std::default_random_engine>
std::vector<T> make_simple_vector(dimi r)
{
  std::vector<T> result;
  result.reserve(r);
  dimi o;
  std::cout<<"Random (0 for yes)? ";
  std::cin>>o;
  if(o)
    {
      T temp;
      for(dimi k = 0; k < r; k++)
	{
	  std::cout<<"Input element #"<<k<<":";
	  std::cin>>temp;
	  result.push_back(temp);
	}
    }
  else
    {
      engine generator;
      std::cout<<"Input bounds: ";
      T A,B;
      std::cin>>A>>B;
      distribution values(std::min(A,B),std::max(A,B));
      for(dimi k = 0; k < r; k++)
	{
	  result.push_back(values(generator));
	}
    }
  return result;
}

template <class T>
void evaluationtester(polynomial<T>& polyref)
{
   T testval = 0;
   while(testval == 0)
     {
       std::cout<<"Evaluate polynomial for values: ";
       std::cin>>testval;
       std::cout<<"Result: "<<polyref.evaluate(testval)<<"\n";
       std::cout<<"Continue (press 0)? ";
       std::cin>>testval;
     }
}

void basic_tensor_test()
{
  double testvector[5] = {5,4,3,2,1};
  rvector<double> vectorone (testvector, 3);
  std::cout<<vectorone.elements[1]<<"\n"; // Test # 1
  rvector<double> vectortwo (vectorone); // Test # 2
  std::cout<<vectorone.print()<<"\n"; // Test # 3 (HALF COMPLETE)
  vectorone+=vectortwo; // Test # 4.1
  std::cout<<vectorone.print()<<"\n"; // Test # 4.2
  std::cout<<vectorone.printdetails()<<"\n";
  vectorone*=vectortwo;
}

void basic_vector_test()
{
  double testvector[5] = {5,4,3,2,1};
  std::cout<<"Part 2: basic simple vector functions:\n";
  realvector<double> vectorthree (testvector, 4);
  std::cout<<vectorthree.print()<<"\n";
  realvector<double> vectorfour (vectorthree);
  std::cout<<vectorfour.print()<<"\n";
  vectorfour*= 1000;
  std::cout<<vectorfour.print()<<"\n";
  int cycles = 0;
  while(cycles == 0)
    {
      std::cout<<"Input vector addition cycles: ";
      std::cin>>cycles;
      for(int iter = 0; iter < cycles; iter++)
	{
	  vectorthree+=vectorthree;
	  vectorthree+=vectorfour;
	  vectorthree*=-1;
	  vectorthree+=vectorthree;
	  vectorthree*=0.5;
	}
      std::cout<<"Result: "<<vectorthree.print()<<"\n";
      std::cout<<"Continue (press 0)? ";
      std::cin>>cycles;
    }
}

void basic_polynomial_test()
{
  double testvector[5] = {5,4,3,2,1};
  std::cout<<"Part 4: basic polynomials\n";
  polynomial<double> polynomialone (testvector, 4);
  std::cout<<polynomialone.print()<<"\n\n";
  std::cout<<polynomialone.printdetails()<<"\n";
  std::cout<<polynomialone.print("z")<<"\n";
  polynomial<double> polynomialtwo = polynomialone.derivative();
  std::cout<<polynomialtwo.print()<<"\n";
  evaluationtester(polynomialtwo);
}

void custom_polynomial_test()
{
  int length = 0;
  while(length == 0)
    {
      std::cout<<"Input test polynomial length: ";
      std::cin>>length;
      polynomial<double> polytotest = simplepolynomialwriter(length);
      if(!((length%2)==0))
	{
	  length++;
	}
      std::cout<<"Resulting polynomial: "<<polytotest.printdetails()<<"\n";
      std::cout<<"Re-enter polynomial (press 0 for yes)? ";
      std::cin>>length;
      if(length==0)
	{
	  continue;
	}
      evaluationtester(polytotest);
      std::cout<<"Re-enter a new polynomial (press 0 for yes)? ";
      std::cin>>length;
    }
}

void matrix_test()
{
  double testmatrix[16] = {1,2,3,4,1,2,3,4,2,3,5,7,4,6,10,14};
  rvector<double> matrixtest (testmatrix, 4, 4);
  rvector<double> matrixtesttwo (testmatrix, 2, 8);
  std::cout<<"Matrices:\n"<<matrixtest.printdetails()<<"\n"<<matrixtesttwo.printdetails()<<"\n";
  rvector<double> matrixtestthree (matrixtest);
  std::cout<<"Copied matrix:\n"<<matrixtestthree.print()<<"\n";

  // Goal syntax:
  
  //matrixtest+=matrixtest;
  //matrixtesttwo+=matrixtest; // Check output for properly indicated error!
  //matrixtest*=5;
  //std::cout<<"Results:\n"<<matrixtest.printdetails()<<"\n"<<matrixtesttwo.printdetails()<<"\n";
  //// Check matrix multiplication
  //matrixtest*=matrixtest;
  //matrixtest*=matrixtestthree;
}

void variable_test()
{
  variable x ("x");
  variable y ("y");
  // GOAL SYNTAX
  y = x; // Pointer-passing symbolic value setting
  variable z (y); // Constructing z as a copy of y with name "y"
  y = z.evaluate(); // Evaluating z (should return the arbitrary variable x) and setting this value to y by pointer passing
  if(y == x) // Checking if y and x point to the same thing (x points to arbitrary x (NULL), and y points to x, therefore, they point to the same thing, as the pointers should "fall through" in checking)
  {
   std::cout<<"TRUE!\n"; // Indicate successful check. This should happen if the above syntax works correctly
  }
  y.arbitrary(); // Resets y to an arbitrary variable again
  z.arbitrary();
  double testvalues[8] = {4,4,2,4,1,3,5,7.9};
  polynomial<double> testpoly (testvalues, 8, y);
  polynomial<double> testpolytwo (testvalues, 8);
  testpolytwo*=2.56;
  std::cout<<testpolytwo.print()<<"\n";
  std::cout<<testpoly.print()<<"\n";
  testpolytwo.setVariable(y);
  std::cout<<testpolytwo.print()<<"\n";
  testpoly.setVariable(x);
  std::cout<<testpoly.print()<<"\n";
  // GOAL SYNTAX
  //polynomial<double> testpolythree = testpolytwo.evaluate(z); // Should evaluate testpolytwo for z, returning a polynomial in z
  //std::cout<<testpolythree.evaluate()<<"\n"; // Evaluates testpolytwo for the VALUE of its variable, z. If NULL, behavior (currently) undefined. If no value, behavior currently undefined.
}


template<class T = double, class distribution = std::uniform_real_distribution<double>, class engine = std::default_random_engine>
smatrix<T> make_auto_matrix(int rows, int cols, T low = -10, T high = 10)
{
  std::vector<T> matrixvalues;
  matrixvalues.reserve(rows*cols);
  engine generator;
  distribution values(low,high);
  for(int iter = 0; iter < rows; iter++)
    {
      for(int miniiter = 0; miniiter < cols; miniiter++)
	{
	  matrixvalues.push_back(values(generator));
	}
    }
  smatrix<T> result (matrixvalues, cols);
  return result;
}

template<class T = double>
smatrix<T> make_simple_matrix(dimi rows, dimi cols)
{
  dimi in;
  std::cout<<"Random (0 for yes)? ";
  std::cin>>in;
  if(!in)
    {
      T a;
      T b;
      std::cout<<"Range (a,b): ";
      std::cin>>a>>b;
      return make_auto_matrix(rows,cols,std::min(a,b),std::max(a,b));
    }
  std::vector<T> matrixvalues;
  matrixvalues.reserve(rows*cols);
  std::cout<<"Input values:\n";
  for(dimi iter = 0; iter < rows; iter++)
    {
      for(dimi miniiter = 0; miniiter < cols; miniiter++)
	{
	  T temp;
	  std::cout<<"Input element ("<<(iter+1)<<","<<(miniiter+1)<<"): ";
	  std::cin>>temp;
	  matrixvalues.push_back(temp);
	}
    }
  smatrix<T> result (matrixvalues, cols);
  return result;
}

template<class elementType = double, class CT = std::vector<elementType>>
void simple_matrix_test()
{
  int r, c, s;
  std::cout<<"Skip basic test? If yes, press 0: ";
  std::cin>>s;
  std::cout<<"Simple matrix test:\nInput number of rows, then number of columns\n";
  std::cin>>r;
  std::cin>>c;
  smatrix<elementType,CT> testmatrix;
  if(s!=0)
    {
      std::cout<<"Input matrix:\n";
      testmatrix = make_simple_matrix<elementType>(r,c);
      std::cout<<"Resulting matrix:\n"<<testmatrix.print()<<"\n";
      std::cout<<"Trace:"<<testmatrix.tr()<<"\n";

    }
  std::cout<<"Input matrix:\n";
  smatrix<elementType,CT> testmatrixtwo = make_simple_matrix<elementType>(r,c);
  std::cout<<"Resulting matrix:\n"<<testmatrixtwo.print()<<"\n";
  if(!(s==0))
    {
      testmatrix += testmatrixtwo;
      std::cout<<"Addition =\n"<<testmatrix.print()<<"\n";
      testmatrix *= 53.241;
      std::cout<<"Scalar multiplication =\n"<<testmatrix.print()<<"\n";
    }
  std::cout<<"Input compatible matrix dimension: ";
  std::cin>>r;
  std::cout<<"Input matrix:\n";
  smatrix<elementType,CT> multiplybymatrix = make_simple_matrix(c,r);
  std::cout<<"Resulting matrix:\n"<<multiplybymatrix.print()<<"\n";
  smatrix<elementType,CT> result = testmatrixtwo * multiplybymatrix;
  if(s)
    {
      std::cout<<"Second matrix * third matrix: \n"<<result.print()<<"\n";
    }
  else
    {
       std::cout<<"First matrix * second matrix: \n"<<result.print()<<"\n";
    }
  smatrix<elementType,CT> result2 = result;
  result2*=0;
  /*
  matrixOps::arrayMultiplication(testmatrixtwo.elements,multiplybymatrix.elements,result2.elements,testmatrixtwo.rows(),testmatrixtwo.cols(),multiplybymatrix.cols());
  std::cout<<"Method 2: \n"<<result2.print()<<"\n";
  */
  result = Outer(testmatrixtwo, multiplybymatrix);
  std::cout<<"Outer product: \n"<<result.print()<<"\n";
  std::cout<<"Determinant of:\n";
  std::cout<<"Right matrix: "<<testmatrixtwo.det()<<"\n";
  std::cout<<"Left matrix: "<<multiplybymatrix.det()<<"\n";
  std::cout<<"Outer product: "<<result.det()<<"\n";
}

template<class T = double>
void simple_matrix_transpose_test()
{
  dimi c, r;
  std::cout<<"Transpose test: "
      <<"\nInput matrix dimensions:";
  std::cin>>r;
  std::cin>>c;
  smatrix<T> testmatrix = make_simple_matrix(r, c);
  std::cout<<"Matrix:\n"<<testmatrix.print();
  smatrix<T> transposetest = testmatrix.transpose();
  std::cout<<"\nTranspose:\n"<<transposetest.print()<<"\n";
}

template<class T = double>
void simple_matrix_elrop_test()
{
  dimi c, r;
  T w;
  std::cout<<"Elementary operations test: "
      <<"\nInput matrix dimensions:";
  std::cin>>r;
  std::cin>>c;
  smatrix<T> testmatrix = make_simple_matrix(r, c);
  std::cout<<"Matrix:\n"<<testmatrix.print()<<"\n";
  std::cout<<"Specify matrix operations by inputting two rows and a weight."
	   <<"\nSpecify two rows with a weight of '0' to flip them"
	   <<"\nSpecify one row, set the second row to '-1' and set a weight to multiply by that weight "
	   <<"\nSpecify both rows as '-1' and weight to multiply the whole matrix by that weight"
	   <<"\nSpecify two rows and a weight to add row 2 times the weight to row 1"
	   <<"\nFinally, simply replace rows with columns for column operations.\n\n";
  std::cout<<"Perform row operations? (0 for yes) ";
  std::cin>>r;
  while(!r)
    {
      std::cout<<"Press (0) for column operations: ";
      std::cin>>r;
      if(r)
	{
	  std::cout<<"Specify rows: ";
	  std::cin>>r;
	  std::cin>>c;
	  std::cout<<"Weight: ";
	  std::cin>>w;
	  testmatrix.elrop(r,c,w);
	  std::cout<<"Result:\n"<<testmatrix.print()<<"\n";
	}
      else
	{
	  std::cout<<"Flip columns: ";
	  std::cin>>r;
	  std::cin>>c;
	  std::cout<<"Weight: ";
	  std::cin>>w;
	  testmatrix.elcop(r,c,w);
	  std::cout<<"Result:\n"<<testmatrix.print()<<"\n";
	}
      std::cout<<"Press (0) to operate again: ";
      std::cin>>r;
    }
  testmatrix.row_reduce();
  std::cout<<"Row reduced echelon form:\n"<<testmatrix.print()<<"\n";
  std::cout<<"Echelonizing...\n";
  testmatrix.echelonize();
  std::cout<<"Row reduced echelon form:\n"<<testmatrix.print()<<"\n";
}

template<class T = double>
void rref_test()
{
  dimi c, r;
  //double w;
  std::cout<<"Row reduction test: "
      <<"\nInput matrix dimensions:";
  std::cin>>r;
  std::cin>>c;
  smatrix<T> testmatrix = make_simple_matrix(r, c);
  std::cout<<"Matrix:\n"<<testmatrix.print()<<"\n";
  testmatrix.row_reduce();
  std::cout<<"Row reduced form:\n"<<testmatrix.print()<<"\n";
  std::cout<<"Echelonizing...\n";
  testmatrix.echelonize();
  std::cout<<"Row reduced echelon form:\n"<<testmatrix.print()<<"\n";
}

template<class T = double>
void determinant_test()
{
  std::cout<<"Determinant Test:\nInput number of rows and columns: ";
  dimi r, c;
  std::cin>>r;
  std::cin>>c;
  smatrix<T> sample_matrix = make_simple_matrix(r,c);
  std::cout<<"Result (standard method): \n";
  std::cout<<sample_matrix.printdetails();
  /*
  std::cout<<"Pseudoelimination method:\n";
  std::cout<<"\nComputing...";
  double det = matrixDet::pseudoElimination<double>(sample_matrix);
  std::cout<<"\nResult: "<<det<<"\n";
  */
}

void auto_matrix_test()
{
  std::cout<<"Auto Matrix Test:\nInput number of rows and columns: ";
  dimi r, c;
  std::cin>>r;
  std::cin>>c;
  smatrix<double> sample_matrix = make_auto_matrix(r,c);
  std::cout<<"Result: \n";
  std::cout<<sample_matrix.printdetails();
}

void matrix_addition_test()
{
  std::cout<<"Semi-symbolic matrix operation test:\nInput number of rows and columns: ";
  dimi r,c;
  std::cin>>r;
  std::cin>>c;
  std::cout<<"Input matrix 1:\n";
  smatrix<double> matrix1 = make_simple_matrix(r,c);
  std::cout<<"Input matrix 2:\n";
  smatrix<double> matrix2 = make_simple_matrix(r,c);
  //auto result = matrix1 + matrix2;
  std::cout<<"\nMatrix 1:\n"<<matrix1.print()<<"\n";
  std::cout<<"\nMatrix 2:\n"<<matrix2.print()<<"\n";
  smatrix<double> matrix3 = matrix1 + matrix2;
  smatrix<double> matrix4 = matrix3 + matrix1 + matrix2;
  //matrix4 = matrix1 + matrix2 + matrix3;
  //std::cout<<"Re-added\n";
  std::cout<<"\nAddition Result:\n"<<matrix3.print()<<"\n";
  std::cout<<"\nRe-addition Result (should be addition result times 2):\n"<<matrix4.print()<<"\n";
  matrix3 = matrix1 * matrix2;
  std::cout<<"\nMultiplication Result:\n"<<matrix3.print()<<"\n";
}

void symbolic_tensor_test()
{
  dimi l;
  std::cout<<"Symbolic Vector Test\nInput vector size: ";
  std::cin>>l;
  double* testvector = new double[l];
  std::cout<<"Input vector elements: ";
  for (dimi iter = 0; iter < l; iter++)
    {
      std::cin>>testvector[iter];
    }
  rvector<double> vector1 (testvector,l);
  std::cout<<"Input vector 2 elements: ";
  for (dimi iter = 0; iter < l; iter++)
    {
      std::cin>>testvector[iter];
    }
  rvector<double> vector2 (testvector,l);
  delete[] testvector;
  rvector<double> vector3 = vector1;
  vector3+= vector2;
  std::cout<<"\n"<<vector1.print()<<" + "<<vector2.print()<<" = "<<vector3.print()<<"\n";
  //auto symbolic_expression_test = vector1 + vector2; //TODO: perform proper test
}

void submatrixops_test()
{
  
  int r, c, s;
  std::cout<<"Skip basic test? If yes, press 0: ";
  std::cin>>s;
  std::cout<<"Simple matrix test:\nInput number of rows, then number of columns\n";
  std::cin>>r;
  std::cin>>c;
  smatrix<long double> testmatrix = make_simple_matrix(r,c);
  if(!(s==0))
    {
      std::cout<<"COMPUTING...";
      long double trace = testmatrix.tr();
      long double det = testmatrix.det();
      std::cout<<"\n\n\n\n\n";
      std::cout<<"Resulting matrix:\n"<<testmatrix.print();
      std::cout<<"\nTrace: "<<trace;
      std::cout<<"\nDeterminant: "<<det;
    }
  s = 0;
  while(!s)
    {
      std::cout<<"\nInput row reduction parameters (starting row, starting column, ending row, ending column) :";
      std::cin>>r;
      std::cin>>c;
      std::cin>>s;
      int f;
      std::cin>>f;
      testmatrix.row_reduce(r,c,s,f);
      std::cout<<"Result: \n"<<testmatrix.print()<<"\n";
      std::cout<<"Input echelonization parameters: (starting row, starting column, ending row, ending column): ";
      std::cin>>r;
      std::cin>>c;
      std::cin>>s;
      std::cin>>f;
      testmatrix.echelonize(r,c,s,f);
      std::cout<<"Result: \n"<<testmatrix.print()<<"\n";
      std::cout<<"Perform more operations (0 for yes)?";
      std::cin>>s;
    }
}

template<class FT = double>
void fourier_test()
{
  dimi p;
  dimi s;
  std::vector<std::complex<FT>> x;
  std::cout<<"Input array size: 2^";
  std::cin>>p;
  s = 1 << p;
  x.reserve(s);
  std::cout<<"Shift? ";
  dimi sh;
  std::cin>>sh;
  s+=sh;
  std::cout<<"Input elements as (real,imag) or as real numbers: \n";
  for(dimi k = 0; k < s; k++)
    {
      std::complex<FT> temp;
      std::cout<<(k+1)<<" of "<<s<<": ";
      std::cin>>temp;
      x.push_back(temp);
    }
  auto X = x;
  std::cout<<"Input: <"<<X[0];
  for(dimi k = 1; k < s; k++)
    {
      std::cout<<","<<X[k];
    }
  std::cout<<">\n\n\n\n";
  std::chrono::time_point<std::chrono::high_resolution_clock> start_DFT, end_DFT;
  start_DFT = std::chrono::high_resolution_clock::now();
  simpleDFT::DFT(x.begin(),X.begin(),X.size());
  end_DFT = std::chrono::high_resolution_clock::now();
  std::cout<<"DFT Result: \n\n<"<<X[0];
  for(dimi k = 1; k < s; k++)
    {
      std::cout<<","<<X[k];
    }
  std::chrono::duration<double, std::nano> DFT_time = end_DFT - start_DFT;
  std::cout<<">\nWhich took "<<DFT_time.count()<<" nanoseconds to compute\n\n";
  if(!sh)
    {
      std::chrono::time_point<std::chrono::high_resolution_clock> start_FFT, end_FFT;
      start_FFT = std::chrono::high_resolution_clock::now();
      auto Y = radix2FFT::simpleFFT<decltype(x)>(x.begin(),s,1);
      end_FFT = std::chrono::high_resolution_clock::now();
      std::chrono::duration<double, std::nano> FFT_time = end_FFT - start_FFT;
      std::cout<<"FFT:";
      arrayPrinter::print_to(std::cout,Y)<<"\n";
      std::cout<<"Which took "<<FFT_time.count()<<" nanoseconds to compute\n";
      std::cout<<"Equal? "<<array_almost_equal(X.begin(),X.end(),Y.begin())<<"\n";
      auto I = radix2FFT::simpleFFT<decltype(Y),1>(Y.begin(),s,1);
      elementwiseScalarMultiplication::op(I,(1/static_cast<double>(s)));
      std::cout<<"Inverse FFT: ";
      arrayPrinter::print_to(std::cout,I)<<"\n";
      FFT_time = DFT_time - FFT_time;
      std::cout<<"\n\nThe FFT is "<<FFT_time.count()<<" nanoseconds faster than the DFT for "<<Y.size()<<" elements.\n";
    }
}

void details()
{
  std::cout<<"Compiled on: "<<__DATE__<<" at "<<__TIME__
	   <<", with a compiler having C++ version "<<__cplusplus<<".\n"
	   <<"The source file used is "<<__FILE__
  #ifdef __TIMESTAMP__
	   <<", which was last modified on "<<__TIMESTAMP__
  #endif
	   <<"\n"
    /*
	   <<"Fast FMA is: "
  #ifdef FP_FAST_FMAF
	   <<"enabled!"
  #elseif FP_FAST_FMA
	   <<"enabled!"
  #elseif FP_FAST_FMAL
	   <<"enabled!"
  #else
	   <<"disabled!"
  #endif
    */
	   <<"\n";
  #ifdef _TMATLIB_USE_CPPZ_IF_AVAILABLE_
  std::cout<<"C++1z features will be used if the compiler indicates they are available\n"
	   <<"C++1z features are currently: "
  #ifdef _TMATLIB_CPPZ_ENABLED_
	   <<"Enabled!\n";
  #else
           <<"Disabled!\n";
  #endif
  #endif
  std::cout<<"\n";
  #ifdef _T_VEC_ENABLE_ALL_SCAFFOLDING_
  std::cout<<"All vector scaffolding enabled!\n";
  #endif
  #ifndef _T_VEC_ENABLE_ALL_SCAFFOLDING
  #ifdef _T_VEC_RREF_SCAFFOLDING_
  std::cout<<"rref scaffolding enabled!\n";
  #endif
  #ifdef _T_VEC_SYMB_SCAFFOLDING_
  std::cout<<"Vector symbolics scaffolding enabled!\n";
  #endif
  #ifdef _T_VEC_OPS_SCAFFOLDING_
  std::cout<<"Vector operation scaffolding enabled!\n";
  #endif
  #endif
  #ifdef _T_SYMB_ENABLE_ALL_SCAFFOLDING_
  std::cout<<"All symbolics scaffolding enabled!\n";
  #endif
  #ifndef _T_SYMB_ENABLE_ALL_SCAFFOLDING_
  #ifdef _T_SYMB_SCAFFOLDING_ON
  std::cout<<"Symbolics scaffolding on\n";
  #endif
  #ifdef _T_SYMB_OPER_SCAFFOLDING_
  std::cout<<"Symbolic operation scaffolding enabled!\n";
  #endif
  #endif
}

void integration_test()
{
  long double testvalues[6] = {2,3,4,5,3,2};
  polynomial<long double> testpoly (testvalues, 6);
  std::cout<<"Polynomial: "<<testpoly.printdetails()<<"\n";
  std::cout<<"Input integral bounds: ";
  double a, b;
  std::cin>>a;
  std::cin>>b;
  std::cout<<"Numerical Integral: "<<numint(testpoly,a,b,0.01,true)<<"\n";
}

void elementwise_addition_test()
{
  realvector<double> u = {5,4,3,2,1};
  realvector<double> v = {1,2,3,4,5};
  realvector<double> w;
  w.swapcontainer(elementwiseAddition::op(u.elements, v.elements));
  std::cout<<u.print()<<" + "<<v.print()<<" = "<<w.print()<<"\n";
  w+=u;
  std::cout<<"Result: "<<w.print()<<"\n";
  w = u + v;
  std::cout<<"Result: "<<w.print()<<"\n";
}

void basic_linear_systems_test()
{
  dimi c = 0, r;
  double w;
  std::cout<<"Basic linear systems test\n"
	   <<"Input number of variables: ";
  while(!c)
    {
      std::cin>>c;
      if(!c)
	{
	  std::cout<<"Error! Cannot have 0 variables! Please re-enter your desired number of variables.\n";
	}
    }
  std::cout<<"Input number of equations: ";
  std::cin>>r;
  c++;
  long double* data = new long double[(r*c)];
  for(dimi iter = 0; iter < r; iter++)
    {
      std::cout<<"Input equation #"<<r<<":\n";
      std::cout<<"__t1";
      for(dimi miniiter = 1; miniiter < (c-1); miniiter++)
	{
	  std::cout<<" + _t"<<(miniiter+1);
	}
      std::cout<<" = _\n";
      std::cout<<"Input values: ";
      for(dimi miniiter = 0; miniiter < c; miniiter++)
	{
	  std::cin>>data[miniiter+(iter*c)];
	}
      std::cout<<"Equation: "
	       <<data[iter*c]<<"t1";
	for(dimi miniiter = 1; miniiter < (c-1); miniiter++)
	{
	  std::cout<<" + "<<data[miniiter+(iter*c)]<<"t"<<(miniiter+1);
	}
	std::cout<<" = "<<data[(iter*c)+c-1]<<"\n\n";
    }
  asmatrix<long double> testmatrix (data, r, c, (c-1));
  std::cout<<"Matrix:\n"<<testmatrix.print()<<"\n";
  std::cout<<"Specify matrix operations by inputting two rows and a weight."
	   <<"\nSpecify two rows with a weight of '0' to flip them"
	   <<"\nSpecify one row, set the second row to '-1' and set a weight to multiply by that weight "
	   <<"\nSpecify both rows as '-1' and weight to multiply the whole matrix by that weight"
	   <<"\nSpecify two rows and a weight to add row 2 times the weight to row 1"
	   <<"\nFinally, simply replace rows with columns for column operations.\n\n";
  std::cout<<"Perform row operations? (0 for yes) ";
  std::cin>>r;
  while(!r)
    {
      std::cout<<"Press (0) for column operations: ";
      std::cin>>r;
      if(r)
	{
	  std::cout<<"Specify rows: ";
	  std::cin>>r;
	  std::cin>>c;
	  std::cout<<"Weight: ";
	  std::cin>>w;
	  testmatrix.elrop(r,c,w);
	  std::cout<<"Result:\n"<<testmatrix.print()<<"\n";
	}
      else
	{
	  std::cout<<"Flip columns: ";
	  std::cin>>r;
	  std::cin>>c;
	  std::cout<<"Weight: ";
	  std::cin>>w;
	  testmatrix.elcop(r,c,w);
	  std::cout<<"Result:\n"<<testmatrix.print()<<"\n";
	}
      std::cout<<"Press (0) to operate again: ";
      std::cin>>r;
    }
  testmatrix.rref_right();
  std::cout<<"Solved form:\n"<<testmatrix.print()<<"\n";
  delete[] data;
}

void refactored_elrops_test()
{
  std::cout<<"Input rows and columns: ";
  dimi r,c;
  dimi beam,swap;
  double weight;
  std::cin>>r;
  std::cin>>c;
  std::cout<<"\nInput matrix elements:\n";
  smatrix<double> A = make_simple_matrix(r,c);
  auto B = A;
  std::cout<<"Result: \n"<<A.print()<<"\n";
  r = 0;
  while(!r)
    {
      std::cout<<"Row operation (not 0), or column (0)? ";
      std::cin>>c;
      std::cout<<"Input ELOP (Beam,Swap,Weight): \nInput parameter beam: ";
      std::cin>>beam;
      std::cout<<"Input parameter swap: ";
      std::cin>>swap;
      std::cout<<"Input parameter weight: ";
      std::cin>>weight;
      if(!c)
	{
	  A.elsbop(beam,swap,weight);
	  elops_on_container<_STANDARD_ELOP_FUNCTIONS_>::elsbop(B.elements,beam,swap,weight,B.rows(),B.cols());
	}
      else
	{
	  A.elbop(beam,swap,weight);
	  elops_on_container<_STANDARD_ELOP_FUNCTIONS_>::elbop(B.elements,beam,swap,weight,B.rows(),B.cols());
	}
      std::cout<<"Results: \n\nNORMAL\n\n"<<A.print()<<"\n\nREFACTORED\n\n"<<B.print()<<"\n";
      std::cout<<"Perform another ELOP (0 for yes)? ";
      std::cin>>r;
    }
}

void refactored_rref_test()
{
  std::cout<<"Input rows and columns: ";
  dimi r,c;
  //dimi beam,swap;
  //double weight;
  std::cin>>r;
  std::cin>>c;
  std::cout<<"\nInput matrix elements:\n";
  smatrix<double> A = make_simple_matrix(r,c);
  std::cout<<"Result: \n"<<A.print()<<"\n";
  std::chrono::time_point<std::chrono::high_resolution_clock> start_old, end_old, start_new, end_new;
  auto B = A;
  start_old = std::chrono::high_resolution_clock::now();
  auto X = B.det();
  end_old = std::chrono::high_resolution_clock::now();
  B.row_reduce();
  start_new = std::chrono::high_resolution_clock::now();
  auto E = A.elements;
  det_tagalong<double> x;
  matrixRRF::elimination_reduce(E,A.rows(),A.cols(),x,0,0,A.rows(),A.cols());
  x.det_value=matrixDeterminant::multiplyDiagonal<double>(E,r)/(x.det_value);
  end_new = std::chrono::high_resolution_clock::now();
  std::swap(E,A.elements);
  std::chrono::duration<double, std::micro> DET_time = end_old - start_old;
  std::cout<<"Old Result: \n"<<B.print()<<"\n";
  std::cout<<"Old determinant: "<<X<<"\n"
	   <<"Computed in: "<<DET_time.count()<<" microseconds\n";
  std::cout<<"New Result: \n"<<A.print()<<"\n";
  DET_time = end_new - start_new;
  std::cout<<"New determinant: "<<x.det_value<<"\n"
	   <<"Computed in: "<<DET_time.count()<<" microseconds\n";
}

void equality_test()
{
  realvector<double> x = {3,4,5,6};
  realvector<double> y = {7,9,3,2};
  realvector<double> z = x;
  realvector<int> t = {3,4,5,6};
  std::cout<<"x = y (no)? "<<(x==y)<<"\n"
	   <<"x = z (yes)? "<<(x==z)<<"\n"
	   <<"x = t (yes)? "<<(x==t)<<"\n";
  smatrix<double> A ({1,2,3,4,5,6,7,8,9},3);
  smatrix<double> B ({2,3,4,5,6,7,8,9,10},3);
  smatrix<double> C = A;
  smatrix<double> D ({1,2,3,4,5,6,7,8,9},6);
  smatrix<double> E ({1,0,0,0,1,0,0,0,1},3);
  smatrix<double> F = E * A;
  E = F;
  E.hard_flip();
  E.flip();
  //E+=E;
  std::cout<<"A = B (no)? "<<(A==B)<<"\n"
	   <<"A = C (yes)? "<<(A==C)<<"\n"
	   <<"A = D (no)? "<<(A==D)<<"\n"
	   <<"A = E (yes)? "<<(A==E)<<"\n"
	   <<"A = F (yes)? "<<(A==F)<<"\n";
}

void unit_test_test()
{
  auto_equal_check<double,double,quadratic_functor> auto_quadratic_test ({1,2,3,4,5},{1,4,9,16,25});
  snum a,b,c;
  std::cout<<"Auto equal result: "<<auto_quadratic_test(a,b,c)<<"\n";
  unit_test<decltype(auto_quadratic_test),double> quadratic_test ("Quadratic Test", auto_quadratic_test);
  time_result_series<std::chrono::high_resolution_clock::time_point> d;
  //std::cout<<"Test result: "<<quadratic_test.run_test()<<"\n";
  auto_quadratic_test(d,b,std::cout,true);
  auto_quadratic_test.set_testid("Quadratic Test");
  auto_quadratic_test(d,b,std::cout,true);

  auto_equal_check<std::vector<std::complex<double>>,std::vector<std::complex<double>>,fast_fourier_functor,double_equality> auto_fourier_test;
  auto_fourier_test.auto_inputs.push_back({1,2,3,4,5,6,7,8});
  auto_fourier_test.auto_inputs.push_back({0,1,0,1,0,1,0,1});
  auto_fourier_test.auto_inputs.push_back({1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16});
  slow_fourier_functor F;
  auto_fourier_test.fillResults(F);
  std::cout<<"Auto fourier result: "<<auto_fourier_test(d,b,std::cout,true)<<"\n";
}

template<class T = double>
void printer_test()
{
  std::cout<<"Printer Test\n";
  std::cout<<"Input rows and columns: ";
  dimi r,c;
  std::cin>>r;
  std::cin>>c;
  std::cout<<"Input matrix:\n";
  smatrix<T> E = make_simple_matrix<T>(r,c);
  std::cout<<"Transpose (1 or 0)? ";
  bool t;
  std::cin>>t;
  E.direction = !t;
  std::cout<<"Internal printer:\n"<<E.print()<<"\n";
  std::cout<<"External printer:\n";
  matrixPrinter::print_to(std::cout,E.elements,E.split,!(E.direction));
  std::cout<<"\n";
  std::cout<<"Array style:\n";
  arrayPrinter::print_to(std::cout,E.elements);
  std::cout<<"\nVector style:\n";
  arrayPrinter::print_to<vectorStyle>(std::cout,E.elements);
  std::cout<<"\nEquation style:\n";
  matrixPrinter::print_to<linearSystemStyle<'x'>>(std::cout,E.elements,E.split,!(E.direction));
  std::cout<<"\nPrinting the actual matrix:\n";
  std::cout<<E<<"\n";
}

template<class T = double>
void inversion_test()
{
  std::cout<<"Inversion test:\n"
	   <<"Input rows: ";
  dimi r;
  std::cin>>r;
  std::cout<<"Input matrix:\n";
  smatrix<T> A = make_simple_matrix<T>(r,r);
  std::cout<<"\nResulting matrix:\n"<<A<<"\n";
  smatrix<T> B = A;
  smatrix<T> C = A;
  smatrix<T> I = identitymatrix(r,r);
  std::cout<<"Identity test:\n"<<I<<"\n";
  inversion_tagalong<T*> inversiontag (I.elements.data(),I.split);
  inversion_tagalong<T*> reductiontag (B.elements.data(),C.split);
  matrixRRF::elimination_reduce(C.elements,C.rows(),C.cols(),reductiontag,0,0,C.rows(),C.cols());
  std::cout<<"Test Augmented Reduction:\n"<<C<<"\n===\n"<<B<<"\n";             ;
  matrixEchelonization::echelonize_reduced(C.elements,C.rows(),C.cols(),reductiontag,0,0,C.rows(),C.cols());
  std::cout<<"Test Echelonization:\n"<<C<<"\n==\n"<<B<<"\n";
  
  matrixRRF::elimination_reduce(A.elements,A.rows(),A.cols(),inversiontag,0,0,A.rows(),A.cols());
  std::cout<<"Augmented Reduction:\n"<<A<<"\n---\n"<<I<<"\n";
  matrixEchelonization::echelonize_reduced(A.elements,A.rows(),A.cols(),inversiontag,0,0,A.rows(),A.cols());
  std::cout<<"Inversion:\n"<<A<<"\n--\n"<<I<<"\n";
}

template<class T = double>
void rotation_test()
{
  std::cout<<"Rotation test:\nInput rows and columns: ";
  dimi r,c;
  std::cin>>r>>c;
  std::cout<<"Input matrix:\n";
  smatrix<T> A = make_simple_matrix<T>(r,c);
  std::cout<<"\nResulting matrix:\n"<<A<<"\n";
  std::vector<T> trig;
  trig.assign(r,0);
  r = 0;
  while(!r)
    {
      std::cout<<"Input zeroing command (source row, 1 above zeroing row, zero column): ";
      std::cin>>r>>c;
      matrixRotation::zero_rotate(A.elements.begin(),A.cols(),r,c);
      std::cout<<"\nResulting matrix:\n"<<A<<"\n";
      std::cout<<"Continue (press 0 for yes)?";
      std::cin>>r;
    }
}

template<class T = double>
void power_eigentest()
{
  std::cout<<"Convergence and Power Algorithm Test:\nInput rows and columns: ";
  dimi r,c;
  std::cin>>r>>c;
  std::cout<<"Input matrix:\n";
  smatrix<T> A = make_simple_matrix<T>(r,c);
  std::cout<<"\nResulting matrix:\n"<<A<<"\n";
  std::vector<T> IV (std::max(r,c),1);
  std::vector<T> AV (std::max(r,c),0);
  std::cout<<"First result: ";
  A(IV.begin(),AV.begin(),AV.size());
  arrayPrinter::print_to(std::cout,AV);
  std::cout<<"\nFloating Norm: "<<floatingNorm(AV.begin(),AV.size())<<"\nSimple Norm: "<<simpleNorm(AV.begin(),AV.size())
	   <<"\nDifference: "<<std::abs(floatingNorm(AV.begin(),AV.size())-simpleNorm(AV.begin(),AV.size()))<<"\n";
  iterativeConvergence::converge_data<normalizationStep>(IV.begin(),IV.size(),AV.begin(),A);
  std::cout<<"\nFinal eigenvector: ";
  arrayPrinter::print_to(std::cout,IV);
  A(IV.begin(),AV.begin(),AV.size());
  std::cout<<"\nFirst multiplication result: ";
  arrayPrinter::print_to(std::cout,AV);
  std::cout<<"\nEigenvalue: "<<((AV[0])/(IV[0]))<<"\n";
}

template<class T = double>
void sorting_test()
{
  std::cout<<"Input # of elements: ";
  dimi r;
  std::cin>>r;
  std::vector<T> elems;
  dimi k = 0;
  T temp;
  while(k < r)
    {
      std::cout<<"Input element #"<<(k+1)<<": ";
      std::cin>>temp;
      elems.push_back(temp);
      k++;
    }
  std::vector<T> D2 = elems;
  std::vector<T> D4 = elems;
  std::cout<<"Array: ";
  arrayPrinter::print_to(std::cout,elems)<<"\n";
  std::cout<<"Sorted arrays:\n";
  swapSort(elems,1);
  arrayPrinter::print_to(std::cout,elems)<<"\n";
  swapSort(D2,2);
  arrayPrinter::print_to(std::cout,D2)<<"\n";
  swapSort(D4,4);
  arrayPrinter::print_to(std::cout,D4)<<"\n";
  k = 0;
  while(!k)
    {
      std::cout<<"\n\nInput element to add to sorted elements: ";
      std::cin>>temp;
      sorted_insert(elems,temp);
      std::cout<<"Result: ";
      arrayPrinter::print_to(std::cout,elems)<<"\n";
      std::cout<<"Press 0 to continue: ";
      std::cin>>k;
    }
  std::vector<T> pairsorted;
  k = 0;
  T temp2;
  while(!k)
    {
      std::cout<<"Input pair to add to pairsorted elements: ";
      std::cin>>temp>>temp2;
      sorted_insert_first(pairsorted,temp,temp2);
      arrayPrinter::print_to(std::cout,pairsorted)<<"\n";
      std::cout<<"Press 0 to continue: ";
      std::cin>>k;
    }
  std::vector<T> pairsorted_insert;
  k = 0;
  while(!k)
    {
      std::cout<<"Input unsorted pair to add to pairsorted elements: ";
      std::cin>>temp>>temp2;
      pairsorted_insert.push_back(temp);
      pairsorted_insert.push_back(temp2);
      //sorted_insert_first(pairsorted,temp,temp2);
      arrayPrinter::print_to(std::cout,pairsorted_insert)<<"\n";
      std::cout<<"Press 0 to continue: ";
      std::cin>>k;
    }
  sorted_insert_pair_array(pairsorted,pairsorted_insert);
  std::cout<<"Result: ";
  arrayPrinter::print_to(std::cout,pairsorted)<<"\n";
}

template<class T = double>
void QR_test()
{
  std::cout<<"QR test:\nInput rows:";
  dimi r;
  std::cin>>r;
  dimi rows = r;
  std::cout<<"Input matrix:\n";
  smatrix<T> A = make_simple_matrix<T>(r,r);
  smatrix<T> ID = identitymatrix(r,r);
  auto I = ID;
  auto temp = I;
  std::cout<<"Print (0 for no)? ";
  dimi print;
  std::cin>>print;
  if(print)
    std::cout<<"\nResulting matrix:\n"<<A<<"\n";
  std::vector<T> trig;
  trig.assign(2*r,0);
  r = 0;
  dimi iter = 1;
  dimi toggle;
  dimi autotoggle;
  std::cout<<"Attempt Hessenberg form transformation? (0 for yes): ";
  std::cin>>toggle;
  if(!toggle)
    {
      if(print)
	{
	  to_hessenberg_form::convert(A.elements.begin(),A.cols(),trig.begin());
	  std::cout<<"\nResulting matrix:\n"<<A<<"\n";
	}
      else
	{
	  to_hessenberg_form::convert(A.elements.begin(),A.cols(),trig.begin(),simple_percent<decltype(std::cout)>(std::cout,rows));
	}
    }
  std::cout<<"Beginning QR algorithm: \n";
  std::cout<<"Manual (0 for yes)? ";
  std::cin>>toggle;
  if(!toggle)
    {
      dimi L;
      std::cout<<"View orthogonal and upper triangular (n for n iterations)? ";
      std::cin>>toggle;
      std::cout<<"Automatic (0 for yes)? ";
      std::cin>>autotoggle;
      std::cout<<"Input submatrix size: ";
      std::cin>>L;
      while(!r)
	{
	  std::cout<<"Iteration #"<<iter<<":\n";
	  iter++;
	  if(autotoggle)
	    {
	      if(toggle)
		std::cout<<"Converting to upper triangular form...";
	      matrixRotation::zero_rotate_right_diag(trig.begin(),A.elements.begin(),A.cols(),L);
	      if(toggle)
		{
		  std::cout<<"\nResulting matrix:\n"<<A<<"\n";
		  std::cout<<"Orthogonal matrix: ";
		  matrixRotation::rotate_read_transpose<true>(trig.begin(),I.elements.begin(),I.cols(),L);
		  std::cout<<"\n"<<I<<"\n";
		  I = ID;
		}
	      matrixRotation::rotate_read_transpose<true>(trig.begin(),A.elements.begin(),A.cols(),L);
	    }
	  else
	    {
	      QR_decomposition::step(A.elements.begin(),trig.begin(),A.cols(),L);
	    }
	  std::cout<<"RQ form: ";
	  std::cout<<"\n"<<A<<"\n";
	  std::cout<<"Continue (press 0 for yes)? ";
	  std::cin>>r;
	  if(toggle)
	    {
	      toggle--;
	    }
	}
    }
  else
    {
      std::vector<std::complex<T>> result;
      result.reserve(A.cols());
      std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
      start = std::chrono::high_resolution_clock::now();
      if(!print)
	{
	  QR_algorithm::write_eigenvalues<std::complex<T>>(std::back_inserter(result),A.elements.begin(),A.cols(),A.cols(),trig.begin(),QR_algorithm::log_check<>());
	}
      else
	{
	  QR_algorithm::write_eigenvalues<std::complex<T>>(std::back_inserter(result),A.elements.begin(),A.cols(),A.cols(),trig.begin());
	}
      end = std::chrono::high_resolution_clock::now();
      std::chrono::duration<double, std::micro> time = end - start;
      std::cout<<"Eigenvalues: ";
      arrayPrinter::print_to(std::cout,result)<<"\n\nTime: "<<time.count()<<" microseconds\n";
    }
}

template<class T = double>
void matrix_multiplication_test()
{
  std::chrono::time_point<std::chrono::high_resolution_clock> sdef,ssimp,spara,edef,esimp,epara;
  std::cout<<"Matrix multiplication test:\nInput rows and columns (r,c): ";
  dimi m,n;
  std::cin>>m>>n;
  std::cout<<"Input matrix:\n";
  smatrix<T> A = make_simple_matrix<T>(m,n);
  std::cout<<"Input compatible dimension: ";
  dimi p;
  std::cin>>p;
  std::cout<<"Input second matrix:\n";
  smatrix<T> B = make_simple_matrix<T>(n,p);
  std::cout<<"Print (0 for no)? ";
  dimi print;
  std::cin>>print;
  if(print)
      std::cout<<"Calculation: \n"<<A<<"\n * \n"<<B<<"\n";
  std::cout<<"Multiplication results:\n\n";
  sdef = std::chrono::high_resolution_clock::now();
  smatrix<T> def = A * B;
  edef = std::chrono::high_resolution_clock::now();
  if(print)
    std::cout<<"Default: "<<def<<"\n";
  ssimp = std::chrono::high_resolution_clock::now();
  smatrix<T> simp;
  simp.split = p;
  simp.elements.assign(m*p,0);
  matrix::simple_multiplication::multiply(simp.begin(),A.begin(),B.begin(),m,n,p);
  esimp = std::chrono::high_resolution_clock::now();
  spara = std::chrono::high_resolution_clock::now();
  smatrix<T> para;
  para.split = p;
  para.elements.assign(m*p,0);
  matrix::simple_iterator_multiplication::multiply(para.begin(),A.begin(),B.begin(),m,n,p);
  epara = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::micro> time = edef - sdef;
  if(print)
    {
      std::cout<<"Simple: "<<simp<<"\n";
      std::cout<<"Iterator: "<<para<<"\n";
    }
  std::cout<<"Times: \nDefault: "<<time.count()<<"\n";
  time = esimp - ssimp;
  std::cout<<"Simple: "<<time.count()<<"\n";
  time = epara - spara;
  std::cout<<"Iterator: "<<time.count()<<"\n";
  std::cout<<"Transpose results, without allocation (better situation): ";
  B.direction = !(B.direction);
  ssimp = std::chrono::high_resolution_clock::now();
  simp.elements.assign(m*p,0);
  matrix::simple_multiplication::multiply<true>(simp.begin(),A.begin(),B.begin(),m,n,p);
  esimp = std::chrono::high_resolution_clock::now();
  spara = std::chrono::high_resolution_clock::now();
  matrix::simple_iterator_multiplication::multiply<true>(para.begin(),A.begin(),B.begin(),m,n,p);
  epara = std::chrono::high_resolution_clock::now();
  if(print)
    {
      std::cout<<"Simple: "<<simp<<"\n";
      std::cout<<"Iterator: "<<para<<"\n";
    }
  std::cout<<"Times: \nDefault: "<<time.count()<<"\n";
  time = esimp - ssimp;
  std::cout<<"Simple: "<<time.count()<<"\n";
  time = epara - spara;
  std::cout<<"Iterator: "<<time.count()<<"\n";
  #ifdef CBLAS_H
  std::cout<<"BLAS comparison test: \n";
  ssimp = std::chrono::high_resolution_clock::now();
  simp.elements.assign(m*p,0);
  BLAS_interface::matrix::matrix_multiplication_kernel::op(simp.elements.data(),A.elements.data(),B.elements.data(),m,n,p,true,true,true);
  esimp = std::chrono::high_resolution_clock::now();
  time = esimp - ssimp;
  if(print)
    {
      std::cout<<"Result: "<<simp<<"\n";
    }
  std::cout<<"BLAS time: "<<time.count()<<"\n";
  #endif
  
}

void BLAS_test()
{
#ifdef _TMATLIB_BLAS_INTERFACE_INCLUDED_
  std::cout<<"BLAS operation test: \n";
  std::cout<<"FMA subtest: \n";
  std::cout<<"Input scalar: ";
  double scalar;
  std::cin>>scalar;
  std::cout<<"Input vector length: ";
  dimi r;
  std::cin>>r;
  std::chrono::time_point<std::chrono::high_resolution_clock> BLAS_start,BLAS_end,T_start,T_end;

  std::cout<<"Input vector 1: \n";
  realvector<double> u (make_simple_vector<double>(r));
  std::cout<<"Input vector 2: \n";
  realvector<double> v (make_simple_vector<double>(r));
  auto x = u;
  auto y = v;

  dimi o;
  std::cout<<"Print (0 for no)? ";
  std::cin>>o;
  
  T_start = std::chrono::high_resolution_clock::now();
  elementwiseFMA::FMA(u.elements.begin(),u.elements.end(),v.elements.begin(),scalar);
  T_end = std::chrono::high_resolution_clock::now();
  
  BLAS_start = std::chrono::high_resolution_clock::now();
  BLAS_interface::vector::FMA_kernel::op(x.elements.data(),y.elements.data(),scalar,x.elements.size());
  BLAS_end = std::chrono::high_resolution_clock::now();
  
  std::chrono::duration<double, std::nano> time = T_end - T_start;
  std::cout<<"tmatlib result: ";
  if(o)
    std::cout<<u<<" ,";
  std::cout<<"in time: "<<time.count()<<"\n";

  time = BLAS_end - BLAS_start;
  std::cout<<"BLAS result: ";
  if(o)
    std::cout<<x<<" ,";
  std::cout<<"in time: "<<time.count()<<"\n";

  std::cout<<"Equal: "<<array_almost_equal<10>(u.elements,x.elements)<<"\n";
  
#else
  std::cout<<"ERROR: BLAS interface not included at compile time. Please re-compile with interface to use this test\n";
#endif
}

void help()
{
  std::cout<<"tmatlib tester help:\n";
  std::cout<<"This program uses tmatlib to compile various small test programs to test the functionality of the library. May also be useful for multiplying matrices and computing determinants.\n";
}

void testnavigator()
{
  std::cout<<"Goto test# (0 for details, -1 for help): ";
  int testid;
  std::cin>>testid;
  switch(testid)
    {
    case -2:
      BLAS_test();
      break;
    case -1:
      help();
      break;
    case 0:
      details();
      break;
    case 1:
      basic_tensor_test();
      break;
    case 2:
      basic_vector_test();
      break;
    case 3:
      basic_polynomial_test();
      break;
    case 4:
      custom_polynomial_test();
      break;
    case 5:
      variable_test();
      break;
    case 6:
      matrix_test();
      break;
    case 7:
      std::cout<<"Precision (-2, -1, 0 or 1)? ";
      std::cin>>testid;
      switch(testid)
	{
	case 1:
	  simple_matrix_test<long double>();
	  break;
	case -1:
	  simple_matrix_test<float>();
	  break;
	case -2:
	  simple_matrix_test<int>();
	  break;
	default:
	  simple_matrix_test();
	  break;
	}
      break;
    case 8:
      simple_matrix_transpose_test();
      break;
    case 9:
      std::cout<<"Precision (-2, -1, 0, 1 or 2)? ";
      std::cin>>testid;
      switch(testid)
	{
	case 1:
	  simple_matrix_elrop_test<long double>();
	  break;
	case -1:
	  simple_matrix_elrop_test<float>();
	  break;
	case 2:
	  simple_matrix_elrop_test<std::complex<double>>();
	  break;
	case -2:
	  simple_matrix_elrop_test<int>();
	  break;
	default:
	  simple_matrix_elrop_test();
	  break;
	}
      break;
    case 10:
      std::cout<<"Precision (-2,-1, 0, 1 or 2)? ";
      std::cin>>testid;
      switch(testid)
	{
	case 0:
	  determinant_test();
	  break;
	case -1:
	  determinant_test<float>();
	  break;
	case 2:
	  simple_matrix_elrop_test<std::complex<double>>();
	  break;
	case -2:
	  determinant_test<int>();
	  break;
	default:
	  determinant_test<long double>();
	  break;
	}
      break;
    case 11:
      rref_test();
      break;
    case 12:
      auto_matrix_test();
      break;
    case 13:
      matrix_addition_test();
      break;
    case 14:
      symbolic_tensor_test();
      break;
    case 15:
      integration_test();
      break;
    case 16:
      elementwise_addition_test();
      break;
    case 17:
      basic_linear_systems_test();
      break;
    case 18:
      submatrixops_test();
      break;
    case 20:
      fourier_test();
      break;
    case 21:
      refactored_rref_test();
      break;
    case 22:
      equality_test();
      break;
    case 23:
      unit_test_test();
      break;
    case 24:
      printer_test();
      break;
    case 25:
      inversion_test();
      break;
    case 26:
      simple_matrix_test<double,std::valarray<double>>();
      break;
    case 27:
      rotation_test();
      break;
    case 28:
      power_eigentest();
      break;
    case 29:
      sorting_test();
      break;
    case 30:
      QR_test();
      break;
    case 31:
      matrix_multiplication_test();
      break;
    default:
      std::cout<<"Error! Invalid Test ID!\n";
    }
}

int main()
{
  std::cout<<"Welcome to the tmatlib testing utility!\n";
  details();
  std::cout<<"Initiating test routines\n\n";
  int testing = 0;
  while(testing == 0)
    {
      testnavigator();
      std::cout<<"Navigate to another test (press 0 for yes)? ";
      std::cin>>testing;
    }
  /*rvector<double> vectorthree (vectortwo);
  vectorthree = vectortwo + vectorone; // Test # 5.1
  std::cout<<vectorthree.print()<<"\n"; // Test # 5.2
  vectorthree*=7; // Test # 6.1
  std::cout<<vectorthree.print()<<"\n"; // Test # 6.2
  vectorthree = (2 * vectorthree) + ((1 * vectorone) + (vectorone + vectortwo)); // Test # 7.1
  std::cout<<vectorthree.print()<<"\n"; // Test # 7.2
  std::cout<<(vectorthree*vectorone)<<"\n"; // Test # 8
  polynomial<double> polyone (testvector, 4); // Test # 9
  std::cout<<polyone.print()<<"\n"; // Test # 10
  polyone+=polyone; // Test # 11.1
  std::cout<<polyone.print()<<"\n";*/ // Test # 11.2
  return 0;
}
