// Functions to allow easily printing internal data and mathematical objects to strings and output streams, including files

// Header guard. Skips over entire header if already included
#ifndef _T_PRNT_INCLUDED_
#define _T_PRNT_INCLUDED_

// Standard library dependencies
#include <string>
#include <sstream>
#include <type_traits>

// Internal dependencies
#include "utility.hpp"

struct Style
{
};

struct arrayStyle: public Style
{
  inline static std::string begin() {return "{";}
  inline static std::string afterNum(dimi N) {return "";}
  inline static std::string end() {return "}";}
  inline static std::string sepr() {return ",";}
};

struct vectorStyle: public arrayStyle
{
  inline static std::string begin() {return "<";}
  inline static std::string afterNum(dimi N) {return "";}
  inline static std::string end() {return ">";}
};

struct dynamicCharacter
{
  void used() {}
  void reset() {}
  void getChar() {}
  
};

template<class DT>
void resetChar(DT& x) {x.reset();}

void resetChar(const char& x) {}
  
template<class sourceType = std::string>
struct variableCharacter: public dynamicCharacter
{
  sourceType source;
  dimi size;
  dimi index;
  decltype(source[0]) getDirectChar(dimi n) {return source[n];}
  decltype(source[0]) getChar(dimi n)
  {
    dimi i = n%size;
    return getDirectChar(i);
  }
  decltype(source[0]) getChar() {return getDirectChar(index);}
  variableCharacter(sourceType input): source(input), index(0) {size = input.size();}
  void used() {index++; index = std::min((index%size),index);}
  void reset() {index = 0;}
};

template<>
struct variableCharacter<char>: public dynamicCharacter
{
  char varname;
  char index;
  char size;
  std::string getDirectChar(dimi n) {return (numtostring(varname) + numtostring(n+1));}
  std::string getChar(dimi n) {return getDirectChar(n);}
  std::string getChar() {return getDirectChar(index);}
  variableCharacter(char input): varname(input), index(0) {}
  void used() {
    index++;
  }
  void reset() {index = 0;}
};

template<char var = 'x'>
struct equationStyle: public Style
{
  inline static std::string begin() {return "";}
  inline static variableCharacter<char> afterNum(dimi N) {
    return var;
  }
  inline static std::string end() {return "";}
  inline static std::string sepr() {return " + ";}
};


template<class ST>
std::ostream& operator<<(std::ostream& osObject, variableCharacter<ST>& C)
{
  osObject<<(C.getChar());
  C.used();
  return osObject;
}


struct arrayPrinter
{
  template<class streamType, class inputType, class CT1, class CT2, class CT3, class CT4>
  inline static streamType& print_iter_to(streamType& printer, inputType PX, dimi N, CT1 begin, CT2 sepr,CT3 end, CT4 afternum)
  {
    printer<<(begin)<<(*PX)<<afternum;
    PX++;
    for(dimi iter = 1; iter < N; iter++)
      {
	printer<<(sepr)<<(*PX)<<afternum;
	PX++;
      }
    printer<<(end);
    return printer;
  }
  template<class style_used = arrayStyle, class streamType, class inputType>
  inline static streamType& print_iter_to(streamType& printer, inputType PX, dimi N)
  {
    return print_iter_to(printer,PX,N,style_used::begin(),style_used::sepr(),style_used::end(),style_used::afterNum(N));
  }
  template<class style_used = arrayStyle, class streamType, class inputType>
  inline static streamType& print_to(streamType& printer, const inputType& X, dimi N = 0)
  {
    if(N==0)
      {
	N = container_size(X);
      }
    auto PX = container_beginning(X);
    return print_iter_to<style_used>(printer,PX,N);
  }
  template<class style_used = arrayStyle, class streamType, class inputType>
  inline static streamType& print_to(streamType& printer, const inputType& X, dimi N, dimi shift)
  {
    if(N==0)
      {
	dimi K = container_size(X);
	if(K > shift)
	  {
	    N = K - shift;
	  }
	else
	  {
	    return printer; // Do nothing
	  }
      }
    auto PX = std::next(container_beginning(X),shift);  
    return print_iter_to<style_used>(printer,PX,N);
  }
  template<class style_used = arrayStyle, class streamType = std::ostringstream, class arrayType>
  inline static std::string print(const arrayType& X, dimi N = 0)
  {
    streamType printer;
    return (print_to<style_used>(printer,X,N)).str();
  }
  template<class style_used = arrayStyle, class streamType = std::ostringstream, class arrayType>
  inline static std::string print(const arrayType& X, dimi shift, dimi N = 0)
  {
    streamType printer;
    return (print_to<style_used>(printer,X,shift,N)).str();
  }
};

struct matrixStyle: public arrayStyle
{
  typedef arrayStyle subStyle;
  typedef arrayPrinter subPrinter;
  inline static std::string sepr() {return ("\n" + arrayStyle::sepr());}
  inline static std::string transpose_indicator() {return "^T";}
};

template<char var = 'x'>
struct linearSystemStyle: public Style
{
  struct subStyle: public equationStyle<var> {};
  typedef arrayPrinter subPrinter;
  inline static std::string begin() {return ("");}
  inline static std::string end() {return ("");}
  inline static std::string afterNum() {return ("");} //TODO: add equality indicator
  inline static std::string sepr() {return ("\n");}
  inline static std::string transpose_indicator() {return "";}
};

struct matrixPrinter
{
  
  template<class style_used = matrixStyle, class streamType, class inputType>
  inline static streamType& print_iter_to(streamType& printer, inputType X, dimi r, dimi c)
  {
    printer<<style_used::begin();
    style_used::subPrinter::print_iter_to(printer,X,c,style_used::subStyle::begin(),style_used::subStyle::sepr(),style_used::subStyle::end(),style_used::subStyle::afterNum(c));
    std::advance(X,c);
    for(dimi k = 1; k < r; k++)
      {
	printer<<style_used::sepr();
	style_used::subPrinter::print_iter_to(printer,X,c,style_used::subStyle::begin(),style_used::subStyle::sepr(),style_used::subStyle::end(),style_used::subStyle::afterNum(c));
	std::advance(X,c);
      }
    printer<<style_used::end();
    return printer;
  }
  template<class style_used = matrixStyle, class streamType, class inputType>
  inline static streamType& print_to(streamType& printer, const inputType& A, dimi r, dimi c)
  {
    auto X = container_beginning(A);
    return print_iter_to<style_used>(printer,X,r,c);
  }
  template<class style_used = matrixStyle, class subprinter = arrayPrinter, class streamType, class inputType>
  inline static streamType& print_to(streamType& printer, const inputType& A, dimi split)
  {
    return print_to<style_used,subprinter>(printer,A,(container_size(A)/split),split);
  }
  template<class style_used = matrixStyle, class streamType, class inputType>
  inline static streamType& print_to(streamType& printer, const inputType& A, dimi r, dimi c, bool transpose)
  {
    print_to<style_used>(printer,A,r,c);
    if(transpose)
      {
	printer<<style_used::transpose_indicator();
      }
    return printer;
  }
  template<class style_used = matrixStyle, class streamType, class inputType>
  inline static streamType& print_iter_to(streamType& printer, inputType& A, dimi r, dimi c, bool transpose)
  {
    print_iter_to<style_used>(printer,A,r,c);
    if(transpose)
      {
	printer<<style_used::transpose_indicator();
      }
    return printer;
  }
  template<class style_used = matrixStyle, class streamType, class inputType>
  inline static streamType& print_to(streamType& printer, const inputType& A, dimi split, bool transpose)
  {
    print_to<style_used>(printer,A,(container_size(A)/split),split);
    if(transpose)
      {
	printer<<style_used::transpose_indicator();
      }
    return printer;
  }
  template<class style_used = matrixStyle, class streamType = std::ostringstream, class inputType>
  inline static std::string print(const inputType& A, dimi split, bool transpose)
  {
    streamType printer;
    print_to(printer,A,split,transpose);
    return printer.str();
  }
};

#endif
