#ifndef _T_TEST_INCLUDED_
#define _T_TEST_INCLUDED_

#include "calculus.hpp"
#include "fourier.hpp"
#include <string>
#include <chrono>

struct stepper
{
  template<class IT>
  stepper(IT proj) {}
  stepper() {}
  inline static void step() {}
  inline static void outer() {}
};

template<class ST>
struct simple_percent: public stepper
{
  double iterations;
  double proj_max;
  ST& stream;
  inline void step() {iterations++;}
  inline void outer() {stream<<std::max((iterations/proj_max * 100), 0.0)<<"% complete\n";}
  simple_percent(ST& S, double proj): proj_max(proj), stream(S) {}
};

struct status_array_interface
{
  template<class IT>
  status_array_interface& operator[](const IT& index) {return *this;}
  template<class IT>
  status_array_interface& get_object(const IT& index) {return *this;}
  template<class IT>
  status_array_interface& set_object(const IT& index) {return *this;}
  void reserve(dimi N) {}
  void assign(dimi N) {}
};

struct snum: public status_array_interface
{
  status_enum status;
  snum(dimi N) {}
  snum() {}
}; // Status number

template<class T>
struct statwrapper: public status_array_interface
{
  T data;
};

template<class T, class ST>
struct print_push
{
  T* member;
  ST& stream;
  template<class IT>
  print_push& operator=(const IT& IN) {member->push_back(IN); stream<<IN; return *this;}
  print_push& operator=(print_push<T,ST> IN) {member = IN.member; return *this;}
  print_push& operator*() {return *this;}
  void operator++() {/*No-op*/}
  void operator++(int x) {/*No-op*/}

  print_push(T& mem, ST& str): stream (str) {member = &mem;}
};

template<class functor, class dataType>
struct unit_test
{
  std::string test_name;
  functor test;
  dataType statistics;
  status_enum status;
  template<class ST = snum, class RT = snum, class LT = snum>
  status_enum run_test(ST stat = 1, RT result = 1, LT log = 1, bool PS = false, bool PR = false)
  {
    try
      {
	return test(stat,result,log,PS,PR);
      }
    catch(...)
      {
	return status_enum::general_exception;
      }
  }
  template<class ST = snum, class RT = snum, class LT = snum>
  void update_status(ST stat = 0, RT result = 0, LT log = 0, bool PS = false, bool PR = false)
  {
    try
      {
	status = run_test(stat,result,log,PS,PR);
      }
    catch(...)
      {
	status = status_enum::general_exception;
      }
  }
  unit_test(std::string testname): test_name(testname), test() {}
  unit_test(std::string testname, functor to_test): test_name(testname), test(to_test) {}
};

template<class ST>
void status_begin(ST& status)
{
  // Do nothing by default
}

template<class ST>
void status_end(ST& status)
{
  // Do nothing by default
}


//TODO: allow for other time units
template<class TT, class DCT = std::chrono::high_resolution_clock>
struct timestatus
{
  TT begin;
  TT end;
  template<class IT>
  inline void set_begin(IT& input) {begin = input;}
  template<class IT>
  inline void set_end(IT& input) {begin = end;}
  inline void begin_now() {begin = DCT::now();}
  inline void end_now() {end = DCT::now();}
  std::string time_taken() {return (numtostring(std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count()) + "ns\n");}
  timestatus(TT begin_in, TT end_in): begin(begin_in), end(end_in) {}
  timestatus() {}
};


template<class TT, class ET>
inline void status_begin(timestatus<TT,ET>& status)
{
  status.begin_now();
}

template<class TT, class ET>
inline void status_end(timestatus<TT,ET>& status)
{
  status.end_now();
}

template<class CT>
struct status_array: public status_array_interface
{
  CT data;
  void reserve(dimi N) {reserve_container(data,N);}
  void set_size(dimi N) {std::cout<<"Assigning "<<N<<"\n";data.assign(N,0);}
  auto get_beginning() -> decltype(data.begin()) {return (data.begin());}
  auto get_end() -> decltype(data.end()) {return (data.end());}
  auto get_object(dimi N) -> decltype(data[N]) {return data[N];}
  template<class IT>
  void set_object(const IT& obj) {}
  auto operator[](dimi N) -> decltype(get_object(N)) {return get_object(N);}
};

template<class TT>
struct time_result_series: public status_array<std::vector<TT>>
{
  void reserve(dimi N) {reserve_container(this->data,2*N);}
  void set_size(dimi N) {this->data.assign(2*N,0);}
  timestatus<TT> get_object(dimi N) {timestatus<TT> T; return T;}
  void set_object(const timestatus<TT>& T) {this->data.push_back(T.begin), this->data.push_back(T.end);}
};


template<class LT>
void writeLog(LT& log, std::string line)
{
  log<<line;
}

template<class LT>
void writeLog(LT& log, snum line)
{
  // Do nothing by default
}

template<class LT, class TT, class DCT>
void writeLog(LT& log, timestatus<TT,DCT> line)
{
  log<<("Time: " + line.time_taken());
}

void writeLog(snum log, std::string line)
{
  // Do  nothing by default
}

template<class DT>
void writeLog(snum log, DT line)
{
  // Do nothing by default
}

template<class resultType, class inputType, class functor, class binaryOp = std::equal_to<void>, class RVT = std::vector<resultType>, class IVT = std::vector<inputType>>
struct auto_equal_check
{
  RVT goal_results;
  IVT auto_inputs;
  functor tester;
  std::string log_testid;
  binaryOp check;
  auto_equal_check(std::initializer_list<inputType> inputs, std::initializer_list<resultType> goals): goal_results(goals), auto_inputs(inputs), tester() , log_testid("AET"), check(){};
  auto_equal_check(std::initializer_list<inputType> inputs, std::initializer_list<resultType> goals, functor test): goal_results(goals), auto_inputs(inputs), tester(test) , log_testid("AET"), check(){};
  auto_equal_check(std::initializer_list<inputType> inputs, std::initializer_list<resultType> goals, std::string testid): goal_results(goals), auto_inputs(inputs), tester() , log_testid(testid), check(){};
  auto_equal_check(std::initializer_list<inputType> inputs, std::initializer_list<resultType> goals, functor test, std::string testid): goal_results(goals), auto_inputs(inputs), tester(test) , log_testid(testid), check(){};
  auto_equal_check(std::initializer_list<inputType> inputs, std::initializer_list<resultType> goals, binaryOp checker): goal_results(goals), auto_inputs(inputs), tester() , log_testid("AET"), check(checker){};
  auto_equal_check(std::initializer_list<inputType> inputs, std::initializer_list<resultType> goals, functor test, binaryOp checker):
    goal_results(goals), auto_inputs(inputs), tester(test) , log_testid("AET"), check(checker){};
  auto_equal_check(std::initializer_list<inputType> inputs, std::initializer_list<resultType> goals, std::string testid, binaryOp checker):
    goal_results(goals), auto_inputs(inputs), tester() , log_testid(testid), check(checker){};
  auto_equal_check(std::initializer_list<inputType> inputs, std::initializer_list<resultType> goals, functor test, std::string testid, binaryOp checker):
    goal_results(goals), auto_inputs(inputs), tester(test) , log_testid(testid), check(checker){};
  auto_equal_check(IVT inputs, RVT goals): goal_results(goals), auto_inputs(inputs), tester() , log_testid("AET"), check(){};
  auto_equal_check(IVT inputs, RVT goals, functor test): goal_results(goals), auto_inputs(inputs), tester(test) , log_testid("AET"), check(){};
  auto_equal_check(IVT inputs, RVT goals, std::string testid): goal_results(goals), auto_inputs(inputs), tester() , log_testid(testid), check(){};
  auto_equal_check(IVT inputs, RVT goals, functor test, std::string testid): goal_results(goals), auto_inputs(inputs), tester(test) , log_testid(testid), check(){};
  auto_equal_check(IVT inputs, RVT goals, binaryOp checker): goal_results(goals), auto_inputs(inputs), tester() , log_testid("AET"), check(checker){};
  auto_equal_check(IVT inputs, RVT goals, functor test, binaryOp checker):
    goal_results(goals), auto_inputs(inputs), tester(test) , log_testid("AET"), check(checker){};
  auto_equal_check(IVT inputs, RVT goals, std::string testid, binaryOp checker):
    goal_results(goals), auto_inputs(inputs), tester() , log_testid(testid), check(checker){};
  auto_equal_check(IVT inputs, RVT goals, functor test, std::string testid, binaryOp checker):
    goal_results(goals), auto_inputs(inputs), tester(test) , log_testid(testid), check(checker){};
  auto_equal_check(): tester(), check() {};
  void set_testid(std::string testid) {log_testid = testid;}
  template<class ST, class RT, class LT>
  status_enum operator() (ST& status, RT& result, LT& log, bool printstatus = false, bool printresult = false)
  {
    dimi size = std::min(goal_results.size(),auto_inputs.size());
    auto IB = container_beginning(auto_inputs);
    auto GB = container_beginning(goal_results);
    try
      {
	status.reserve(size);
	for(dimi iter = 0; iter < size; iter++)
	  {
	    auto temp = *IB; // Moving dereferencing costs out of (potential) timer zone
	    auto stat = status.get_object(iter);
	    status_begin(stat);
	    auto T = tester(temp);
	    status_end(stat);
	    status.set_object(stat);
	    //TODO: add result log writer
	    writeLog(log,(log_testid + ": #" + numtostring(iter) + "\n")); //Temporary
	    if(printresult)
	      {
		//TODO: print result
	      }
	    if(printstatus)
	      {
		//std::cout<<stat.time_taken();
		writeLog(log,stat);
	      }
	    //std::cout<<check(T,*GB)<<" , "<<(T==*GB)<<"\n";
	    if(!(check(T,(*GB))))
	      {
		writeLog(log,"False!\n"); //Temporary
		return status_enum::invalid_false;
	      }
	    IB++;
	    GB++;
	  }
      }
    catch(...)
      {
	return status_enum::general_exception;
      }
    return status_enum::valid_true;
  }
  template<class result_functor>
  void fillResults(result_functor& F)
  {
    dimi size = auto_inputs.size();
    reserve_container(goal_results,size);
    auto IB = container_beginning(auto_inputs);
    for(dimi k = 0; k < size; k++)
      {
	goal_results.push_back(F(*IB));
	IB++;
      }
  }
};

struct quadratic_functor
{
  double operator() (double x)
  {
    return x*x;
  }
};

struct fast_fourier_functor
{
  template<class IT>
  IT operator()(const IT& x)
  {
    dimi size = x.size();
    return radix2FFT::simpleFFT<IT>(x.begin(),size,1);
  }
};

struct slow_fourier_functor
{
  template<class IT>
  IT operator()(const IT& x)
  {
    IT X;
    dimi size = x.size();
    X.assign(size,0);
    simpleDFT::DFT(x.begin(),X.begin(),size);
    return X;
  }
};

#endif
