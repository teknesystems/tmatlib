// Generic, template-based numerical algorithms based off arbitrary functors, especially convergence and divergence

// Header guard. Skips over entire header if already included
#ifndef _T_NUM_INCLUDED_
#define _T_NUM_INCLUDED_

#include <iostream>

struct nullStep
{
  template<class IT, class IT2, class ST>
  inline static void process(const IT& A1, const IT& A2, const ST& N) {/*Do nothing (null step)*/}
};

struct iterativeConvergence
{
  template<class processingStep = nullStep, dimi error_factor = 10, class convergenceTest = elementwiseEquality, std::intmax_t M = 10000, class dataPointerType, class sizeType, class functorType>
  inline static dataPointerType converge_data(dataPointerType P, sizeType N, dataPointerType A, functorType f)
  {
    std::intmax_t iter = 0;
    while((iter<M)&&!(convergenceTest::converged(P,A,N)))
      {
	f(P,A,N); // Performing one cycle, writing result to the auxiliary pointer
	//std::cout<<"\nPRE: @"<<iter<<" : ";
	//arrayPrinter::print_iter_to(std::cout,A,N);
	//std::cout<<"\nPOST: ";
	processingStep::process(A,P,N); // Process results (A), potentially using input (P) as well
	//std::cout<<"@"<<iter<<" : ";
	//arrayPrinter::print_iter_to(std::cout,A,N);
	//std::cout<<"\n";
	// Swapping data and aux pointers
	auto T = P;
	P = A;
	A = T;
	// Incrementing interation
	iter++;
      }
    if(iter%2)
      {
	std::copy(A,std::next(A,N),P);
      }
    return P;
  }
};

#endif
