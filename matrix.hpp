// Basic code for matrices and matrix objects

// Header guard. Skips over entire header if already included.
#ifndef _T_MATRIX_INCLUDED_
#define _T_MATRIX_INCLUDED_

// In-library dependencies
#include "vectors.hpp"
#include "data.hpp"
#include "utility.hpp"

// Modules
#include "matrix/elementary_ops.hpp"
#include "matrix/rotation.hpp"
#include "matrix/QR_decomposition.hpp"
#include "matrix/QR_algorithm.hpp"
#include "matrix/hessenberg_form.hpp"
#include "matrix/row_reduction.hpp"
#include "matrix/determinant.hpp"
#include "matrix/multiplication/contiguous_simple.hpp"
#include "matrix/transformation/matrix_vector.hpp"


struct simplematrix: public elementOf<&nxnMatrices> //NOT A VECTOR (trying to avoid the dreaded diamond)!!! Provides simple matrix functions for non-tensor simple matrix implementations.
{
  bool direction; // If direction = true, split = columns
  dimi split;
};

// Polymorphic, abstract matrix
struct abstractarbMatrix
{
  virtual dimi rows()=0;
  virtual dimi cols()=0;
  virtual bool is_transpose()=0;
 };

template<class elementType, class operationalType = elementType>
struct abstractMatrix: public abstractarbMatrix
{
  virtual elementType& getElement(dimi row, dimi col)=0;
  virtual const elementType& getConstElement(dimi row, dimi col)=0;
  virtual elementType det()=0;
  virtual dimi size() {return rows()*cols();}
};

template<class matrixType, class elementType, class operationalType = elementType, class... C>
struct polymorphicMatrixInterface: public abstractMatrix<elementType,operationalType>, public matrixType, public C...
{
  polymorphicMatrixInterface(matrixType input): matrixType (input) {}
};

// Some generic matrices:
struct arbitrarymatrix: public vector, public simplematrix, public Symbolic
{
  dimi beams;
  dimi rows();
  dimi cols();
  arbitrarymatrix() {}
  arbitrarymatrix(dimi rowsin, dimi colsin);
};

struct identitymatrix: public arbitrarymatrix
{
  identitymatrix();
  identitymatrix(dimi rowsin, dimi colsin)
  {
    (this->beams) = rowsin;
    (this->split) = colsin;
    (this->direction) = true;
  }
  identitymatrix(dimi size);
};

struct zeromatrix: public arbitrarymatrix
{
  
};

template <class multiplierType>
struct elementarymatrix: public arbitrarymatrix
{
  elementaryBeamOperation<multiplierType> operation;
  dimi beams;
  // Constructors
  /*
  elementarymatrix();
  elementarymatrix(dimi rows, dimi cols);
  elementarymatrix(elementaryBeamOperation operation, dimi rows, dimi cols);
  */
  // Operations
  dimi rows()
  const {
    if(this->direction)
      {
	return split;
      }
    else
      {
	return beams;
      }
  }
  dimi cols()
  const {
    if(this->direction)
      {
	return beams;
      }
    else
      {
	return split;
      }
  }
  multiplierType det()
  const {
    return operation.det(beams);
  }
  void invert();
  void flip()
  {
    (this->direction) = !(this->direction);
  }
  void hard_flip()
  {
    // Kept only for compatibility with other matrix classes
    (this->direction) = !(this->direction);
  }
  elementarymatrix inverse();
  elementarymatrix transpose()
  const {
    elementarymatrix<multiplierType> result (*this);
    result.flip();
    return result;
  }
  elementarymatrix soft_transpose()
  const {
    elementarymatrix<multiplierType> result (*this);
    result.flip();
    return result;
  }
  dimi getSMatrix();
};


// Semi-symbolic "operation_storage" type.
template <class matrixType, class operationType>
struct sElementaryOpMatrix
{
  dimi index_used; // Index number to operate on
  matrixType starting_matrix;
  elementaryArray<operationType> operations;
  bool isInvertible()
  {
    //return det(starting_matrix) == 0;
    return true; //TEMPORARY!
  }
  operationType det()
  {
    /*
      return det(starting_matrix)*(operations.det(starting_matrix.rows()))
    */    
    return operations.det(starting_matrix.rows()); //TEMPORARY
  }
  sElementaryOpMatrix() {}
  sElementaryOpMatrix(const matrixType &sminput, const elementaryArray<operationType> &opinput)
  {
    starting_matrix = sminput;
    operations = opinput;
  }
};


// Semi-symbolic static "operation_storage" type (potentially optimized out by the compiler).
template <class matrixType, class operationType>
struct stElementaryOpMatrix
{
  const matrixType& starting_matrix;
  const elementaryArray<operationType>& operations;
  bool isInvertible()
  {
    //return det(starting_matrix) == 0;
    return true; //TEMPORARY!
  }
  operationType det()
  {
    /*
      return det(starting_matrix)*(operations.det(starting_matrix.rows()))
    */    
    return operations.det(starting_matrix.rows()); //TEMPORARY
  }
  stElementaryOpMatrix() {}
  stElementaryOpMatrix(const matrixType &sminput, const elementaryArray<operationType> &opinput)
  {
    starting_matrix = sminput;
    operations = opinput;
  }
};


template <class matrixType, class operationType>
struct sElementaryOpMatrix<matrixType*, operationType>
{
  matrixType *starting_matrix;
  elementaryArray<operationType> operations;
  bool isInvertible()
  {
    if(starting_matrix == NULL)
      {
	return true;
      }
    else
      {
	if(/*det(starting_matrix) == 0*/false/*TEMPORARY*/)
	  {
	    return true;
	  }
	else
	  {
	    return false;
	  }
      }
  }
  operationType det()
  {
    if (starting_matrix == NULL)
      {
	return operations.det(starting_matrix->rows());
      }
    else
      {
	return 0; //TEMPORARY!
      }
  }
  void operator*= (sElementaryOpMatrix<matrixType, operationType>  &input)
  {
    if((this->cols()) == input.rows())
      {
	if(input.starting_matrix == NULL)
	  {
	    //TODO: Implement
	  }
	else
	  {
	    //TODO: Input multiplication handling here
	  }
      }
    else
      {
	//TODO: Input error handling here
      }
  }
};

// Basic simple matrix implementation
template <class elementType, class containerType = std::vector<elementType>, class handler = contiguous<true,false>>
struct smatrix: public arrayvector<elementType,containerType>, public simplematrix, public symbolicObject<smatrix<elementType,containerType,handler>>
{
  smatrix() {}
  smatrix(elementType *input, dimi rows, dimi cols)
  {
    try
      {
	reserve_container((this->elements),rows*cols);
	auto IT = rewrite_front_iterator(this->elements);
	for(dimi iter = 0; iter < (rows*cols); iter++)
	  {
	    *IT = (*input);
	    IT++;
	    input++;
	  }
      }
    catch (const std::exception& e)
      {
	(this->elements).clear();
	(this->elements).assign(((this->elements).size()), 0);
	(this->basis_used) = &VECTOR_ERROR;
      }
    (this->basis_used) = NULL;
    (this->split) = cols;
    (this->direction) = true;
  }
  smatrix(containerType& input, dimi split, bool dir = true)
  {
    using std::swap;
    swap(input,this->elements);
    this->split = split;
    this->direction = dir;
  }
  smatrix(dimi length, bool dir)
  {
    (this->basis_used) = NULL;
    (this->split) = length;
    (this->direction) = dir;
  }
  smatrix(dimi rows, dimi cols)
  {
    (this->basis_used) = NULL;
    (this->split) = cols;
    (this->direction) = true;
    (this->elements).reserve(rows*cols);
  }
  smatrix(dimi rows, dimi cols, elementType setValue)
  {
    (this->basis_used) = NULL;
    (this->split) = cols;
    (this->direction) = true;
    assign_to_container((this->elements),(rows*cols), setValue);
  }
  smatrix(const smatrix& input)
  {
    (this->basis_used) = input.basis_used;
    (this->split) = input.split;
    (this->direction) = input.direction;
    (this->elements) = input.elements;
  }
  template<class inputType>
  smatrix(const smatrix<inputType>& input)
  {
    (this->basis_used) = input.basis_used;
    (this->split) = input.split;
    (this->direction) = input.direction;
    reserve_container((this->elements),input.elements.size());
    auto IT = rewrite_front_iterator((this->elements));
    auto IT_IN = container_beginning(input.elements);
    auto END = std::next(IT_IN,input.elements.size());
    //dimi N = input.elements.size();
    while(IT_IN < END)
      {
	//std::cout<<"LOOP\n";
	*IT = *IT_IN;
	IT_IN++;
	IT++;
	//std::cout<<"LEND\n";
	}
  }
  template<class inputType>
  smatrix(const std::initializer_list<inputType>& input, dimi split, bool inputdir = true)
  {
    (this->direction) = inputdir;
    (this->split) = split;
    reserve_container((this->elements),input.size());
    auto IT = rewrite_front_iterator((this->elements));
    if(input.size()%split == 0)
      {
	auto iterator = input.begin();
	for(dimi iter = 0; iter < input.size(); iter++)
	  {
	    *IT = (iterator[iter]);
	    IT++;
	  }
      }
    else
      {
	(this->elements).clear();
	(this->basis_used) = &VECTOR_ERROR;
	return;
      } 
    (this->basis_used) = NULL;
  }
  
  auto begin() -> decltype(container_beginning(this->elements))
  {
    return container_beginning(this->elements);
  }
  auto end() -> decltype(container_beginning(this->elements))
  {
    return container_end(this->elements);
  }
  auto operator[] (dimi index)-> jump_pointer<decltype(container_beginning(this->elements))>
  {
    auto result = container_beginning(this->elements);
    std::advance(result,((this->direction)?(index*(this->split)):index));
    return jump_pointer<decltype(container_beginning(this->elements))>(result,(this->direction)?1:split);
  }
  dimi rows()
  const { 
    if((this->direction))
      {
	return (this->elements).size()/(this->split);
      }
    else
      {
	return (this->split);
      }
  }
  dimi cols()
  const {
    if((this->direction))
      {
	return (this->split);
      }
    else
      {
	return (this->elements).size()/(this->split);
      }
  }
  // Container compatibility functions:
  elementType& at(dimi index)
  {
    return (this->elements).at(index);
  }
  const elementType& const_at(dimi index)
  const {
    return (this->elements).const_at(index);
  }
  bool reserve(dimi index)
  {
    reserve_container((this->elements),index);
    return true;
  }
  bool resize(dimi index)
  {
    reserve_container((this->elements),index);
    return true;
  }
  bool clear(dimi index)
  {
    clear_container((this->elements),index);
    return true;
  }
  bool empty()
  {
    return false; //TEMPORARY!
  }
  elementType& getElement(dimi row, dimi col)
  {
    if(direction)
      {
	return (this->elements)[row*(this->split) + col];
      }
    else
      {
	return (this->elements)[col*(this->split) + row];
      }
  }
  const elementType& getConstElement(dimi row, dimi col)
  const {
    if(direction)
      {
	return (this->elements)[row*(this->split) + col];
      }
    else
      {
	return (this->elements)[col*(this->split) + row];
      }
  }
  elementType& getElement(dimi index)
  {
    return (this->elements)[index];
  }
  const elementType& getConstElement(dimi index)
  const{
    return (this->elements)[index];
  }
  void setDiagonal (elementType setValue)
  {
    for(int iter = 0; iter < rows(); iter++)
      {
	getElement(iter, iter) = setValue;
      }
  }
  /*
  smatrix(elementType setValue, dimi rows, dimi cols)
  {
    (this->basis_used) = NULL;
    (this->split) = cols;
    (this->direction) = true;
    (this->elements).assign((rows*cols), 0);
    (this->elements).setDiagonal(setValue);
    }*/
  template<class MT>
  void setBlock(const MT& A, dimi row, dimi col)
  {
    for(dimi iter = 0; iter < A.rows(); iter++)
      {
	for(dimi miniiter = 0; miniiter < A.cols(); miniiter++)
	  {
	    getElement((iter + row), (miniiter + col)) = A.getConstElement(iter, miniiter);
	  }
      }
  }
  template<class MT>
  void setBlock(elementType multiplier, const MT& A, dimi row, dimi col)
  {
    //std::cout<<"setBlock called @("<<row<<","<<col<<") with multiplier "<<multiplier<<"\n"; // Scaffolding
    for(dimi iter = 0; iter < A.rows(); iter++)
      {
	for(dimi miniiter = 0; miniiter < A.cols(); miniiter++)
	  {
	    getElement((iter + row), (miniiter + col)) = multiplier * A.getConstElement(iter, miniiter);
	  }
      }
  }
  void invert()
  {
    //TODO: Implement
  }
  void flip()
  {
    (this->direction) = !(this->direction);
  }
  void forward_pivot_swap(dimi diag)
  {
    for(dimi iter = 0; iter < (std::min(rows(), cols()) - diag); iter++)
      {
	std::swap(getElement(diag, (diag + iter)), getElement((diag+iter),diag));
      }
  }
  void hard_flip()
  {
    if(rows() == cols())
      {
	for(dimi iter = 0; iter < std::min(rows(), cols()); iter++)
	  {
	    forward_pivot_swap(iter);
	  }
      }
    else
      {
	std::vector<elementType> newvalues;
	newvalues.reserve(rows()*cols());
	dimi newsplit = (this->elements).size()/(this->split);
	if(this->direction)
	  {
	    for(dimi oldrowiter = 0; oldrowiter < rows(); oldrowiter++)
	      {
		for(dimi oldcoliter = 0; oldcoliter < cols(); oldcoliter++)
		  {
		    newvalues[(oldrowiter * newsplit) + oldcoliter] = getConstElement(oldrowiter, oldcoliter);
		  }
	      } 
	  }
	else
	  {
	    for(dimi oldrowiter = 0; oldrowiter < rows(); oldrowiter++)
	      {
		for(dimi oldcoliter = 0; oldcoliter < cols(); oldcoliter++)
		  {
		    newvalues[(oldcoliter * newsplit) + oldrowiter] = getConstElement(oldrowiter, oldcoliter);
		  }
	      } 
	  }
	(this->split) = newsplit;
	std::swap((this->elements),newvalues);
      }
  }
  smatrix<elementType> transpose()
  {
    smatrix<elementType> A (*this);
    A.hard_flip();
    return A;
  }
  smatrix<elementType> soft_transpose()
  {
    smatrix<elementType> A (*this);
    A.flip();
    return A;
  }
  elementType sumDiagonal()
  const {
    elementType result = 0; //TODO: find a way to specifically get the additive identity of elementType, including the additive identity element of the vectorspace of the first element of a matrix of vectors
    for(dimi iter = 0, i = 0; iter < (this->elements).size(); iter+=split)
      {
	result+=(this->elements)[iter + i];
	i++;
      }
    return result;
  }
  elementType multiplyDiagonal()
  const {
    elementType result = 1; //TODO: find a way to get the multiplicative identity of elementType, including the multiplicative identity element of the vectorspace of the first element of a matrix of vectors
    for(dimi iter = 0, i = 0; iter < (this->elements).size(); iter+=split)
      {
	result*=(this->elements)[iter + i];
	i++;
      }
    return result;
  }
  elementType tr()
  const {
    if(split == ((this->elements).size())/split)
      {
	return sumDiagonal();
      }
    else
      {
	return 0; //TODO: throw error!
      }
  }
  // Scalar multiplication and vector addition
  template<class scalarType>
  void operator*=(scalarType scalar_multiplier)
  {
    // try
    //{
    elementwiseScalarMultiplication::op((this->elements),scalar_multiplier);
    /* }
    catch (const std::exception& e)
      {
	//TODO: implement exception handling
	}*/
  }
  template<class CT>
  void operator+=(const smatrix<elementType,CT>& input_matrix)
  {
    if(((this->elements).size() == input_matrix.elements.size())&&((this->split)==input_matrix.split))
      {
	elementwiseAddition::op_to((this->elements),input_matrix.elements);
      }
    else
      {
	(this->basis_used) = &VECTOR_ERROR;
      }
  }
  template<class CT>
  void operator-=(const smatrix<elementType,CT>& input_matrix)
  {
    if(((this->elements).size() == input_matrix.elements.size())&&((this->split)==input_matrix.split))
      {
	for(dimi iter = 0; iter < input_matrix.elements.size(); iter++)
	  {
	    (this->elements)[iter]-=input_matrix.elements[iter];
	  }
      }
    else
      {
        (this->basis_used) = &VECTOR_ERROR;
      }
  }
  // Elementary row operation functions
  void elbop(dimi beam, dimi swap, elementType weight)
  {
    elops_on_container<_STANDARD_ELOP_FUNCTIONS_>::elbop((this->elements),beam,swap,weight,rows(),cols());
  }
  void elsbop(dimi beam, dimi swap, elementType weight)
  {
    elops_on_container<_STANDARD_ELOP_FUNCTIONS_>::elsbop((this->elements),beam,swap,weight,rows(),cols());
  }
  void elbop(const elementaryBeamOperation<elementType> &input)
  {
    elbop(input.beam,input.swap,input.weight);
  }
  void elsbop(const elementaryBeamOperation<elementType> &input)
  {
    elsbop(input.beam,input.swap,input.weight);
  }
  void elrop(const dimi row1, const dimi row2, const elementType weight)
  {
    if(this->direction)
      {
	elbop(row1, row2, weight);
      }
    else
      {
	elsbop(row1, row2, weight);
      }
  }
  void elrop(const elementaryBeamOperation<elementType> &input)
  {
    elrop(input.beam,input.swap,input.weight);
  }
  void elcop(const dimi col1, const dimi col2, const elementType weight)
  {
    if(this->direction)
      {
	elsbop(col1, col2, weight);
      }
    else
      {
	elbop(col1, col2, weight);
      }
  }
  void elcop(const elementaryBeamOperation<elementType> &input)
  {
    elcop(input.beam,input.swap,input.weight);
  }
  void elbop(const elementaryBeamOperation<elementType> &input, const bool rowcol)
  {
    if(rowcol)
      {
	elrop(input);
      }
    else
      {
	elcop(input);
      }
  }
  // Determinant, rrf, rref, augmentation and rank functions
  void row_reduce(dimi startrow, dimi startcol, dimi endrow, dimi endcol, elementaryArray<elementType>& operations)
  {
    ELOP_tagalong<elementType,decltype(operations)> ops (operations);
    matrixRRF::elimination_reduce((*this).elements,rows(),cols(),ops,startrow,startcol,endrow,endcol);
  }
    // Determinant, rrf, rref, augmentation and rank functions
  void row_reduce(dimi startrow, dimi startcol, dimi endrow, dimi endcol, elementType& operations)
  {
    det_tagalong<double> x;
    //std::cout<<"ORR CALLED\n";
    matrixRRF::elimination_reduce((*this).elements,rows(),cols(),x,startrow,startcol,endrow,endcol);
    //std::cout<<"ORR FIN\n";
    operations*=x.det_value;
  }
  void row_reduce(dimi startrow, dimi startcol, dimi endrow, dimi endcol)
  {
    dimi state;
    //std::cout<<"RR CALLED\n";
    matrixRRF::elimination_reduce((this->elements),rows(),cols(),state,startrow,startcol,endrow,endcol);
    //std::cout<<"RR FIN\n";
  }
  void row_reduce(dimi maxrow, dimi maxcol)
  {
    row_reduce(0,0,maxrow,maxcol);
  }
  void row_reduce()
  {
    //elementaryArray<elementType> temporary;
    row_reduce(rows(),cols());
  }
  void to_rrf(dimi startcol, dimi startrow, dimi endrow, dimi endcol)
  {
    //TODO: Implement changed algorithm, or remove redundant feature
    dimi state;
    //std::cout<<"RR CALLED\n";
    matrixRRF::elimination_reduce((this->elements),rows(),cols(),state,startrow,startcol,endrow,endcol);
    //std::cout<<"RR FIN\n";
  }
  void echelonize(dimi startrow, dimi startcol, dimi endrow, dimi endcol)
  {
    matrixEchelonization::echelonize_reduced((this->elements),rows(),cols(),startrow,startcol,endrow,endcol);
  }
  void echelonize()
  {
    echelonize(0,0,rows(),cols());
  }
  smatrix<elementType> rref(elementaryArray<elementType>& operations)
  {
    smatrix<elementType> result (*this);
    result.row_reduce(0,0,rows(),cols(),operations);
    result.echelonize(0,0,rows(),cols(),operations);
    return result;
  }
  smatrix<elementType> rref()
  {
    smatrix<elementType> result (*this);
    result.row_reduce(0,0,rows(),cols());
#ifdef _T_VEC_RREF_SCAFFOLDING_
    std::cout<<"ECHELONIZING\n"; //Scaffolding
#endif
    result.echelonize(0,0,rows(),cols());
    return result;
  }
  sElementaryOpMatrix<smatrix<elementType>, elementType> append_operations(elementaryArray<elementType> operations)
  {
    sElementaryOpMatrix<smatrix<elementType>, elementType> result (*this, operations);
    return result;
  }
  bool is_linearly_independent()
  {
    //TODO: Make more efficient
    smatrix<elementType> temporary = *this;
    temporary.row_reduce(0,0,rows(),cols());
    return !(temporary.multiplyDiagonal()==0);
  }
  elementType det()
  const {
    auto E = (*this).elements;
    det_tagalong<elementType> x;
    dimi r = rows(), c = cols();
    matrixRRF::elimination_reduce(E,r,c,x,0,0,r,c);
    return (matrixDeterminant::multiplyDiagonal<elementType>(E,r)/(x.det_value));
  }
  smatrix inverse()
    const {
    auto C = std::min(rows(),cols());
    auto E = this->elements;
    smatrix R = identitymatrix(C,C);
    inversion_tagalong<decltype(R.elements.data())> inversiontag (R.elements.data(),R.split);
    matrixRRF::elimination_reduce(E,C,C,inversiontag,0,0,C,C);
    matrixEchelonization::echelonize_reduced(E,C,C,inversiontag,0,0,C,C);
    return R;
  }
  template<class contType>
  contType evaluate(const contType& input)
  const {
    contType result;
    if(input.size()==(this->cols()))
      {
	reserve_container(result,(this->rows()));
	for(dimi iter = 0; iter++; iter < rows())
	  {
	    push_to_container(result,(input[0]*(getConstElement(iter,0))),iter);
	    for(dimi miniiter = 1; miniiter < cols(); miniiter++)
	      {
		result[iter]+=(input[miniiter]*getConstElement(iter,miniiter));
	      }
	  }
      }
    else
      {
	throw 1; //TODO: Add better exception handling
      }
      return result;
  }
  template<class contType>
  contType evaluate_lenient(const contType& input)
  const {
    contType result;
    reserve_container(result,(this->rows()));
    for(dimi iter = 0; iter++; iter < rows())
      {
	push_to_container(result,(input[0]*(getConstElement(iter,0))),iter);
	for(dimi miniiter = 1; miniiter < std::min(input.size(),cols()); miniiter++)
	  {
	    result[iter]+=(input[miniiter]*getConstElement(iter,miniiter));
	  }
      }
    return result;
  }
  template<class contType>
  contType eval(const contType& input)
  const {
    evaluate_lenient(input);
  }
  template<class contType>
  contType evaluate_transpose(const contType& input)
  const {
    contType result;
    if(input.size()==(this->rows()))
      {
	reserve_container(result,(this->cols()));
	for(dimi iter = 0; iter++; iter < cols())
	  {
	    push_to_container(result,(input[0]*(getConstElement(0,iter))),iter);
	    for(dimi miniiter = 1; miniiter < rows(); miniiter++)
	      {
		result[iter]+=(input[miniiter]*getConstElement(miniiter,iter));
	      }
	  }
      }
    else
      {
	throw 1; //TODO: Add better exception handling
      }
      return result;
  }
  template<class contType>
  contType evaluate_lenient_transpose(const contType& input)
  const {
    contType result;
    reserve_container(result,std::min(input.size(),cols()));
    for(dimi iter = 0; iter++; iter < cols())
      {
	push_to_container(result,(input[0]*(getConstElement(0,iter))),iter);
	for(dimi miniiter = 1; miniiter < std::min(input.size(),rows()); miniiter++)
	  {
	    result[iter]+=(input[miniiter]*getConstElement(miniiter,iter));
	  }
      }
    return result;
  }
  template<class contType>
  contType eval_t(const contType& input)
  const {
    evaluate_lenient_transpose(input);
  }

  // Matrices as functors
  template<class IT1, class IT2, class ST>
  void operator() (IT1 IA, IT2 OA, ST N)
  const {
    //TODO: optionally put array access safety measure here.
    auto ELEM = container_beginning(this->elements);
    auto r = rows();
    contiguous_matrix_vector_multiplication::transform(IA,std::min(N,this->split),OA,r,ELEM); //TODO: add a "strict mode" where N == (this->split)
    // Note result may be erroneous for N<(this->split), and leniency is mainly intended for N > split
    //TODO: perhaps add case where N < (this->split) triggers submatrix evaluation...
  }

  template<class VT>
  VT operator() (VT V)
  {
    VT O;
    reserve_container(O,rows());
    return operator() (container_beginning(V),rewrite_front_iterator(O),container_size(V));
  }
  
  template<class streamType>
  streamType& print(streamType& printer)
  const {
    return matrixPrinter::print_to(printer,(this->elements),(this->split),!(this->direction));
  }
  std::string print() const
  {
    std::ostringstream printer;
    return (print(printer)).str();
  }
  std::ostream& printdetails(std::ostream& printer)
  {
    printer<<"Object Type: smatrix\n"
	   <<"Rows: "<<rows()<<" Columns: "<<cols()<<"\n"
	   <<"Elements:\n";
    print(printer)<<"\n"
	   <<"Is linearly independent? "<<(this->is_linearly_independent())<<"\n"
	   <<"Determinant: "<<(this->det())<<"\n";
    return printer;
  }
  std::string printdetails()
  {
    std::ostringstream printer;
    printdetails(printer);
    return printer.str();
  }

  // Implicit conversions from named matrices (e.g. the identity matrix)
  smatrix(const identitymatrix& I)
  {
    (this->split) = I.split;
    (this->elements).reserve(I.split*I.beams);
    (this->direction) = I.direction;
    for(dimi iter = 0; iter < split; iter++)
      {
	for(dimi firstiter = 0; firstiter < iter; firstiter++)
	  {
	    (this->elements).push_back(0);
	  }
	(this->elements).push_back(1);
	 for(dimi firstiter = (iter+1); firstiter < split; firstiter++)
	   {
	     (this->elements).push_back(0);
	   }
      }
  }
  smatrix(const zeromatrix& Z)
  {
    (this->split) = Z.split;
    (this->direction) = Z.direction;
    (this->elements).assign((Z.split * Z.beams),0);
  }
  
  // Operational smatrix constructors
  // These constructors construct smatrix objects from symbolic operations
  // E.g., for smatrix A, B, C, C = A + B, the expression A + B returns the symbolic operation object (A,B,+)
  // These constructors then take these objects and assign the value of A + B to C.

  // "+" operator (addition)
  // Copy operator (containers specified)
  template <class contType2>
  const smatrix<elementType, containerType>& operator=(const staticBinaryOperation<smatrix<elementType,containerType>,smatrix<elementType,contType2>,&add>& X)
  {
    const smatrix<elementType, containerType>& right = X.right;
    const smatrix<elementType, containerType>& left = X.left;
    //TODO: Implement basis pointer handling (e.g. for INVALID_MATRIX errors
    if((right.rows() == left.rows())&&(right.cols() == left.cols()))
      {
	// Inherited value initialization
	(this->basis_used) = right.basis_used;
	(this->direction) = right.direction;
	(this->split) = right.split;
	reserve_container((this->elements),(right.rows() * right.cols()));
	if(right.direction == left.direction)
	  {
	    for(dimi iter = 0; iter < (right.rows() * right.cols()); iter++)
	      {
		(this->elements).push_back(right.elements[iter]+left.elements[iter]);
	      }
	  }
	else
	  {
	    for(dimi iter = 0; iter < right.rows(); iter++)
	      {
		//TODO: Optimize!
		for(dimi miniiter = 0; miniiter < right.cols(); miniiter++)
		  {
		    (this->elements).push_back(right.getConstElement(iter,miniiter)+left.getConstElement(iter,miniiter));
		  }
	      }
	  }
      }
    return *this;
  }
  // Extensible-right copy operator WIP
  template <class contType2, class elementType2, class inputType1, class inputType2>
  const smatrix<elementType, containerType>& operator=(const staticBinaryOperation<staticBinaryOperation<inputType1,inputType2,&add>,smatrix<elementType2,contType2>,&add>& X)
  {
    #ifdef _T_VEC_SYMB_SCAFFOLDING_
    std::cout<<"Beginning conversion from symbolic to static form\n";
    #endif
    const dimi depth = op_rdepth<staticBinaryOperation<staticBinaryOperation<inputType1,inputType2,&add>,smatrix<elementType2,contType2>,&add>,&add>::num;
    //operator=(X.right);
    const smatrix<elementType2,contType2>* ptrs[depth];
    op_ptrs<staticBinaryOperation<inputType1,inputType2,&add>,smatrix<elementType2,contType2>,smatrix<elementType2,contType2>>::get_ptrs(ptrs,X);
    #ifdef _T_VEC_SYMB_SCAFFOLDING_
    std::cout<<"Depth calculated to be "<<depth<<", array initialized\n";
    #endif
    //TODO: Implement basis pointer handling (e.g. for INVALID_MATRIX errors)
    (this->basis_used) = X.right.basis_used;
    (this->direction) = X.right.direction;
    (this->split) = X.right.split;
    (this->elements).reserve(X.right.rows()*X.right.cols());
    #ifdef _T_VEC_SYMB_SCAFFOLDING_
    std::cout<<"Element vector initialized\n";
    #endif
    for(dimi siter = 0; siter < (X.right.rows()*X.right.cols()); siter+=(this->split))
      {
	for(dimi iter = 0; iter < (this->split); iter++)
	  {
	    (this->elements).push_back(X.right.elements[iter+siter]);
	    #ifdef _T_VEC_SYMB_SCAFFOLDING_
	    std::cout<<"Value "<<X.right.elements[iter+siter]<<" pushed back at position "<<(iter+siter)<<". Parameters ("<<iter<<","<<siter<<") of "<<split<<"w/ depth "<<depth<<".\n";
	    #endif
	    for(dimi miniiter = 0; miniiter < depth; miniiter++)
	      {
		#ifdef _T_VEC_SYMB_SCAFFOLDING_
		std::cout<<"Preparing to add matrix #"<<miniiter<<" of "<<depth<<"\n"
			 <<"Rows: "<<ptrs[miniiter]->rows()<<" (vs. "<<X.right.rows()<<"); Cols: "<<ptrs[miniiter]->cols()<<" (vs. "<<X.right.cols()<<")\n"
			 <<"Parameters:"<<((ptrs[miniiter]->rows())==(X.right.rows()))<<" AND "<<((ptrs[miniiter]->cols())==(X.right.cols()))
			 <<" = "<<(((ptrs[miniiter]->rows())==(X.right.rows()))&&((ptrs[miniiter]->cols())==(X.right.cols())))<<"\n";
		//std::cout<<"Added matrix:\n"<<ptrs[miniiter]->print()<<"\n";
		#endif
		if(((ptrs[miniiter]->rows())==(X.right.rows()))&&((ptrs[miniiter]->cols())==(X.right.cols())))
		  {
		    if((this->direction) == ptrs[miniiter]->direction)
		      {
			#ifdef _T_VEC_SYMB_SCAFFOLDING_
			std::cout<<"Non transpose... ";
			#endif
			(this->elements)[siter+iter] += ptrs[miniiter]->elements[siter+iter];
			#ifdef _T_VEC_SYMB_SCAFFOLDING_
			std::cout<<"Added "<<ptrs[miniiter]->elements[siter+iter]<<" @ position.\n";
			#endif
		      }
		    else
		      {
			#ifdef _T_VEC_SYMB_SCAFFOLDING_
			std::cout<<"Transpose... ";
			#endif
			(this->elements)[iter] += ptrs[miniiter]->getConstElement((siter/split),iter); //TODO: Optimize!
			#ifdef _T_VEC_SYMB_SCAFFOLDING_
			std::cout<<"Transpose Added "<<ptrs[miniiter]->getConstElement((siter/split),iter)<<" @ position.\n";
			#endif
		      }
		  }
		else
		  {
		    #ifdef _T_VEC_SYMB_SCAFFOLDING_
		    std::cout<<"Vector addition error! Rows and columns not equal (smatrix)!\n";
		    #endif
		    (this->basis_used) = &VECTOR_ERROR;
		    return *this;
		  }
	      }
	  }
      
	  }
    return *this;
  }
  template <class contType2, class elementType2, class inputType1, class inputType2>
  smatrix (const staticBinaryOperation<staticBinaryOperation<inputType1,inputType2,&add>,smatrix<elementType2,contType2>,&add>& X)
  {
    operator=(X);
  }
    
  // Copy constructor
 template <class contType2>
 smatrix (const staticBinaryOperation<smatrix<elementType,containerType>,smatrix<elementType,contType2>,&add>& X)
  {
    operator=(X);
  }

  // "*" operator (matrix multiplication)
  // Copy operator (containers specified)
  template <class contType2>
  const smatrix<elementType, containerType>& operator=(const staticBinaryOperation<smatrix<elementType,containerType>,smatrix<elementType,contType2>,&multiply>& X)
  {
    const smatrix<elementType, containerType>& A = X.left;
    const smatrix<elementType, containerType>& B = X.right;
    dimi m = A.rows();
    dimi n = A.cols();
    (this->split) = B.cols(); // = p, for an m*p matrix result
    (this->direction) = A.direction;
    if(n == B.rows())
    {
      zero_assign_container((this->elements),((this->split)*n));
      matrix::simple_iterator_multiplication::multiply(container_beginning((this->elements)),container_beginning(A.elements),container_beginning(B.elements),m,n,(this->split),A.direction,B.direction,A.direction);
    }
    else
      {
	//TODO: Potentially return error beyond "invalid arbitrary matrix"
	(this->basis_used) = &VECTOR_ERROR;
	return *this;
      }
    (this->basis_used) = A.basis_used;
    return *this;
  }
  // Copy constructor
  template <class contType2>
  smatrix (const staticBinaryOperation<smatrix<elementType,containerType>,smatrix<elementType,contType2>,&multiply>& X)
  {
    operator=(X);
  }


  // Equality checker:
  template<class elemType2, class contType2>
  bool operator==(const smatrix<elemType2,contType2>& X)
  {
    dimi r = X.rows();
    dimi c = X.cols();
    if((r==(this->rows())&&c==(this->cols())))
      {
	if(X.direction==(this->direction))
	  {
	    return matrixEquality::equal_to((this->elements),X.elements,(r*c),0);
	  }
	else
	  {
	    return matrixEquality::equal_to((this->elements),X.elements,(r*c),r);
	  }
      }
    else
      {
	return false;
      }
  }
};

template<class elementType, class CT, class CT2>
smatrix<elementType,CT> Outer(const smatrix<elementType,CT>& A, const smatrix<elementType,CT2>& B)
{
  std::cout<<"Outer product called!\n"; // Scaffolding
  dimi nrow = A.rows() * B.rows(), ncol = A.cols() * B.cols();
  smatrix<elementType,CT> C (nrow, ncol, 0);
  std::cout<<"Assigned...\n"; // Scaffolding
  for(dimi iter = 0; iter < B.rows(); iter++)
    {
      for(dimi miniiter = 0; miniiter < B.cols(); miniiter++)
	{
	  //std::cout<<"Calling blocksetter for b @("<<iter<<","<<miniiter<<") with multiplier "<<B.getElement(iter, miniiter)<<"\n";
	  C.setBlock(B.getConstElement(iter,miniiter), A, (A.rows() * iter), (A.cols() * miniiter));
	}
    }
  return C;
}


template<class elementType, class containerType>
struct smatrix<elementType,containerType,diagonal>: public arrayvector<elementType, containerType>
{
  dimi beams;
  smatrix() {}
  smatrix(elementType *input, dimi rows, dimi cols)
  {
    try
      {
	reserve_container((this->elements),rows);
	for(int iter = 0; iter < (rows); iter++)
	  {
	    (this->elements).push_back(input[iter]);
	  }
      }
    catch (const std::exception& e)
      {
	(this->elements).clear();
	(this->elements).assign(((this->elements).size()), 0);
      }
    (this->basis_used) = NULL;
    (this->split) = cols;
    beams = rows;
    (this->direction) = true;
  }
  smatrix(dimi length, bool dir)
  {
    (this->basis_used) = NULL;
    (this->split) = length;
    (this->direction) = dir;
  }
  smatrix(dimi rows, dimi cols, bool dir)
  {
    (this->direction) = dir;
    if(dir)
      {
	(this->split) = cols;
	beams = rows;
	(this->basis_used) = NULL;
      }
    else
      {
	(this->split) = rows;
	beams = cols;
	(this->basis_used) = NULL;
      }
  }
  smatrix(dimi rows, dimi cols, elementType setValue)
  {
    (this->basis_used) = NULL;
    (this->split) = cols;
    beams = rows;
    (this->direction) = true;
    (this->elements).assign(rows, setValue);
  }
  elementType multiplyDiagonal()
  {
    elementType result = 1;
    for(dimi iter = 0; iter < (this->elements).size(); iter++)
      {
	result*=(this->elements)[iter];
      }
    return result;
  }
  elementType sumDiagonal()
  {
    elementType result = 0;
    for(dimi iter = 0; iter < (this->elements).size(); iter++)
      {
	result+=(this->elements)[iter];
      }
    return result;
  }
  elementType det()
  {
    if((this->split) == beams)
      {
	return multiplyDiagonal();
      }
  }
  elementType tr()
  {
    if((this->split) == beams)
      {
	return sumDiagonal();
      }
  }
  elementType& getElement(dimi row, dimi col)
  {
    if(row == col)
      {
	return (this->elements)[row];
      }
    else
      {
	return 0;
      }
  }
  const elementType& getConstElement(dimi row, dimi col)
  {
    if(row == col)
      {
	return (this->elements)[row];
      }
    else
      {
	return 0;
      }
  }
  std::string print()
  {
    std::stringstream printer;
    printer<<"{";
    dimi iter = 0;
    if(iter < (this->elements).size())
      {
	while(true)
	  {
	    printer<<"{";
	    for(dimi miniiter = 0; miniiter < (this->elements.size()) - iter - 1; miniiter++)
	      {
		printer<<"0,";
	      }
	    printer<<(this->elements)[iter];
	    iter++;
	    for(dimi miniiter = iter; miniiter < (this->elements).size(); miniiter++)
	      {
		printer<<",0";
	      }
	    printer<<"}";
	    if(iter < (this->elements).size())
	      {
		printer<<"\n,";
	      }
	    else
	      {
		break;
	      }
	  }
      }
    printer<<"}";
    return printer.str();
  }
  bool is_linearly_independent()
  {
    return !(det() == 0);
  }
  dimi rows()
  {
    if(this->direction)
      {
	return beams;
      }
    else
      {
	return this->split;
      }
  }
  dimi cols()
  {
    if(this->direction)
      {
	return this->split;
      }
    else
      {
	return beams;
      }
  }
  void transpose()
  {
  }
  void hard_transpose()
  {
  }
  std::string printdetails()
  {
    std::stringstream printer;
    printer<<"Object Type: smatrix (Sparse, Diagonal)\n"
	   <<"Rows: "<<rows()<<" Columns: "<<cols()<<"\n"
	   <<"Elements:\n"<<(this->print())<<"\n"
	   <<"Is linearly independent? "<<(this->is_linearly_independent())<<"\n"
	   <<"Determinant: "<<(this->det())<<"\n";
    return printer.str();
  }
};

// The basic augmented matrix class, containing functions common to augmented matrices
struct augmentedmatrix
{
  dimi divider; // After which column is the "dividing line" between the matrices appended to each other. Always maps to columns, so use ^T for rows!
  augmentedmatrix() {}
};

template <class elementType, class containerType = std::vector<elementType>>
struct asmatrix: public smatrix<elementType,containerType>, augmentedmatrix // Asmatrix is an augmented simple matrix, that is, the simple matrix class with an extra augmentation "line" at the column index divider
{
  // Constructors
  asmatrix() {}
  asmatrix(elementType *input, dimi rows, dimi cols, dimi div)
  {
    try
      {
	reserve_container((this->elements),rows*cols);
	(this->basis_used) = NULL;
	for(dimi iter = 0; iter < (rows*cols); iter++)
	  {
	    (this->elements).push_back(input[iter]);
	  }
      }
    catch (const std::exception& e)
      {
	(this->elements).clear();
	(this->elements).assign(((this->elements).size()), 0);
	(this->basis_used) = &VECTOR_ERROR;
      }
    (this->split) = cols;
    (this->direction) = true;
    (this->divider) = div;
  }
  asmatrix(dimi length, bool dir, dimi div)
  {
    (this->basis_used) = NULL;
    (this->split) = length;
    (this->direction) = dir;
    (this->divider) = div;
  }
  asmatrix(dimi rows, dimi cols, dimi div)
  {
    (this->basis_used) = NULL;
    (this->split) = cols;
    (this->direction) = true;
    (this->elements).reserve(rows*cols);
    (this->divider) = div;
  }
  asmatrix(dimi rows, dimi cols, elementType setValue, dimi div)
  {
    (this->basis_used) = NULL;
    (this->split) = cols;
    (this->direction) = true;
    (this->elements).assign((rows*cols), setValue);
    (this->divider) = div;
  }
  asmatrix(const smatrix<elementType>& input, dimi div)
  {
    (this->basis_used) = input.basis_used;
    (this->split) = input.split;
    (this->direction) = input.direction;
    (this->elements) = input.elements;
    (this->divider) = div;
  }
  asmatrix(const asmatrix<elementType>& input)
  {
    (this->basis_used) = input.basis_used;
    (this->split) = input.split;
    (this->direction) = input.direction;
    (this->elements) = input.elements;
    (this->divider) = input.divider;
  }
  template<class inputType>
  asmatrix(const std::initializer_list<inputType>& input, dimi split, dimi div, bool dir = true)
  {
    reserve_container((this->elements),input.size());
    for(auto element : input)
      {
	(this->elements).push_back(element);
      }
    (this->split) = split;
    (this->divider) = div;
    (this->direction) = dir;
    (this->basis_used) = NULL;
  }
  asmatrix(const std::initializer_list<elementType>& input, dimi split, dimi div, bool dir = true)
  {
    (this->elements) = input;
    (this->split) = split;
    (this->divider) = div;
    (this->direction) = dir;
    (this->basis_used) = NULL;
  }
  void rref_left()
  {
    this->row_reduce((this->rows()),(this->divider));
    this->echelonize(0,0,(this->rows()),(this->divider));
  }
  void rref_right()
  {
    this->row_reduce(0,(this->divider),(this->rows()),(this->cols()));
    this->echelonize(0,(this->divider),(this->rows()),(this->cols()));
  }
  std::string print()
  const {
    std::stringstream printer;
    printer<<"{";
    for(dimi iter = 0; iter < (((this->elements).size())/(this->split)); iter++)
      {
	if(!(iter == 0))
	  {
	    printer<<"\n,";
	  }
	dimi calibrator = iter*(this->split);
	printer<<"{"<<(this->elements)[calibrator];
	for(dimi miniiter = 1; miniiter < (this->split); miniiter++)
	  {
	    if((miniiter == (this->divider))&&(this->direction))
	      {
		printer<<"|";
	      }
	    printer<<","<<(this->elements)[calibrator+miniiter];
	  }
	printer<<"}";
	if(!(this->direction)&&(iter == (this->divider)))
	  {
	    printer<<"\n";
	    for(dimi miniiter = 1; miniiter < (this->split); miniiter++)
	     {
	       printer<<"_";
	     }
	    printer<<"\n";
	  }
      }
    printer<<"}";
    if(!(this->direction))
      {
	printer<<"^T";
      }
    return printer.str();
  }
  std::string printdetails()
  {
    std::stringstream printer;
    printer<<"Object Type: asmatrix (Augmented Matrix)\n"
	   <<"Rows: "<<(this->rows())<<" Columns: "<<(this->cols())<<"\n"
	   <<"Divided at: "<<(this->divider)<<"\n"
	   <<"Elements:\n"<<(this->print())<<"\n"
	   <<"Is linearly independent? "<<(this->is_linearly_independent())<<"\n"
	   <<"Determinant: "<<(this->det())<<"\n";
    return printer.str();
  }
};

template<class ET,class CT>
std::ostream& operator<<(std::ostream& O, const smatrix<ET,CT>& M)
{
  return M.print(O);
}

#endif
