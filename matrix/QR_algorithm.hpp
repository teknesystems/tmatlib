// The QR algorithm implemented using Givens rotations.

// Header guard. Skips over entire header if already included.
#ifndef _TMATLIB_QR_ALGO_INCLUDED_
#define _TMATLIB_QR_ALGO_INCLUDED_

#include "QR_decomposition.hpp"
#include "../printers.hpp"
#include <iostream>

struct within_epsilon
{
  template<class T>
  bool operator() (T IN)
  {
    return std::abs(IN) < std::abs(std::numeric_limits<T>::epsilon());
  }

};

struct QR_algorithm
{
  template<class L, class R>
  struct right_switch
  {
    typedef R type;
  };
  template<class L>
  struct right_switch<L,void>
  {
    typedef L type;
  };
  template<class DT = void, class IT, class VT1, class VT2, class VT3, class VT4>
  inline static IT write_roots(IT& R, VT1 a, VT2 b, VT3 c, VT4 d)
  {
    if((c==0)||(b==0))
      {
	*R = a;
	R++;
	*R = b;
	R++;
      }
    else
      {
	auto B = a + d;
	typename right_switch<decltype(B),DT>::type discriminant = B*B - 4*(a*d - b*c);
	auto D = std::sqrt(discriminant); //TODO: add "complex switch" based off iterator type
	*R = (B+D)/2.0;
	R++;
	*R = (B-D)/2.0;
	R++;
      }
    return R;
  }
  template<class DT = void>
  struct standard_check
  {
    
    template<class RIT, class MIT, class ZCH>
    bool operator() (RIT R, MIT Q, dimi C, dimi& N, ZCH Z)
    {
      if(N == 1)
	{
	  *R = *Q;
	  R++;
	  N = 0;
	  return false; // End algorithm, final eigenvalue extracted from last element (or eigenvalue is only element)
	}
      else
	{
	  auto A = *(std::next(Q,(N-1)*C + N-2));
	  if(Z(A))
	    {
	      *R = *(std::next(Q,(N-1)*C + N-1));
	      R++;
	      N--;
	      return false; // Call on submatrix, eigenvalue extracted from diagonal (1x1 submatrix)
	    }
	  else
	    {
	      if(N==2)
		{
		  write_roots<DT>(R,*std::next(Q,(N-2)*C + N-2),*std::next(Q,(N-2)*C + N-1),*std::next(Q,(N-1)*C + N-2),*std::next(Q,(N-1)*C + N-1));
		  N-=2;
		  return false; // End algorithm, 2 eigenvalues extracted quadratically from 2x2 submatrix
		}
	      else if(Z(*(std::next(Q,(N-2)*C + N-3))))
		{
		  write_roots<DT>(R,*std::next(Q,(N-2)*C + N-2),*std::next(Q,(N-2)*C + N-1),*std::next(Q,(N-1)*C + N-2),*std::next(Q,(N-1)*C + N-1));
		  N-=2;
		  return false; // Call on submatrix, 2 eigenvalues extracted quadratically from 2x2 submatrix
		}
	      else
		{
		  return true; // Iterate again, no eigenvalues extracted
		}
	    }
	}
    }
  };
  template<class DT = std::complex<double>>
  struct log_check
  {
    template<class RIT, class MIT, class ZCH>
    bool operator() (RIT R, MIT Q, dimi C, dimi& N, ZCH Z)
    {
      static std::intmax_t iter = 1;
      auto CH = standard_check<DT>();
      iter++;
      dimi temp = N;
      bool res = CH(R,Q,C,N,Z);
      if(temp > N)
	{
	  std::cout<<"Eigendecomposition ~"<<((static_cast<double>(C)-N)/C)*100<<"% complete\n";
	}
      if(!(iter%100))
	{
	  std::cout<<"Iteration #"<<iter<<"\n";
	}
      if(N == 0)
	{
	  iter = 0;
	}
      return res;
    }
  };
  struct manual_check
  {
    template<class RIT, class MIT, class ZCH>
    bool operator() (RIT R, MIT Q, dimi C, dimi& N, ZCH Z)
    {
      std::cout<<"Current matrix: \n";
      matrixPrinter::print_iter_to(std::cout,Q,C,C)<<"\nContinue (0- HLT, 1 - WE, 2 - WQ, def - ST)? ";
      int in;
      std::cin>>in;
      switch(in)
	{
	case 0:
	  N = 0; // Halt algorithm
	  return false; // Halt recursion
	case 1:
	  *R = *(std::next(Q,((N-1)*C)+(N-1)));
	  R++;
	  N--;
	  return false;
	case 2:
	  write_roots(R,*std::next(Q,(N-2)*C + N-2),*std::next(Q,(N-2)*C + N-1),*std::next(Q,(N-1)*C + N-2),*std::next(Q,(N-1)*C + N-1));
	  N-=2;
	  return false;
	default:
	  return true;
	}
    }
  };
  struct last_element
  {
    template<class MIT, class VT>
    void operator() (MIT Q, dimi N, dimi C, VT w)
    {
      for(dimi k = 0; k < N; k++)
	{
	  *Q -= w;
	  std::advance(Q,C+1);
	}
    }
    template<class MIT>
    auto operator() (MIT Q, dimi N, dimi C) -> typename std::remove_reference<decltype(*Q)>::type
    {
      auto w = *(std::next(Q,(N-1)*C + (N-1)));
      operator() (Q,N,C,w);
      return w;
    }
  };
  template<class DT = void, dimi iter_cap = 100, class RIT, class MIT, class WIT, class CFT = standard_check<DT>, class ZC = within_epsilon, class SHFT = last_element>
  inline static void write_eigenvalues(RIT R, MIT Q, dimi C, dimi N, WIT W, CFT CHK = CFT(), ZC ZCH = ZC(), SHFT shift = SHFT())
  {
    if(N != 0)
      {
	dimi maxiter = std::min(iter_cap*C,10000uL);
	dimi iter = 0;
	while(CHK(R,Q,C,N,ZCH)&&(iter<maxiter))
	  {
	    auto w = shift(Q,N,C);
	    QR_decomposition::step(Q,W,C,N);
	    shift(Q,N,C,-w);
	    iter++;
	  }
	//std::cout<<"Iterations: "<<iter<<" on "<<N<<"\n";
	write_eigenvalues(R,Q,C,N,W,CHK,ZCH);
      }
    else
      {
	return;
      }
  }
};

#endif
