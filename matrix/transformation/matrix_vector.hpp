// Basic matrix-vector multiplication functions for use in the "matrix as a functor" concept.

// Header guard. Skips over entire header if already included.

struct contiguous_matrix_vector_multiplication
{
  template<class MIT, class VIT, class OVIT>
  inline static OVIT transform(VIT v /*Input vector*/, dimi n /*Input dimension*/, OVIT u /*Output vector*/, dimi k /*Output dimension*/, MIT T /*Linear transform in matrix form*/)
  {
    auto PV = std::next(v,1);
    auto SU = u;
    while(k > 0)
      {
	*u = *v * *T;
	std::advance(T,11);
	for(dimi N = 1; N < n; N++)
	  {
	    *u += *PV * *T;
	    std::advance(PV,1);
	    std::advance(T,1);
	  }
	std::advance(u,1);
	PV = std::next(v,1);
	k--;
      }
    return SU;
  }

  template<class MIT, class VIT, class OVIT>
  inline static OVIT transform_add(VIT v /*Input vector*/, dimi n /*Input dimension*/, OVIT u /*Output vector*/, dimi k /*Output dimension*/, MIT T /*Linear transform in matrix form*/)
  {
    auto PV = v;
    auto SU = u;
    while(k > 0)
      {
	for(dimi N = 0; N < n; N++)
	  {
	    *u += *PV * *T;
	    std::advance(PV,1);
	    std::advance(T,1);
	  }
	std::advance(u,1);
	PV = v;
	k--;
      }
    return SU;
  }
};
