// Matrix RRF, "echelonization" and RREF via Gaussian elimination. Support for determinant and inversion tagalongs.

// Header guard. Skips over entire header if already included
#ifndef _T_MATRIX_RR_INCLUDED_
#define _T_MATRIX_RR_INCLUDED_

#include "system_elrops.hpp"

struct matrixRRF
{
  template<class containerType, class tagalongType>
  inline static void elimination_reduce(containerType& A, dimi rows, dimi cols, tagalongType& T, dimi rstart, sdimi cstart, dimi rend, dimi cend)
  {
    auto PB = container_beginning(A);
    auto PAE = std::next(PB,(rend*cols));
    std::advance(PAE,-1);
    for(auto at_row = std::next(PB,(rstart*cols)); at_row < PAE; std::advance(at_row,cols)) // Generates pointer to first element of current scanned row
      {
	// Initialization of pointers used
	auto modrow = at_row; // Pointer to current element to begin computation on, that is, the ACTIVE ELEMENT (defined here)
	std::advance(modrow,cstart); // Brings the pointer to the starting column
	// Stage I: Checks if active element is zero, if so, swaps rows such that a nonzero row is first. Uses only the swap_beams ELBOP.
	if(*modrow == 0.0)
	  {
	    auto searchedrow = modrow; // Pointer to element searched of row searched, initialized at current active element.
	    do
	      {
		std::advance(searchedrow,cols); // Pointer brought 1 row down (why search the current row twice!)
		tagalongELROPS::srowDown(T);
		while(searchedrow<PAE) // Searching until end of matrix, row by row, at column "cstart"
		  {
		    if(*(searchedrow)!=0.0) // Checking for nonzero element
		      {
			std::advance(searchedrow,-cstart); // Shifting the searched row back to the beginning of the currently searched row (e.g. away from the search start point)
			tagalongELROPS::swap(T,searchedrow,at_row,cols); // Swapping the *entire* searched row and current row. This multiplies the determinant by -1...
			goto row_swap_successful_break; // Goto just to exit the loop and save a potentially expensive comparsion operation
		      }
		    std::advance(searchedrow,cols); // Advances the search pointer down 1 row
		  }
		if(static_cast<dimi>(cstart) >= cend) // If column is greater than the last column (cend = cols is default), terminate row reduction.
		  {
		    return;
		  }

		cstart++; // Increment column to search in further operations.
		modrow++; // Increment active element pointer by 1 to represent this increase in current column.
		if(modrow >= PAE) // Fixes some errors.
		  {
		    return;
		  }
		searchedrow = modrow; // Re-set searched row to new active element
		tagalongELROPS::srowMod(T);
	      } while (*modrow == 0.0);
	  }
      row_swap_successful_break:; // Label for exit point of nested row_swap loop. Saves a potentially expensive dereference and comaparsion by using a GOTO, extensive commenting for safety and clarity.

	// Stage II: Normalize the active element (e.g. turn it to 1), multiplying all other elements in the row by the appropriate values
	auto at_row_end = at_row; // Generates pointer to the beginning of the current row
	std::advance(at_row_end,(cols)); // Shifts pointer to the end of the current row, NOT the next row
	tagalongELROPS::multiply(T,at_row,at_row_end,(1.0/(*modrow))); // Multiplies all of current row by 1/active element, normalizing the active element in the process

	// Stage III: Peform FMA on all rows between at_row and rend to perform 1 round of reduction

	auto current_row = at_row_end;
	std::advance(modrow,cols);
	std::advance(at_row_end,cols);
	tagalongELROPS::srowMod(T);
	tagalongELROPS::rowDown(T);
	while(current_row < PAE)
	  {
	    tagalongELROPS::add(T,current_row,at_row_end,at_row,-(*modrow));
	    std::advance(current_row,cols);
	    std::advance(at_row_end,cols);
	    std::advance(modrow,(cols));
	    tagalongELROPS::rowDown(T);
	  }

	// Cleanup stage:
	// Increment starting column for reduction
	cstart++;
	tagalongELROPS::srowDown(T);
	tagalongELROPS::rowSrc(T);
      }
  }
  template<class containerType>
  inline static void elimination_reduce(containerType& A, dimi r, dimi c, dimi sr, dimi sc, dimi re, dimi ce)
  {
    null_tagalong T;
    elimination_reduce(A,r,c,T,sr,sc,re,ce);
  }
};

struct matrixEquality
{
  template<class container1, class container2>
  inline static bool equal_to(container1& A, container2& B, dimi N, dimi swap2 = 0)
  {
    if(swap2==0)
      return elementwiseEquality::equal_to(A,B);
    else
      {
	dimi s = N/swap2;
	auto BA = container_beginning(A);
	auto BB = container_beginning(B);
	for(dimi iter = 0; iter < s; iter++)
	  {
	    auto BROW = BB;
	    for(dimi miniiter = 0; miniiter < swap2; miniiter++)
	      {
		if(!almost_equal(*BA,*BROW))
		  {
		    return false;
		  }
		std::advance(BA,1);
		std::advance(BROW,s);
	      }
	    BB++;
	  }
	return true;
      }
  }
};

struct matrixEchelonization
{
  template<class containerType, class tagalongType>
  inline static void echelonize_reduced(containerType& A, dimi rows, dimi cols, tagalongType& T, dimi rstart, dimi cstart, sdimi rend, sdimi cend)
  {
    auto ARB = std::next(container_beginning(A),(rstart*cols));
    auto ARE = std::next(ARB,cols);
    dimi advance = 1;
    auto AB = std::next(ARB,cstart+advance);
    auto AE = std::next(ARB,cend);
    auto RE = std::next(container_beginning(A),(rend*cols));
    tagalongELROPS::srowReset(T);
    tagalongELROPS::rowReset(T);
    tagalongELROPS::srowDown(T);
    while(ARB < RE)
      {
	auto NARB = ARE;
	while(AB < AE)
	  {
	    //std::cout<<*AB<<"  "<<*(AB+1)<<"  "<<*NARB<<"\n";
	    tagalongELROPS::add(T,ARB,ARE,NARB,-(*AB));
	    std::advance(AB,1);
	    std::advance(NARB,cols);
	    tagalongELROPS::srowDown(T);
	  }
	std::advance(ARB,cols);
	std::advance(ARE,cols);
	AB = AE;
	AE = std::next(AB,cols);
	advance++;
	std::advance(AB,advance);
	tagalongELROPS::rowDown(T);
	tagalongELROPS::srowMod(T);
	tagalongELROPS::srowDown(T);
      }
  }

  template<class containerType>
  inline static void echelonize_reduced(containerType& A, dimi r, dimi c, dimi rs, dimi cs, sdimi re, sdimi ce)
  {
    null_tagalong status;
    echelonize_reduced(A,r,c,status,rs,cs,re,ce);
  }
};

struct matrixRREF
{
  template<class containerType, class tagalongType>
  inline static void to_rref(containerType& A, dimi r, dimi c, tagalongType& T, dimi sr, dimi sc, dimi re, dimi ce)
  {
    matrixRRF::elimination_reduce(A,r,c,T,sr,sc,re,ce);
    matrixEchelonization::echelonize_reduced(A,r,c,T,sr,sc,re,ce);
  }
  template<class containerType>
  inline static void to_rref(containerType& A, dimi r, dimi c, dimi sr, dimi sc, dimi re, dimi ce)
  {
    null_tagalong T;
    to_rref(A,r,c,T,sr,sc,re,ce);
  }
};

#endif
