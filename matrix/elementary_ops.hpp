// Elementary operations on arrays of data for matrix, vector and general use.
// Including elementary beam (array row) operations, segmented beam operations (array column), array equality checking and rapid matrix/array block filling with specified value patterns.

// Header guard (skips over entire header if already included)
#ifndef _T_VEC_ELEM_INCLUDED_
#define _T_VEC_ELEM_INCLUDED_

//#define _T_ELEM_SCAFFOLDING_

// Swap algorithms:

// Standard, elementwise row/column (beam/segmented beam) swaps
struct elementwiseSwap
{
  // Standard, full row/column swaps:
  
  // Inheritable binding for std::swap_ranges with automatic parallelization and potential specialized overloads for certain input iterator types 
  template<class iter1, class iter2, class iter3>
  inline static void swap_beams(iter1 A, iter2 AE, iter3 B)
  {
    std::swap_ranges(
		#ifdef _TMATLIB_CPPZ_EXEC_ENABLED_
		std::execution::par,
		#endif
		A,AE,B);
  }
  template<class iter1, class iter2>
  inline static void swap_beams(iter1 A, iter2 B, dimi n)
  {
    auto AE = A;
    std::advance(AE,n);
    swap_beams(A,AE,B);
  }
  template<bool transpose = false, class iter1, class iter2>
  inline static void segmented_swap(iter1 A, iter2 B, dimi n, dimi m)
  {
    using std::swap;
    jump_pointer<iter1,transpose> JA (A,n);
    jump_pointer<iter2,transpose> JB (B,n);
    for(dimi iter = 0; iter < m; iter++)
      {
	swap(*JA,*JB);
	JA++;
	JB++;
      }
  }
  template<class containerType>
  inline static void swap_beams(containerType& A, dimi beam1, dimi beam2, dimi cols)
  {
    auto SA = container_beginning(A);
    auto SB = SA;
    std::advance(SA,(cols*beam1));
    std::advance(SB,(cols*beam2));
    auto SAE = SA;
    SAE = SA;
    std::advance(SAE,cols);
    swap_beams(SA,SAE,SB);
  }
  template<bool transpose = false, class containerType>
  inline static void swap_segmented_beams(containerType& A, dimi beam1, dimi beam2, dimi rows, dimi cols)
  {
    jump_pointer<decltype(container_beginning(A)),transpose> JA (container_beginning(A),cols);
    auto JB = JA;
    JA>>beam1;
    JB>>beam2;
    auto JAE = JA;
    JAE+=rows;
    swap_beams(JA,JAE,JB);
  }

  // Partial swaps:

  // Partial fill swaps:
};

// Quick swap algorithms on diagonal matrices
// Basically, generally, flipping two appropriate elements, but binding to the generalized inheritable swap operation system
struct diagonalSwap
{
  
};

// Beam addition
struct elementwiseBeamAddition
{
  // Standard, full row/column addition
  
  // Row (beam) fused multiply add
  template<class containerType, class scalarType>
  inline static void rFMA(containerType& A, dimi beam1, dimi beam2, const scalarType& scalar, dimi cols) 
  {
    auto PA = container_beginning(A);
    auto PB = PA;
    std::advance(PA,cols*beam1);
    std::advance(PB,cols*beam2);
    auto PAE = PA;
    std::advance(PAE,cols);
    elementwiseFMA::FMA(PA,PAE,PB,scalar);
  }
  // Column (segmented beam) fused multiply add
  template<bool transpose = false, class containerType, class scalarType>
  inline static void srFMA(containerType& A, dimi beam1, dimi beam2, const scalarType& scalar, dimi cols, dimi rows) 
  {
    jump_pointer<decltype(container_beginning(A)),transpose> PA (container_beginning(A),cols);
    auto PB = PA;
    PA>>beam1;
    PB>>beam2;
    auto PAE = PA;
    PAE+=rows;
    elementwiseFMA::FMA(PA,PAE,PB,scalar);
  }

  // Partial addition:

  // Partial row (beam) fused multiply add: Performs FMA on a section of row of length col-start, starting at element start
  template<class containerType, class scalarType>
  inline static void prFMA(containerType& A, dimi beam1, dimi beam2, const scalarType& scalar, dimi cols, dimi start) 
  {
    auto PA = container_beginning(A);
    auto PB = PA;
    std::advance(PA,cols*beam1);
    std::advance(PB,(cols*beam2+start));
    auto PAE = PA;
    std::advance(PAE,cols);
    std::advance(PA,start);
    elementwiseFMA::FMA(PA,PAE,PB,scalar);
  }

  // Column (segmented beam) partial fused multiply add: Performs FMA on a section of segmented beam of length (element count) col-start, starting at element start
  template<bool transpose = false, class containerType, class scalarType>
  inline static void psrFMA(containerType& A, dimi beam1, dimi beam2, const scalarType& scalar, dimi cols, dimi rows, dimi start) 
  {
    jump_pointer<decltype(container_beginning(A)),transpose> PA (container_beginning(A),cols);
    auto PB = PA;
    PA>>beam1;
    PB>>beam2;
    auto PAE = PA;
    PAE+=rows;
    PA+=start;
    PB+=start;
    elementwiseFMA::FMA(PA,PAE,PB,scalar);
  }
  
  // Partial fill addition: fills section of segmented beam 2 of length "n" with fill_scalar (defaults to 0), performs FMA on the remainder of beam 2 and beam 1 with weight scalar.
  
  template<class containerType, class scalarType, class scalarType2 = scalarType>
  inline static void pfrFMA(containerType& A, dimi beam1, dimi beam2, const scalarType& scalar, dimi cols, dimi shift, const scalarType2& fill_scalar = 0) 
  {
    auto PA = container_beginning(A);
    auto PB = PA;
    std::advance(PB, beam1*cols + shift);
    std::advance(PA, beam1*cols);
    auto PAE = PA;
    std::advance(PAE,shift);
    std::fill(PA,PAE,fill_scalar);
    PAE = PA;
    std::advance(PAE,cols);
    std::advance(PA,shift);
    elementwiseFMA::FMA(PA,PAE,PB,scalar);
  }

  template<bool transpose = false, class containerType, class scalarType, class scalarType2 = scalarType>
  inline static void pfsrFMA(containerType& A, dimi beam1, dimi beam2, const scalarType& scalar, dimi cols, dimi rows, dimi shift, const scalarType2& fill_scalar = 0) 
  {
    jump_pointer<decltype(container_beginning(A)),transpose> PA (container_beginning(A),cols);
    auto PB = PA;
    PA>>beam1;
    PB>>beam2;
    auto PAE = PA;
    PAE+=shift;
    std::fill(PA,PAE,fill_scalar);
    PAE = PA;
    PAE+=rows;
    PA+=shift;
    PB+=shift;
    elementwiseFMA::FMA(PA,PAE,PB,scalar);
  }

  //pfrFMA on iterators
  template<class iter1, class iter2, class scalarType, class scalarType2 = scalarType>
  inline static void pfrFMA(iter1& A, iter1& AE, iter2 B, dimi shift, const scalarType& scalar, const scalarType2& fill_scalar = 0)
  {
    auto C = A;
    std::advance(C,shift);
    std::advance(B,shift);
    std::fill(A,C,fill_scalar);
    elementwiseFMA::FMA(C,AE,B,scalar);
  }
};

// Row multiplication
struct elementwiseBeamMultiplication
{
  // Standard, full row/column multiplication
  
  template<class containerType, class scalarType>
  inline static void beamMultiplication(containerType& A, dimi beam, const scalarType& scalar, dimi cols)
  {
    auto PA = container_beginning(A);
    std::advance(PA,(cols*beam));
    auto PAE = PA;
    std::advance(PAE,cols);
    elementwiseScalarMultiplication::op(PA,PAE,scalar);
  }
  template<bool transpose = false, class containerType, class scalarType>
  inline static void sbeamMultiplication(containerType& A, dimi beam, const scalarType& scalar, dimi cols, dimi rows)
  {
    jump_pointer<decltype(container_beginning(A)),transpose> PA (container_beginning(A),cols);
    PA>>beam;
    auto PAE = PA;
    PAE+=rows;
    elementwiseScalarMultiplication::op(PA,PAE,scalar);
  }

  // Partial multiplication:

  // On iterators, without shift:
  template<class iterType, class scalarType>
  inline static void beamMultiplication(iterType& A, iterType& AE, const scalarType& scalar)
  {
    elementwiseScalarMultiplication::op(A,AE,scalar);
  }
  // On iterators with shift:
  template<class iterType, class scalarType>
  inline static void beamMultiplication(iterType A, iterType& AE, const scalarType& scalar, dimi shift)
  {
    std::advance(A,shift);
    elementwiseScalarMultiplication::op(A,AE,scalar);
  }
  
  // Partial fill multiplication:
};

#define _STANDARD_ELOP_FUNCTIONS_ elementwiseBeamMultiplication,elementwiseBeamAddition,elementwiseSwap,elementwiseScalarMultiplication

// Standard ELBOP/ELSBOP interpreter
template<class multiplySingle = elementwiseBeamMultiplication, class add = elementwiseBeamAddition, class swap_beams = elementwiseSwap, class multiplyAll = elementwiseScalarMultiplication>
struct elops_on_container
{
  // Standard ELOPS. Note these functions are "unsafe", and bounds checking should be done *before* calling them.
  //TODO: Potentially implement "safe" versions
  template<class containerType, class scalarType>
  inline static void elbop(containerType& A, dimi beam, dimi swap, const scalarType& weight, dimi rows, dimi cols)
  {
    #ifdef _T_ELEM_SCAFFOLDING_
    std::cout<<"Elementary beam operation: ";
    #endif
    if(swap == std::numeric_limits<dimi>::max())
      {
	if(beam == std::numeric_limits<dimi>::max())
	  {
	    #ifdef _T_ELEM_SCAFFOLDING_
	    std::cout<<"Multiplying all elements by "<<weight<<"\n";
	    #endif
	    multiplyAll::op(A,weight);
	  }
	else
	  {
	    #ifdef _T_ELEM_SCAFFOLDING_
	    std::cout<<"Multiplying beam "<<beam<<" by "<<weight<<" (taking into account "<<cols<<" columns)\n";
	    #endif
	    multiplySingle::beamMultiplication(A,beam,weight,cols);
	  }
      }
    else
      {
	if(weight == 0.0/*weight == additiveIdentity<scalarType>.getValue();*/)
	  {
	    #ifdef _T_ELEM_SCAFFOLDING_
	    std::cout<<"Swapping beams "<<beam<<" and "<<swap<<" (taking into account "<<cols<<" columns)\n";
	    #endif
	    swap_beams::swap_beams(A,beam,swap,cols);
	  }
	else
	  {
	    #ifdef _T_ELEM_SCAFFOLDING_
	    std::cout<<"Performing rFMA on "<<beam<<" and "<<swap<<" with weight "<<weight<<" (taking into account "<<cols<<" columns)\n";
	    #endif
	    add::rFMA(A,beam,swap,weight,cols);
	  }
      }
  }
  template<bool transpose = false, class containerType, class scalarType>
  inline static void elsbop(containerType& A, dimi beam, dimi swap, const scalarType& weight, dimi rows, dimi cols)
  {
    if(swap == std::numeric_limits<dimi>::max())
      {
	if(beam == std::numeric_limits<dimi>::max())
	  {
	    multiplyAll::op(A,weight);
	  }
	else
	  {
	    multiplySingle::sbeamMultiplication/*<transpose>*/(A,beam,weight,cols,rows);
	  }
      }
    else
      {
	if(weight == 0.0/*weight == additiveIdentity<scalarType>.getValue();*/)
	  {
	    swap_beams::swap_segmented_beams/*<transpose>*/(A,beam,swap,cols,rows);
	  }
	else
	  {
	    add::srFMA/*<transpose>*/(A,beam,swap,weight,cols,rows);
	  }
      }
  }
};

template <class multiplierType>
struct elementaryBeamOperation: public symbolicOperation // Beam, as these operations are applied to straight "beams" of numbers, usually rows/columns, but applicable to many things and other mathematical objects!
{
  dimi beam;
  dimi swap;
  multiplierType weight;
  // If swap = -1 (note here swap is unsigned, so -1 is equivalent to the maximum value)
  // Then, if beam = n =/= -1, multiply row/col beam by weight, else, multiply ENTIRE MATRIX by weight w
  // If swap = n != -1 then:
  // If weight = additiveIdentity(multiplierType) (0 for now), swap rows/cols beam and swap
  // If weight =/= additiveIdentity(multiplierType) (0 for now), add row/col swap times weight to row/col beam
  elementaryBeamOperation(int b, int s, multiplierType w): beam (b), swap (s), weight (w)
  {
  }
  multiplierType det()
  {
    if (swap == std::numeric_limits<dimi>::max())
      {
	return weight;
      }
    else
      {
	if (weight == 0) //TODO: check for equality to additive identity
	  {
	    return -1; //TODO: return additive inverse of multiplicative identity
	  }
	else
	  {
	    return 1; //TODO: return multiplicative identity
	  }
      }
  }
  multiplierType det(dimi anti_index_size)
  {
    if (swap == std::numeric_limits<dimi>::max())
      {
	if (beam == std::numeric_limits<dimi>::max())
	  {
	    return std::pow(weight, anti_index_size);
	  }
	return weight;
      }
    else
      {
	if (weight == 0) //TODO: check for equality to additive identity
	  {
	    return -1; //TODO: return additive inverse of multiplicative identity
	  }
	else
	  {
	    return 1; //TODO: return multiplicative identity
	  }
      }
  }
  std::string print()
  {
    std::stringstream printer;
    if (swap == std::numeric_limits<dimi>::max())
      {
	if (beam == std::numeric_limits<dimi>::max())
	  {
	    printer<<"Multiply ENTIRE MATRIX by "<<weight<<"\n";
	    return printer.str();
	  }
	printer<<"Multiply BEAM #"<<beam<<" by "<<weight<<"\n";
	return printer.str();
      }
    else
      {
	if (weight == 0)
	  {
	    printer<<"Swap BEAM #"<<beam<<" and "<<swap<<"\n";
	    return printer.str();
	  }
	else
	  {
	    printer<<"Add BEAM #"<<swap<<" to "<<beam<<"\n";
	    return printer.str();
	  }
      }
  }
};

template <class multiplierType>
struct elementaryArray
{
  elementaryArray() {};
  std::deque<elementaryBeamOperation<multiplierType>> components;
  multiplierType det()
  const {
    multiplierType result = 1;
    for(int iter = 0; iter < components.size(); iter++)
      {
	result*=(components[iter].det());
      }
    return result;
  }
  multiplierType det(dimi anti_index_size)
  {
    multiplierType result = 1;
    for(dimi iter = 0; iter < components.size(); iter++)
      {
	result*=components[iter].det(anti_index_size);
	//std::cout<<"COMPONENT DETERMINANT: "<<components[iter].det(anti_index_size)<<"For OPERATION: "<<components[iter].print(); //Scaffolding
      }
    return result;
  }
  void swap (int b, int s)
  {
    components.emplace_back(b, s, 0);
  }
  void add (int b, int s, multiplierType w)
  {
    components.emplace_back(b, s, w);
  }
  void add (int b, int s)
  {
    components.emplace_back(b, s, 1);
  }
  void multiply (int b, multiplierType w)
  {
    components.emplace_back(b, -1, w);
  }
  void postmultiply (elementaryBeamOperation<multiplierType> input)
  {
    components.push_back(input);
  }
  void premultiply (elementaryBeamOperation<multiplierType> input)
  {
    components.push_front(input);
  }
  void postmultiply (int b, int s, multiplierType w)
  {
    components.emplace_front(b, s, w);
  }
  void premultiply (int b, int s, multiplierType w)
  {
    components.emplace_back(b, s, w);
  }
};

#endif
