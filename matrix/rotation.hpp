// Givens rotations, rotational matrices and other matrix operations involving trigonometric functions and rotations.

// Header guard. Skips over entire header if already included
#ifndef _T_MATRIX_ROT_INCLUDED_
#define _T_MATRIX_ROT_INCLUDED_

#include <cmath> // For trigonometric functions

struct contiguousRotation
{
  template<bool transpose = true, class IT, class IT2, class IT3, class FT>
  inline static IT rotate_write(IT W, IT2 I, IT3 J, dimi cols, FT c, FT s)
  {
    (*W) = c;
    std::advance(W,1);
    _TMATLIB_CONST_IF_ (transpose)
      {
	(*W) = -s;
      }
    else
      {
	(*W) = s;
      }
    rotate(I,J,cols,c,s);
    return W;
  }
  template<bool right = false, class iterType, class iterType2, class floatType>
  inline static void rotate(iterType I, iterType2 J, dimi cols, floatType c, floatType s, dimi N)
  {
    for(dimi k = 0; k < N; k++)
      {
	//std::cout<<(*I)<<","<<(*J)<<"\n";
	typename std::remove_reference<decltype(*I)>::type T = *I; // Copy over I whilst removing the reference
	//std::cout<<(*I)<<","<<(*J)<<"\n";
       	(*I)*=c;
	(*I)-=(s)*(*J);
	(*J)*=c;
	(*J)+=(s)*(T);
	_TMATLIB_CONST_IF_ (right)
	{
	  std::advance(J,cols);
	  std::advance(I,cols);
	}
	else
	  {
	    std::advance(J,1);
	    std::advance(I,1);
	  }
      }
  }
  template<bool right = false, class iterType, class iterType2, class floatType>
  inline static void rotate(iterType I, iterType2 J, dimi cols, floatType c, floatType s)
  {
    rotate<right>(I,J,cols,c,s,cols);
  }
  template<bool right = false, class IT1, class IT2, class FT>
  inline static void rotate_ref(IT1& I, IT2& J, dimi cols, FT c, FT s)
  {
    rotate<right>(I,J,cols,c,s);
  }
  template<bool right = false, class iterType, class floatType>
  inline static void rotate(iterType A, dimi cols, dimi i, dimi j, floatType theta)
  {
    auto I = A;
    auto J = std::next(I,j*cols);
    std::advance(I,i*cols);
    rotate<right>(I,J,cols,std::cos(theta),std::sin(theta));
  }
  template<bool right = false, class IT1, class IT2, class HYPT, class FT1, class FT2>
  inline static void zero_rotate(IT1 I, IT2 J, dimi cols, FT1 A, FT2 B, HYPT hyp, dimi N)
  {
    if(B!=0)
      {
	rotate<right>(I,J,cols,(A/hyp),(-B/hyp),N); // Make I and J be incremented by the rotate function via references
      }
  }
  template<bool right = false, class IT1, class IT2, class HYPT, class FT1, class FT2>
  inline static void zero_rotate(IT1 I, IT2 J, dimi cols, FT1 A, FT2 B, HYPT hyp)
  {
    zero_rotate(I,J,cols,A,B,hyp,cols);
  }
  template<bool right = false, class IT>
  inline static void zero_rotate_w(IT A, dimi cols, dimi source_row, dimi zero_col, dimi N, dimi source_diff = 1)
  {
    auto I = std::next(A,source_row*cols);
    auto J = std::next(I,source_diff*(cols));
    auto a = *(std::next(I,zero_col));
    auto b = *(std::next(J,zero_col));
    zero_rotate<right>(I,J,cols,a,b,std::hypot(a,b),N);
  }
  template<bool right = false, class IT>
  inline static void zero_rotate(IT A, dimi cols, dimi source_row, dimi zero_col, dimi source_diff = 1)
  {
    zero_rotate_w(A,cols,source_row,zero_col,cols,source_diff);
  }
  template<bool right = false, class IT, class IT2>
  inline static IT zero_rotate_w(IT W, IT2 A, dimi cols, dimi source_row, dimi zero_col, dimi N, dimi source_diff = 1)
  {
    auto I = std::next(A,source_row*cols);
    auto J = std::next(I,source_diff*(cols));
    auto a = *(std::next(I,zero_col));
    auto b = *(std::next(J,zero_col));
    //std::cout<<"Matrix to be zeroed: ["<<a<<","<<b<<"]^T\n";
    auto hyp = std::hypot(a,b);
    *W = a/hyp;
    std::advance(W,1);
    *W = right?(b/hyp):(-b/hyp);
    zero_rotate<right>(I,J,cols,a,b,hyp,N);
    return std::next(W,1);
  }
  template<bool right = false, class IT, class IT2>
  inline static IT zero_rotate(IT W, IT2 A, dimi cols, dimi source_row, dimi zero_col, dimi source_diff = 1)
  {
    return zero_rotate_w(W,A,cols,source_row,zero_col,cols,source_diff);
  }
  template<bool transpose = true, class IT, class IT2>
  inline static void zero_rotate_right_diag(IT W, IT2 A, int cols)
  {
    for(int k = 0; k < (cols-1); k++)
      {
	W = zero_rotate(W,A,cols,k,k);
      }
  }
  template<bool transpose = true, class IT, class IT2>
  inline static void zero_rotate_right_diag(IT W, IT2 A, dimi cols, int N)
  {
    //std::cout<<"ZRCALLED!\n";
    for(int k = 0; k < N-1; k++)
      {
	W = zero_rotate_w(W,A,cols,k,k,N);
      }
  }
  template<bool right = true, class IT, class CT>
  inline static void rotate_read_diag(IT R, int N, CT& A, int cols)
  {
    auto I = container_beginning(A);
    auto J = std::next(I,cols);
    auto RC = R; // Cosine access
    auto RS = std::next(RC,1); // Sine access
    for(int k = 0; k < (cols-1); k++)
      {
	rotate<right>(I,J,cols,*RC,*RS);
	std::advance(I,cols);
	std::advance(J,cols);
	std::advance(RC,2);
	std::advance(RS,2);
      }
  }
  template<bool right = true, class IT, class IT2>
  inline static void rotate_read_transpose(IT R, IT2 A, dimi cols, int N)
  {
    auto I = A;
    auto J = std::next(I,1);
    auto RC = R; // Cosine access
    auto RS = std::next(RC,1); // Sine access
    for(int k = 0; k < (N-1); k++)
      {
	rotate<right>(I,J,cols,*RC,*RS,N);
	std::advance(I,1);
	std::advance(J,1);
	std::advance(RC,2);
	std::advance(RS,2);
      }
  }
  template<bool right = true, class IT, class IT2>
  inline static void rotate_read_transpose(IT R, IT2 A, int cols)
  {
    auto I = A;
    auto J = std::next(I,1);
    auto RC = R; // Cosine access
    auto RS = std::next(RC,1); // Sine access
    for(int k = 0; k < (cols-1); k++)
      {
	rotate<right>(I,J,cols,*RC,*RS);
	std::advance(I,1);
	std::advance(J,1);
	std::advance(RC,2);
	std::advance(RS,2);
      }
  }
};

// Temporary.
struct matrixRotation: public contiguousRotation
{
};

#endif
