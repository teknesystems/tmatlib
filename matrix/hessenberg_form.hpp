// Tools for converting a matrix to upper and lower Hessenberg form whilst preserving eigenvalues

// Header guard. Skips over entire header if already included
#ifndef _T_MATRIX_HESS_INCLUDED_
#define _T_MATRIX_HESS_INCLUDED_

#include "reflection.hpp"
#include "../numerical_util.hpp"

struct stepper;

struct to_hessenberg_form
{
  template<class T, class T2>
  inline static T2 copy_sign_of(T s, T2 v)
  {
    return (s < 0)?(-v):(v);
  }
  template<class MIT, class VIT, class ST = stepper>
  inline static void convert(MIT Q, dimi N, VIT u, ST step = ST())
  {
    if(N != 0)
      {
	//std::cout<<"Convert validly called with size "<<N<<"!\n";
	auto P = std::next(Q,N);
	auto QSHIFT = std::next(Q,1);
	jump_pointer<decltype(Q)> QJUMP (std::next(Q,N),N);
	// Each iteration should zero a column of the matrix, with N-2 iterations (N-1 - 1) converting the matrix to Hessenberg form:
	for(dimi k = 1; k < N-1; k++)
	  {
	    //std::cout<<"Iteration #"<<k<<" of "<<(N-2)<<"\n";
	    
	    // Stage I: Generate vector u to reflect over
	    // Part 1: Copy over "subcolumn" of length N-k
	    //std::cout<<"Generating vector (STAGE I): [ ";
	    auto IVW = u;
	    auto TP = P;
	    bool flag = true;
	    for(dimi iter = 0; iter < N-k; iter++)
	      {
		*IVW = *TP;
		//std::cout<<(*IVW)<<" ";
		if(flag) // Only check for Hessenberg form if not yet established the matrix is NOT in Hessenberg form
		  {
		    if((iter != 0)&&(*IVW != 0)) // Check for nonzero elements under subdiagonal
		      {
			//std::cout<<" <f> ";
			flag = false;
		      }
		  }
		std::advance(IVW,1);
		std::advance(TP,N);
	      }
	    //std::cout<<"]\n";
	    if(flag)
	      continue; // Signifies column is already in Hessenberg form. Continue to next column.
	    // Part 2: Subtract norm from first element, normalize result
	    //std::cout<<"Preparing and normalizing vector: ";
	    auto norm = floatingNorm(u,(N-k));
	    //std::cout<<"Start Norm = "<<norm<<"  ";
	    *u+= std::copysign(norm,*u);
	    auto to_norm = u;
	    norm = floatingNorm(u,(N-k));
	    //std::cout<<"Final Norm = "<<norm<<"\n";
	    //std::cout<<"Normalized final vector: [ ";
	    for(dimi iter = 0; iter < N-k; iter++)
	      {
		*to_norm/=norm;
		//std::cout<<*to_norm<<" ";
		to_norm++;
	      }
	    //std::cout<<"]\n";

	    // Stage II: Implicit left multiplication of (N-k)*(N-k) submatrix by I - 2*u*u^T using projection of each column of Q in the direction orthogonal to the vector u (generated above)
	    //std::cout<<"Implicit left multiplication (STAGE II)...\n";
	    auto J = QJUMP;
	    for(dimi iter = 0; iter < N; iter++)
	      {
		elementaryReflection::reflect_normalized(J,(N-k),u); // Calculating orthogonal projection
		J>>1; // Shifting the pointer to the next column
	      }
	    
	    // Stage III: Compute similar matrix by implict right multiplication (N-k)*(N-k) submatrix by I - 2*u*u^T
	    //std::cout<<"Implicit right multiplication (STAGE III)...\n";
	    TP = QSHIFT;
	    for(dimi iter = 0; iter < N; iter++)
	      {
		elementaryReflection::reflect_normalized(TP,(N-k),u);
		std::advance(TP,N);
	      }

	    // Cleanup:
	    std::advance(P,N+1); // Push P forwards to current active submatrix
	    std::advance(QSHIFT,1); // Shift right matrix iterator to represent shrinking reflector submatrix
	    QJUMP++; // Shift left matrix iterator down one row to represent shrinking reflector submatrix
	    step.step();
	    step.outer();
	  }
      }
  }
};

#endif
