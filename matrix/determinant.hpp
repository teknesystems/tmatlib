// Computing the determinant of a matrix

#ifndef _T_MATRIX_DET_INCLUDED_
#define _T_MATRIX_DET_INCLUDED_

#include <iterator>


// Utilities for computing the determinant. TODO: add function to return the determinant for an input 2D array
struct matrixDeterminant
{
  template<class productType, class containerType>
  inline static productType multiplyDiagonal(containerType& E, dimi r)
  {
    auto BE = container_beginning(E);
    auto EE = container_end(E);
    decltype((*BE) * (*EE)) acc = *BE;
    std::advance(BE, r+1);
    while(BE < EE)
      {
	acc *= *BE;
	std::advance(BE,r+1);
      }
    return acc;
  }
  template<class iterType>
  inline static auto multiplyDiagonal(iterType& B, iterType& E, dimi r) -> decltype((*B) * (*E))
  {
    decltype((*B) * (*E)) acc = *B;
    std::advance(B, r+1);
    while(B < E)
      {
	acc*= B;
	std::advance(B, r+1);
      }
    return acc;
  }
};

#endif
