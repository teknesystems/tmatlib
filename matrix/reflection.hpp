// Elementary reflections of matrices

// Header guard (skips over entire header if already included
#ifndef _T_MATRIX_REFL_INCLUDED_
#define _T_MATRIX_REFL_INCLUDED_

struct elementaryReflection
{
  template<class VIT, class NIT>
  inline static void reflect_normalized(VIT u, dimi k, NIT n)
  {
    // dot = u * v
    auto dot = *u * *n;
    auto v = u;
    auto o = n;
    v++;
    o++;
    for(dimi i = 1; i < k; i++)
      {
	dot += *v * *o;
        v++;
	o++;
      }
    // The projection of u orthogonal to n is u - 2(u*n)n
    for(dimi i = 0; i < k; i++)
      {
	*u -= 2*(*n)*dot;
        u++;
        n++;
      }
  }

  // Alternative interface (more in line with projn(u) notation)
  template<class VIT, class NIT>
  inline static void project_normalized(NIT n, VIT u, dimi N)
  {
    reflect_normalized(u,N,n);
  }
};

#endif
