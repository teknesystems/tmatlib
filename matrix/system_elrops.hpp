// Elementary row operations/column operations (ELROPS) to be used on linear systems so as to:
// Compute their properties (determinant tagalong)
// Solve then generally or for a specific case (inversion tagalong on input vector or identity matrix)
// Decompose them (ELOP tagalong)

#ifndef _T_MATRIX_SYS_ELROPS_
#define _T_MATRIX_SYS_ELROPS_

#include "elementary_ops.hpp"

// Null tagalong (do nothing)
struct null_tagalong
{
};

// Temporary tagalong container
template<class elementType>
struct det_tagalong
{
  elementType det_value;
  det_tagalong(): det_value(1) {}
  template<class inputType>
  det_tagalong(inputType x): det_value(x) {}
};

template<class dataPointerType, class pointerType = dataPointerType>
struct inversion_tagalong
{
  dataPointerType data;
  sdimi split;
  pointerType mod;
  pointerType src;
  template<class scalarType>
  void add(const scalarType& S)
  {
    //std::cout<<"ADD: "<<S<<"*"<<(*src)<<" TO "<<(*mod)<<"\n";
    elementwiseFMA::FMA(mod,std::next(mod,split),src,S);
    //std::cout<<"GET: "<<*mod<<"\n";
  }
  template<class scalarType>
  void mult(const scalarType& S)
  {
    //std::cout<<"MULT: "<<S<<"\n";
    elementwiseScalarMultiplication::op(mod,std::next(mod,split),S);
  }
  void swap()
  {
    elementwiseSwap::swap_beams(mod,std::next(mod,split),src);
  }

  // Manipulation functions
  void sup() {std::advance(src,-split);}
  void sdown() {std::advance(src,split);}
  void up() {std::advance(mod,-split);}
  void down() {std::advance(mod,split);}
  void reset() {mod = data; src = data;}
  void mreset() {mod = data;}
  void sreset() {src = data;}
  void smod() {src = mod;}
  void msrc() {mod = src;}

  // Empty constructor
  inversion_tagalong(): split(0) {}
  // Basic constructor
  inversion_tagalong(pointerType D, sdimi S): data(D), split(S), mod(D), src(D) {}
};

template<class weightType,class arrayType = elementaryArray<weightType>>
struct ELOP_tagalong
{
  dimi mod;
  dimi src;
  void sup() {src--;}
  void sdown() {src++;}
  void up() {mod--;}
  void down() {mod++;}
  void reset() {mod = 1; src = 1;}
  void mreset() {mod = 1;}
  void sreset() {src = 1;}
  void smod() {src = mod;}
  void msrc() {mod = src;}
  arrayType operations;

  ELOP_tagalong(): mod(0),src(0),operations() {}
  ELOP_tagalong(dimi mod_in, dimi src_in = 0): mod(mod_in), src(src_in), operations() {}
  ELOP_tagalong(arrayType op_in): mod(1), src(1), operations(op_in) {}
  ELOP_tagalong(const arrayType& op_in): mod(1), src(1), operations(op_in) {}
  
  void swap() {operations.premultiply(mod,src,0);}
  template<class scalarType>
  void add(const scalarType& S) {operations.premultiply(mod,src,S);}
  template<class scalarType>
  void mult(const scalarType& S) {operations.premultiply(mod,std::numeric_limits<dimi>::max(),S);}
};

struct tagalongELROPS
{
    // Temporary null tagalongs
  template<class tagalongType, class iter1, class iter2>
  inline static void swap(const tagalongType& T, const iter1& A, const iter2& B, dimi cols)
  {
    elementwiseSwap::swap_beams(A,B,cols);
  }
  template<class tagalongType, class iter1, class iter2, class scalarType>
  inline static void multiply(const tagalongType& T, const iter1& A, const iter2& B, const scalarType& S)
  {
    elementwiseBeamMultiplication::beamMultiplication(A,B,S);
  }
  template<class tagalongType, class iter1, class iter2, class iter3, class scalarType>
  inline static void add(const tagalongType& T, const iter1& A, const iter2& B, const iter3& C, const scalarType& S)
  {
    elementwiseFMA::FMA(A,B,C,S);
  }
  // Temporary, basic determinant tagalong
  template<class elementType, class iter1, class iter2>
  inline static void swap(det_tagalong<elementType>& T, const iter1& A, const iter2& B, dimi cols)
  {
    elementwiseSwap::swap_beams(A,B,cols);
    T.det_value*=(-1);
  }
  template<class elementType, class iter1, class iter2, class scalarType>
  inline static void multiply(det_tagalong<elementType>& T, const iter1& A, const iter2& B, const scalarType& S)
  {
    elementwiseBeamMultiplication::beamMultiplication(A,B,S);
    T.det_value*=S;
  }
  
  // Inversion tagalong
  
  template<class IT, class IT2, class iter1, class iter2>
  inline static void swap(inversion_tagalong<IT,IT2>& T, const iter1& A, const iter2& B, dimi cols)
  {
    elementwiseSwap::swap_beams(A,B,cols);
    T.swap();
  }
  template<class IT, class IT2, class iter1, class iter2, class scalarType>
  inline static void multiply(inversion_tagalong<IT,IT2>& T, const iter1& A, const iter2& B, const scalarType& S)
  {
    elementwiseBeamMultiplication::beamMultiplication(A,B,S);
    T.mult(S);
  }
  template<class IT, class IT2, class iter1, class iter2, class iter3, class scalarType>
  inline static void add(inversion_tagalong<IT,IT2>& T, const iter1& A, const iter2& B, const iter3& C, const scalarType& S)
  {
    elementwiseFMA::FMA(A,B,C,S);
    T.add(S);
  }

  // ELOP tagalong
  template<class IT, class IT2, class iter1, class iter2>
  inline static void swap(ELOP_tagalong<IT,IT2>& T, const iter1& A, const iter2& B, dimi cols)
  {
    elementwiseSwap::swap_beams(A,B,cols);
    T.swap();
  }
  template<class IT, class IT2, class iter1, class iter2, class scalarType>
  inline static void multiply(ELOP_tagalong<IT,IT2>& T, const iter1& A, const iter2& B, const scalarType& S)
  {
    elementwiseBeamMultiplication::beamMultiplication(A,B,S);
    T.mult(S);
  }
  template<class IT, class IT2, class iter1, class iter2, class iter3, class scalarType>
  inline static void add(ELOP_tagalong<IT,IT2>& T, const iter1& A, const iter2& B, const iter3& C, const scalarType& S)
  {
    elementwiseFMA::FMA(A,B,C,S);
    T.add(S);
  }

  
   //Null tagalong row operations
  template<class IT>
  inline static void srowUp(const IT & T) {}
  template<class IT>
  inline static void srowDown(const IT & T) {}
  template<class IT>
  inline static void srowReset(const IT & T) {}
  template<class IT>
  inline static void srowMod(const IT & T) {}
  template<class IT>
  inline static void rowUp(const IT & T) {}
  template<class IT>
  inline static void rowDown(const IT & T) {}
  template<class IT>
  inline static void rowReset(const IT & T) {}
  template<class IT>
  inline static void rowSrc(const IT & T) {}
  
  //Inversion tagalong row operations
  template<class IT, class ET>
  inline static void srowUp(inversion_tagalong<IT,ET> & T) {T.sup();}
  template<class IT, class ET>
  inline static void srowDown(inversion_tagalong<IT,ET> & T) {T.sdown();}
  template<class IT, class ET>
  inline static void srowReset(inversion_tagalong<IT,ET> & T) {T.sreset();}
  template<class IT, class ET>
  inline static void srowMod(inversion_tagalong<IT,ET> & T) {T.smod();}
  template<class IT, class ET>
  inline static void rowUp(inversion_tagalong<IT,ET> & T) {T.up();}
  template<class IT, class ET>
  inline static void rowDown(inversion_tagalong<IT,ET> & T) {T.down();}
  template<class IT, class ET>
  inline static void rowReset(inversion_tagalong<IT,ET> & T) {T.mreset();}
  template<class IT, class ET>
  inline static void rowSrc(inversion_tagalong<IT,ET> & T) {T.msrc();}

  // ELOP tagalong row operations
  template<class IT, class ET>
  inline static void srowUp(ELOP_tagalong<IT,ET> & T) {T.sup();}
  template<class IT, class ET>
  inline static void srowDown(ELOP_tagalong<IT,ET> & T) {T.sdown();}
  template<class IT, class ET>
  inline static void srowReset(ELOP_tagalong<IT,ET> & T) {T.sreset();}
  template<class IT, class ET>
  inline static void srowMod(ELOP_tagalong<IT,ET> & T) {T.smod();}
  template<class IT, class ET>
  inline static void rowUp(ELOP_tagalong<IT,ET> & T) {T.up();}
  template<class IT, class ET>
  inline static void rowDown(ELOP_tagalong<IT,ET> & T) {T.down();}
  template<class IT, class ET>
  inline static void rowReset(ELOP_tagalong<IT,ET> & T) {T.mreset();}
  template<class IT, class ET>
  inline static void rowSrc(ELOP_tagalong<IT,ET> & T) {T.msrc();}
};


#endif
