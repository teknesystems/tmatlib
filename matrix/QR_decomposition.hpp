// Code for QR decomposition of upper Hessenberg matrices and submatrices, as well as quick steps of the QR algorithm, including parallelized steps on the CPU and (hopefully) the GPU

// Header guard. Skips over entire header if already included.
#ifndef _TMATLIB_QR_DECOMP_INCLUDED_
#define _TMATLIB_QR_DECOMP_INCLUDED_

#include "rotation.hpp" // For the Givens rotation method of computing QR decompositions

template<class CT>
struct Q_rot_tagalong
{
  CT& data;
};

struct QR_decomposition
{
  // Basic decomposition
  template<class IT, class WT>
  inline static void decompose (WT Q /*Array of elementary rotations stored as c,s pairs)*/ , IT R /*Iterator to contiguous nxn Hessenber matrix*/, dimi n /*matrix size*/, dimi s_n /*submatrix size*/)
  {
    matrixRotation::zero_rotate_right_diag(Q,R,n,s_n);
  }
  template<class IT, class WT>
  inline static void decompose (WT Q, IT R, dimi n) {decompose(Q,R,n,n);}

  // Decomposition to two matrices (transpose application). Requires 2*n*sizeof(T) or less memory
  template<class IT1, class IT2, class WT>
  inline static void decompose (IT1 Q, IT2 R, dimi n, dimi s_n, WT memory)
  {
    decompose(memory,R,n,s_n);
    matrixRotation::rotate_read_transpose<true>(memory,Q,n,s_n);
  }

  // Rapid step of the QR algorithm (single thread). Requires 2*n*sizeof(T) or less memory
  template<class IT, class WT>
  inline static IT step (IT A, WT memory, dimi n, dimi s_n)
  {
    matrixRotation::zero_rotate_right_diag(memory,A,n,s_n);
    matrixRotation::rotate_read_transpose<true>(memory,A,n,s_n);
    return A;
  }
};

#endif
