// Multiplying contiguous matrices (a contiguous size n*m block of memory, with contiguous defined as pointers/iterators going down rows/columns, until switching to the next, till the end of the matrix)
// by the definition.

// Should include sequential (single-thread) and parallel algorithms

#ifndef _T_MATRIX_MULT_CONTIG_INCLUDED_
#define _T_MATRIX_MULT_CONTIG_INCLUDED_

template<class T> struct binary_assignment; // Forward declaration

namespace matrix
{
  struct simple_multiplication
  {
    template<bool transpose = false, bool result_transpose = false, // Whether to treat inputs as having opposite or similar order and whether to write the result in an opposite or similar order
	     class functor = std::plus<>, // Underlying accumulation operation. std::plus for matrix multiplication
	     class assignment_functor = binary_assignment<functor>, // Initial accumulation operation (generally either += or =)
	     class accumulation_functor = binary_assignment<functor>, // Actual accumulation operation (generally +=)
	     class combination_functor = std::multiplies<>, // Underlying matrix multiplier. In case you need some strange field operation, to treat datatypes differently, etc...
	     class OIT, class IT1, class IT2>
    inline static void multiply(OIT C, IT1 A, IT2 B, /* Pointers/random access iterators/random access datastructures to output and two inputs */
				dimi m, dimi n, dimi p, /* Input sizes (m*n and n*p for non-transpose access, m*n and p*n for transpose access). Output size is m*p */
				assignment_functor F = assignment_functor(), accumulation_functor ACC = accumulation_functor(), /* Initial assignment and accumulator functors */
				combination_functor f = combination_functor() /* Functor for combining elements of A and B. Generally simple multiplication as defined for the types*/
				)
    {
      for(dimi iter = 0; iter < m; iter++)
	{
	  for(dimi miniiter = 0; miniiter < p; miniiter++)
	    {
	      _TMATLIB_CONST_IF_ (!transpose)
	      {
		auto acc = f(A[iter*n],B[miniiter]);
		for(dimi microiter = 1; microiter < n; microiter++)
		  {
		    ACC(acc,f(A[iter*n + microiter],B[miniiter + microiter*p]));
		  }
		_TMATLIB_CONST_IF_ (result_transpose)
		{
		  F(C[iter + miniiter*m],acc);
		}
		else
		  {
		    F(C[iter*p + miniiter],acc);
		  }
	      }
	      else
		{
		  auto acc = A[iter*n] * B[miniiter*n];
		  for(dimi microiter = 1; microiter < n; microiter++)
		    {
		      ACC(acc,f(A[iter*n + microiter],B[miniiter*n + microiter]));
		    }
		  _TMATLIB_CONST_IF_ (result_transpose)
		  {
		    F(C[iter + miniiter*m],acc);
		  }
		  else
		    {
		      F(C[iter*p + miniiter],acc);
		    }
		}
	    }
	}
    }
    template<class functor = std::plus<>,class assignment_functor = binary_assignment<functor>,class accumulation_functor = binary_assignment<functor>,class combination_functor = std::multiplies<>,
	     class OIT, class IT1, class IT2>
    inline static void multiply(OIT C, IT1 A, IT2 B, /* Pointers/random access iterators/random access datastructures to output and two inputs */
				dimi m, dimi n, dimi p, /* Input sizes (m*n and n*p for non-transpose access, m*n and p*n for transpose access). Output size is m*p */
				bool dir_A, /* Direction of input A */
				bool dir_B, /* Direction of input B */
				bool dir_C, /* Output direction */
				assignment_functor F = assignment_functor(), accumulation_functor ACC = accumulation_functor(), /* Initial assignment and accumulator functors */
				combination_functor f = combination_functor() /* Functor for combining elements of A and B. Generally simple multiplication as defined for the types*/
				)
    {
      /*multiply<(dir_A != dir_B),(dir_A != dir_C)>(C,A,B,m,n,p,F,ACC,f)*/
      if(dir_A == dir_B)
	{
	  if(dir_C == dir_A)
	    {
	      multiply<false,false>(C,A,B,m,n,p,F,ACC,f);
	    }
	  else
	    {
	      multiply<false,true>(C,A,B,m,n,p,F,ACC,f);
	    }
	}
      else
	{
	  if(dir_C == dir_A)
	    {
	      multiply<true,false>(C,A,B,m,n,p,F,ACC,f);
	    }
	  else
	    {
	      multiply<true,true>(C,A,B,m,n,p,F,ACC,f);
	    }
	}
    }
  };
  
  struct simple_iterator_multiplication
  {
    // On two compatibly sized blocks of memory (n*m and m*p), with the same orientation, outputting to a third
    template<bool transpose = false, bool result_transpose = false,
	     class functor = std::plus<>, // Underlying accumulation operation. std::plus for matrix multiplication
	     class assignment_functor = binary_assignment<functor>, // Initial accumulation operation (generally either += or =)
	     class accumulation_functor = binary_assignment<functor>, // Actual accumulation operation (generally +=)
	     class combination_functor = std::multiplies<>, // Underlying matrix multiplier. In case you need some strange field operation, to treat datatypes differently, etc...
	     class OIT, class IT1, class IT2>
    inline static void multiply(OIT C, IT1 A, IT2 B, dimi m, dimi n, dimi p,
				assignment_functor F = assignment_functor(), accumulation_functor ACC = accumulation_functor(), combination_functor f = combination_functor())
    {
      auto TC = C;
      _TMATLIB_CONST_IF_(!transpose)
	{
	  auto TA = A;
	  auto TB = B;
	  for(dimi iter = 0; iter < m; iter++)
	    {
	      for(dimi miniiter = 0; miniiter < p; miniiter++)
		{
		  TA = A;
		  TB = std::next(B,miniiter);
		  F(*C,f(*TA,*TB));
		  for(dimi microiter = 1; microiter < n; microiter++)
		    {
		      std::advance(TB,p);
		      std::advance(TA,1);
		      ACC(*C,f(*TA,*TB));
		    }
		  std::advance(C,(result_transpose)?m:1);
		}
	      _TMATLIB_CONST_IF_(result_transpose)
	      {
		std::advance(TC,1);
		C = TC;
	      }
	      std::advance(A,n);
	    }
	}
      else
	{
	  auto TA = std::next(A,1);
	  auto TB = std::next(B,1);
	  for(dimi i = 0; i < m; i++)
	    {
	      for(dimi j = 0; j < p; j++)
		{
		  F(*C,f(*A,*B));
		  for(dimi k = 1; k < n; k++)
		    {
		      ACC(*C,f(*TA,*TB));
		      std::advance(TA,1);
		      std::advance(TB,1);
		    }
		  std::advance(C,(result_transpose)?m:1);
		  TA = std::next(A,1);
		}
	      _TMATLIB_CONST_IF_(result_transpose)
	      {
		std::advance(TC,1);
		C = TC;
	      }
	      TB = B;
	      std::advance(A,n);
	    }
	}
    }
    template<class functor = std::plus<>,class assignment_functor = binary_assignment<functor>,class accumulation_functor = binary_assignment<functor>,class combination_functor = std::multiplies<>,
	     class OIT, class IT1, class IT2>
    inline static void multiply(OIT C, IT1 A, IT2 B, /* Pointers/random access iterators/random access datastructures to output and two inputs */
				dimi m, dimi n, dimi p, /* Input sizes (m*n and n*p for non-transpose access, m*n and p*n for transpose access). Output size is m*p */
				bool dir_A, /* Direction of input A */
				bool dir_B, /* Direction of input B */
				bool dir_C, /* Output direction */
				assignment_functor F = assignment_functor(), accumulation_functor ACC = accumulation_functor(), /* Initial assignment and accumulator functors */
				combination_functor f = combination_functor() /* Functor for combining elements of A and B. Generally simple multiplication as defined for the types*/
				)
    {
      /*multiply<(dir_A != dir_B),(dir_A != dir_C)>(C,A,B,m,n,p,F,ACC,f)*/
      if(dir_A == dir_B)
	{
	  if(dir_C == dir_A)
	    {
	      multiply<false,false>(C,A,B,m,n,p,F,ACC,f);
	    }
	  else
	    {
	      multiply<false,true>(C,A,B,m,n,p,F,ACC,f);
	    }
	}
      else
	{
	  if(dir_C == dir_A)
	    {
	      multiply<true,false>(C,A,B,m,n,p,F,ACC,f);
	    }
	  else
	    {
	      multiply<true,true>(C,A,B,m,n,p,F,ACC,f);
	    }
	}
    }
  };
  
}

#endif
