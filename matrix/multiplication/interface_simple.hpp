// Definition based matrix multiplication using a simple interface supporting "getElement(r,c)" and "getConstElement(r,c). Useful for accessing non-matrix data as a matrix through a handler/interface.

// Header guard. Skips over entire header if already included.
#ifndef _T_MATRIX_MULT_INTERF_INCLUDED_
#define _T_MATRIX_MULT_INTERF_INCLUDED_

namespace matrix
{
  struct simple_interface_multiplication
  {
    template<class IT1, class IT2, class IT3>
    inline static void multiply(IT1& C, const IT2& A, const IT3& B, dimi m, dimi n, dimi p)
    {
      for(dimi i = 0; i < m; i++)
	{
	  for(dimi j = 0; j < p; j++)
	    {
	      C.getElement(i,j) = A.getConstElement(i,0) * B.getConstElement(0,j);
	      for(dimi k = 1; k < n; k++)
		{
		  C.getElement(i,j) += A.getConstElement(i,k) * B.getConstElement(k,j);
		}
	    }
	}
    }
  };
}

#endif
