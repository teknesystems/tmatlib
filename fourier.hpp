// Fourier transform and auxiliary classes for 1, 2 and multidimensional discrete space

// Header guard. Skips over entire header if included.
#ifndef _T_DFT_INCLUDED_
#define _T_DFT_INCLUDED_

#include <cmath>
#include <complex> // For the necessary complex exponential and data handling functions
#include "complex_utils.hpp"
#include "utility.hpp"

//#define _T_DFT_SCAFFOLDING_ENABLED_

struct FFT
{
  // TEMPORARY!
  template<class dataType>
  struct FFT_tuning_values
  {
    static constexpr unsigned short exit_at = 32; // Value when DFT is more efficient than FFT due to memory. This should be tuned. 0 means never switch, e.g. use FFT the entire way.
    static constexpr double trig_epsilon = 1e-16;
    static constexpr double transform_trig_epsilon = 3 * trig_epsilon;
  };
  template<class floatType, class inputType>
  inline static auto getTwiddleFactor(inputType multiplier) -> decltype(complexExponential::exp_complex_part(trigonometricConstants<decayComplex<floatType>>::tau::get_multiple(multiplier)))
  {
    return complexExponential::exp_complex_part(trigonometricConstants<floatType>::tau::get_multiple(multiplier));
  }
  template<class floatType, class inputType>
  inline static auto getTwiddleFactor(float multiplier, inputType divider) -> decltype(getTwiddleFactor<floatType>(multiplier/divider))
  {
    return getTwiddleFactor<floatType>(multiplier/divider);
  }
};

struct simpleDFT
{
  template<int expmult = -1, class iter1, class iter2, class intType2>
  inline static void simple_scan(iter1 x, float harmonic, intType2 N, iter2& O)
  {
    (*O) = *x;
    x++;
    for(dimi n = 1; n < N; n++)
      {
	(*O)+= FFT::getTwiddleFactor<decltype((*x).real())>(expmult*(harmonic*n)/N) * (*x); // Summation on n = 0 to N - 1 of x[n]*e^(-i2pikn/N)
	x++;
      }
  }
  template<int expmult = -1, class iter1, class iter2, class intType>
  inline static void DFT(iter1 x, iter2 X, intType N) // Input iterator, output iterator, size
  {
    for(dimi k = 0; k < (N); k++)
      {
	simple_scan<expmult>(x,k,N,X);
	X++;
      }
  }
};

struct radix2FFT
{
  // Bit reversal permutation
  template<class iter1>
  inline static void bitreverse(iter1 x, dimi N)
  {
  }
  template<class iter1, class iter2>
  inline static void bitreverse(iter1 x, iter2 X, dimi N)
  {
  }

  // WIP
  /*
  // In-place FFT
  template<class container1>
  inline static void inplaceBRFFT(container1& x, dimi N, dimi p)
  {
    dimi m;
    auto omega_m = (x[0]);
    auto omega = (x[0]);
    for(dimi s = 1; s < p; s++)
      {
	m = 1 << s; //2^s;
	omega_m = FFT::getTwiddleFactor<decltype((x[0]).real())>(-1.0,m);
	std::cout<<"omega_m @ "<<s<<","<<m<<" = "<<omega_m<<"\n";
	for(dimi k = 0; k < (N-1); k++)
	  {
	    omega = 1.0;
	    for(dimi j = 0; j < (m/2); j++)
	      {
		auto t = omega * x[k+j+(m/2)];
		std::cout<<"t @ "<<s<<","<<k<<","<<j<<" = "<<t<<"\n";
		auto u = x[k+j];
		x[k+j] = u + t;
		x[k+j+(m/2)] = u - t;
		omega *= omega_m;
	      }
	  }
      }
  }
  */
  
  // Simple FFT
  
  template<int expmult, class iter1, class iter2, class iter3>
  inline static void mergeFFT(iter1 X, iter2 x, iter3 y, dimi N)
  {
    auto filleven = X;
    auto fillodd = filleven;
    std::advance(fillodd, (N/2));
    for(dimi iter = 0; iter < (N/2); iter++)
      {
	(*filleven) = (*x);
	(*fillodd) = (*y);
	 x++;
	 y++;
	 filleven++;
	 fillodd++;
      }
    for(dimi k = 0; k < (N/2); k++)
      {
	auto t = X[k];
	auto g = FFT::getTwiddleFactor<decltype(t.real())>(static_cast<float>(k)*expmult,N);
	X[k] = t + (g*X[k+(N/2)]);
	X[k+(N/2)] = t - (g*X[k+(N/2)]);
      }
  }
  template<class containerType, int expmult = -1, class iterType>
  inline static containerType simpleFFT(iterType A, dimi N, dimi s)
  {
    containerType R;
    R.resize(N);
    if(N < 8)
      {
	jump_pointer<iterType> J (A,s);
	simpleDFT::DFT<expmult>(J,R.begin(),N);
      }
    else
      {
	auto AE = A;
	auto AO = AE;
	std::advance(AO,s);
	auto RE = simpleFFT<containerType,expmult>(AE,(N/2),2*s);
	auto RO = simpleFFT<containerType,expmult>(AO,(N/2),2*s);
	mergeFFT<expmult>(R.begin(),RE.begin(),RO.begin(),N);
      }
    return R;
  }
  template<int expmult = -1, class containerType>
  inline static containerType simpleFFT(containerType& A, dimi s = 1)
  {
    return simpleFFT<containerType,expmult>(A.begin(),A.size(),s);
  }
  
  //WIP
  /*
  template<int expmult = -1, class IT, class WIT>
  inline static bool shuffleFFT(IT A, dimi N, WIT W)
  {
    dimi j = 1;
    const dimi minimum = 8; //TODO: tune and template

    // Calculating how many jumps will take place
    dimi m = N;
    while((m > minimum)&&(!(m%2)))
      {
	m/=2;
	j++;
      }
    dimi M = N/m; // Number of strided subarrays
    //bool towork = j%2; // If j is odd (true), we end up writing to work (W). Else, we write back to array (A)
    
    // Writing subarray Fourier transforms into work array (linear time)
    jump_pointer<IT> J (A,j);
    auto TW = W;
    for(dimi k = 0; k < M; k++)
      {
	simpleDFT::DFT<expmult>(J,TW,m);
	J>>1;
	std::advance(W,m);
      }

    bool towork = false;
    // Merging subarray Fourier transforms into larger and larger transforms (linearithmic time)
    for(dimi k = 0; k < j; k++)
      {
	// Iterating over all subarrays of length m, M = N/m times, to get N (linear time)
	if(towork)
	  {
	    for(dimi k = 0; k < M/2; k++)
	      {
		mergeFFT<expmult>(std::next(W,k*m*2),std::next(A,k*m),std::next(A,(N/2) + k*m),m);
	      }
	    towork = false;
	  }
	else
	  {
	    for(dimi k = 0; k < M/2; k++)
	      {
		mergeFFT<expmult>(std::next(A,k*m*2),std::next(W,k*m),std::next(W,(N/2) + k*m),m);
	      }
	    towork = true;
	  }
	M/=2; // The number of subarrays halves each merge
	m*=2; // Length doubles, so time remains N (linear). Since we do this log(N) times, we get Nlog(N) runtime
      }
    return towork;
  }
  */
  // FFT wrapper
  template<class containerType>
  inline static containerType FFT(containerType& x, dimi N)
  {
  }

  template<class iter1, class iter2>
    inline static void FFT(iter1 x, iter2 X, dimi N, dimi p = 0)
  {
    if(p)
      {
	bitreverse(x,X,N);
	inplaceBRFFT(X,N,p);
      }
    else
      {
	//TODO: Implement
      }
  }
};

#endif
