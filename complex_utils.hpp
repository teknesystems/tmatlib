// Utilities for manipulating complex numbers and exponentials

// Header guard. Skips over entire header if included.
#ifndef _T_CUTIL_INCLUDED_
#define _T_CUTIL_INCLUDED_

#include <cmath>
#include <complex>
#include "symbolic_constants.hpp"

struct complexExponential
{
  template<class floatType>
  inline static auto cleanResult(std::complex<floatType>& x)
  {
    if(std::abs(x.real()) < 1e-15)
      {
	x.real(0);
      }
    if(std::abs(x.imag()) < 1e-15)
      {
	x.imag(0);
      }
  }
  template<class iterType>
  inline static auto cleanResult(const iterType& X, dimi N)
  {
    for(int k = 0; k < N; k++)
      {
        cleanResult(X[k]);
      }
  }
  template<class floatType>
  inline static constexpr std::complex<floatType> exp_complex_part(const floatType& X)
  {
    std::complex<floatType> R (std::cos(X),std::sin(X));
    cleanResult(R);
    //std::cout<<"Returning "<<R<<"\n";
    return R;
  }
  template<class floatType>
  inline static constexpr std::complex<floatType> exp_complex_part(const std::complex<floatType>& X)
  {
    std::complex<floatType> R (std::cos(X.imag()),std::sin(X.imag()));
    cleanResult(R);
    return R;
  }
};

template<class T>
T complexConjugate(T input)
{
  return input;
}

template<class FT>
std::complex<FT> complexConjugate(std::complex<FT> input)
{
  return conj(input);
}

template<class T>
using decayComplex = typename underlyingFloat<T>::FT;

#endif
