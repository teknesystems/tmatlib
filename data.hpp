// Handlers for 1D and 2D data structures with respect to mathematical objects
// e.g. the construction of different sparse matrices from 1D/2D arrays, treating a 1D array as a matrix/tensor, etc.

// Header guard. Skips over entire header if already included
#ifndef _T_DATA_INCLUDED_
#define _T_DATA_INCLUDED_

#include <cstdint>

enum truth_val : std::uint_least8_t {no = 0, yes = 1, unspecified = 2};

// Denotes a type which is a data handler, contains basic, overloaded (for individual handler) functions returning handler information
struct dataHandler
{
  // Characterstics
  static constexpr truth_val is_sparse = unspecified;
  static constexpr truth_val is_contiguous = unspecified;

  // Optimizations
  static constexpr truth_val fast_random = unspecified;
  static constexpr truth_val fast_resize = unspecified;
  static constexpr truth_val fast_insert = unspecified;
  static constexpr truth_val edit_optimized = unspecified;
  static constexpr truth_val read_optimized = unspecified;
  static constexpr truth_val calc_optimized = unspecified;

  // Restrictions
  static constexpr truth_val constant_size = no;
};

template<bool default_dir = true, bool fixed = false>
struct contiguous: public dataHandler
{
  static constexpr truth_val is_contiguous = yes;
};

template<bool default_dir = true, bool fixed = false>
struct sparse_beam: public dataHandler
{
  static constexpr truth_val is_sparse = yes;
  static constexpr truth_val is_contiguous = no;
};

struct diagonal: public dataHandler
{
  static constexpr truth_val is_sparse = yes;
};

#endif
