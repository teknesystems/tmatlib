// 1D array operations and wrappers/bindings for STL operations to tmatlib vectors and other classes.

// Header guard. Skips over entire header if already included
#ifndef _T_VEC_OPS_INCLUDED_
#define _T_VEC_OPS_INCLUDED_ 

#include <cmath>
#include "operations.hpp"

// Basis for silent vector error handling:
extern basis VECTOR_ERROR;

// Kernel base classes

// Base-class indicating kernel to perform operations on contiguous-dense (e.g. incrementing the *iterator* recieved leads to the next element, not necessarily contiguous in *memory*) data
struct denseKernel
{
};

// Base-class indicating kernel to perform operations ONLY on MEMORY-CONTIGUOUS (e.g. C/Fortran/etc. arrays) data. Mainly for passing pointers (not iterators) to external functions/C-style functions
struct contiguousKernel: public denseKernel
{
};

// Simple elementwise addition:

// Kernel to perform vector addition on input data
struct vectorAdditionKernel
{
};

struct null_op
{
  inline static bool op(...) {return true;}
  inline static bool op_to(...) {return true;}
  inline static bool op_range_to(...) {return true;}
  inline static bool op_range(...) {return true;}
};


template<class vector_addition, class scalar_multiplication, class subtraction = null_op, class negation = null_op, class FMA = null_op>
struct vector_space_over
{
  vector_addition add = vector_addition();
  scalar_multiplication multiply = scalar_multiplication();
  subtraction subtract = subtraction();
  negation negate = negation();
  FMA axpy = FMA(); // Computational aid, can be autodefined (like +=)
};

struct valarray_passthrough_addition
{
    template<bool not_permitted = true, class elementOne>
  static inline void add_to(std::valarray<elementOne>& A, basis*& basis1, const std::valarray<elementOne>& B, const basis* basis2)
  {
    if(basis1 == basis2)
      {
        A+=B;
      }
    else
      {
	//TODO: Rectify basis
        A+=B;
      }
  }
  template<bool not_permitted = true, class elementOne>
  static inline std::valarray<elementOne> add(const std::valarray<elementOne>& A, basis*& basis1, const std::valarray<elementOne>& B, const basis* basis2)
  {
    if(basis1 == basis2)
      {
	std::valarray<elementOne> X = A + B;
	return X;
      }
    else
      {
	//TODO: Rectify basis
	std::valarray<elementOne> X = A + B; //Temporary!
	return X;
      }
  }
};

struct elementwiseAddition: public elementwise_binary<std::plus<>>
{
  /*
#define _TMATLIB_KERNEL_NAME_ simple_binary_container_operation<true,iterator_elementwise_binary_kernel<std::plus<>>>
#define _TMATLIB_OPERATION_NAME_ add
#define _TMATLIB_ASSIGNMENT_NAME_ add_to
#define _TMATLIB_RANGE_ASSIGNMENT_NAME_ add_range_to
#define _TMATLIB_RANGE_NAME_ add_range
#define _TMATLIB_ADDITIONAL_DATA_ basis* B1, basis* B2, basis* B3
#define _TMATLIB_ADDITIONAL_PASS_ B1,B2,B3
#define _TMATLIB_ADDITIONAL_ASSGN_DATA_ basis*& B1, basis* B2
#define _TMATLIB_ADDITIONAL_ASSGN_PASS_ B1,B2
#define _TMATLIB_DONT_UNDEFINE_
#include "operations/wrappers/binary_container_operation.hpp"
#undef _TMATLIB_DONT_UNDEFINE_
#include "operations/wrappers/binary_operation_kernel_wrapper.hpp"
  */
};

struct scalarMultiplicationKernel
{
};

struct valarray_passthrough_multiplication
{
  template<class elementType, class scalar>
  static inline void multiply(std::valarray<elementType>& A, const scalar& B)
  {
    A*=B;
  }
};

// TEMPORARY:
struct elementwiseScalarMultiplication: public elementwise_scalar<std::multiplies<>>
{
};

// Elementwise subtraction:
struct elementwiseSubtraction: public elementwise_binary<std::minus<>>
{
};

// Dot product:
struct elementwiseDotProduct
{
  template<class containerType1, class containerType2>
  inline static auto dot(const containerType1& A, const containerType2& B) -> decltype((*(A.begin()))*(*(B.begin())))
  {
    if(A.size() == B.size())
      {
	decltype(*(A.begin())+*(B.begin())) result = 0;
	for(dimi iter = 0; iter < A.size(); iter++)
	  {
	    result+= B[iter] * A[iter];
	  }
      }
    else
      {
	return 0; //TEMPORARY!
      }
  }
  template<class containerType1, class containerType2>
  inline static auto dot(const containerType1& A, const basis* A_basis, const containerType2& B, const basis* B_basis) -> decltype(*(A.begin())+*(B.begin()))
  {
    //TODO: Rectify basis!
    return dot(A,B); //TEMPORARY!
  }
  template<class IT1, class IT2>
  inline static auto dot(IT1 x, IT2 y, dimi N) -> decltype((*x) + (*y))
  {
    IT1 z = std::next(x,N);
    decltype((*x)+(*y)) acc = 0;
    while(x < z)
      {
	acc+=((*x)+(*y));
	std::advance(x,1);
	std::advance(y,1);
      }
    return acc;
  }
  template<class containerType1, class containerType2>
  inline static containerType1 multiply (const containerType1& A, const containerType2& B)
  {
    containerType1 result;
    reserve_container(result,std::min(A.size(),B.size()));
    for(dimi iter = 0; iter < std::min(A.size(),B.size()); iter++)
      {
	result[iter] = A[iter] * B[iter];
      }
    return result;
  }
  template<class containerType1, class containerType2>
  inline static void multiply_right (containerType1& A, const containerType2& B)
  {
    for(dimi iter = 0; iter < std::min(A.size(),B.size()); iter++)
      {
	A[iter] = A[iter] * B[iter];
      }
  }
  template<class containerType1, class containerType2>
  inline static void multiply_left (const containerType1& A, containerType2& B)
  {
    for(dimi iter = 0; iter < std::min(A.size(),B.size()); iter++)
      {
	B[iter] = A[iter] * B[iter];
      }
  }
};

// Fused multiply add (FMA)
struct elementwiseFMA
{
  // FMA on iterators
  template<class iter1, class iter2, class scalarType>
  inline static void FMA (iter1 A, iter1 AE, iter2 B, const scalarType& S)
  {
    while(A < AE)
      {
	*A = gFMA((*B),S,(*A));
	//std::cout<<"\nFMA operation parameters: RA = "<<*A<<", B = "<<*B<<", S = "<<S<<"\n"; // Scaffolding
	A++;
	B++;
      }
  }
  template<class iter1, class iter2, class iter3>
  inline static void vFMA (iter1 A, iter1 AE, iter2 B, iter3 C)
  {
    while(A < AE)
      {
	*A = gFMA((*B),(*C),(*A));
	A++;
	B++;
	C++;
      }
  }
  template<class iter1, class iter2, class iter3, class scalarType>
  inline static void FMA (iter1 R, iter1 RE, iter2 A, iter3 B, const scalarType& S)
  {
    while(R < RE)
      {
	*R = gFMA((*B),S,(*A));
	A++;
	B++;
	R++;
      }
  }
  template<class iter1, class iter2, class iter3, class iter4>
  inline static void vFMA (iter1 R, iter1 RE, iter2 A, iter3 B, iter4 C)
  {
    while(R < RE)
      {
	*R = gFMA((*B),(*C),(*A));
	A++;
	B++;
	C++;
	R++;
      }
  }
};

struct elementwiseEquality
{
  template<dimi error_factor = 1, class containerOne, class containerTwo>
  static inline bool equal_to(const containerOne& A, const containerTwo& B)
  {
    auto AB = container_beginning(A);
    auto AE = container_end(A);
    auto BB = container_beginning(B);
    return array_almost_equal<error_factor>(AB,AE,BB);
  }
  template<dimi error_factor = 1, class iter1, class iter2>
  static inline bool equal_to(const iter1& A, const iter1& AE, const iter2& B)
  {
    return array_almost_equal<error_factor>(A,AE,B);
  }
  template<dimi error_factor = 1, class containerOne, class containerTwo>
  static inline bool equal_to(const containerOne& A, basis* basis1, const containerTwo& B, basis* basis2)
  {
    auto AB = container_beginning(A);
    auto AE = container_end(A);
    auto BB = container_beginning(B);
    if(basis1 == basis2)
      {
	return array_almost_equal<error_factor>(AB,AE,BB);
      }
    else
      {
	//TODO: Rectify basis
	return array_almost_equal<error_factor>(AB,AE,BB);
      }
  }
  template<dimi error_factor = 1, class iter1, class iter2>
  static inline bool equal_to(const iter1& A, const iter1& AE, basis* basis1, const iter2& B, basis* basis2)
  {
    if(basis1 == basis2)
      {
	return array_almost_equal<error_factor>(A,AE,B);
      }
    else
      {
	//TODO: Rectify basis
	return array_almost_equal<error_factor>(A,AE,B);
      }
  }
  template<dimi error_factor = 4, class iter1, class iter2>
  static inline bool converged(iter1 A1, iter2 A2, dimi N)
  {
    return equal_to<error_factor>(A1,std::next(A1,N),A2);
  }
};

struct vectorProperties
{
  template<class iterType1, class iterType2>
  bool areOrthogonal(iterType1 x, iterType2 y, dimi N)
  {
    return (elementwiseDotProduct::dot(x,y,N) == 0);
  }
};

/*
struct standardCrossProduct
{
  // Note: the below functions are UNSAFE.
  // they do NOT check if the vectors are all the same length, etc.!
  template <class containerType1, class containerType2>
  inline static containerType1 crossProductOf(const containerType1& A, const containerType1& B)
  {
    containerType1& result;
    reserve_container(result, 3);
    push_to_container(result,A[1]*B[2] - A[2]*B[1],0);
    push_to_container(result,A[0]*B[2] - A[2]*B[0],1);
    push_to_container(result,A[0]*B[1] - A[1]*B[0],2);
    return result;
  }
  template <class containerType>
  inline static containerType nProductOf(std::initializer_list<containerType> containers)
  {
    containerType result;
    reserve_container(result,containers.size()-1);
    for(dimi iter = 0; iter < containers.size()-1; iter++)
      {
	//auto result_element = submatrixDet(treatAsMatrixRows(containers),iter);
	push_back_to_container(result,result_element,iter); //TEMPORARY
      }
      }
};
*/

#endif
