COMPILER = g++
FLAGS = -std=c++17 -g

tmatlib: test.cpp
	$(COMPILER) test.cpp -Wall $(FLAGS) -o test
