// Utilities for numerical computations, including:
// Vector norms (for arbitrary input containers/iterators), as generalizations of std::hypot

// Header guard. Skips over entire header if included.
#ifndef _T_NUTIL_INCLUDED_
#define _T_NUTIL_INCLUDED_

#include <cmath>
#include "utility.hpp"

template<class T>
auto floatingNorm(T I, dimi N) -> decltype((*I)*std::sqrt((*I)/(*I)))
{
  auto mult = std::abs(*I);
  auto E = std::next(I,N);
  std::advance(I,1);
  if(I < E)
    {
      auto sum = (*I)/mult;
      sum*=sum;
      std::advance(I,1);
      while(I < E)
	{
	  auto temp = (*I)/mult;
	  sum+=(temp*temp);
	  std::advance(I,1);
	}
      sum+=1;
      mult*=std::sqrt(sum);
    }
  return mult;
}

template<class T>
auto simpleNorm(T I, dimi N) -> decltype(std::sqrt((*I) * (*I)))
{
  auto E = std::next(I,N);
  auto sum = (*I) * (*I);
  std::advance(I,1);
  while(I < E)
    {
      sum+=((*I) * (*I));
      std::advance(I,1);
    }
  return std::sqrt(sum);
}

template<class IT>
auto normalize(IT I, dimi n) -> decltype(floatingNorm(I,n))
{
  auto N = floatingNorm(I,n); //TODO: determine appropriate norm type automatically
  auto E = std::next(I,n);
  while(I!=E)
    {
      (*I)/=N;
      std::advance(I,1);
    }
  return N;
}

struct normalizationStep
{
  template<class IT>
  inline static auto process(IT I, dimi n) -> decltype(floatingNorm(I,n))
  {
    return normalize(I,n);
  }
  template<class IT, class IT2>
  inline static auto process(IT I, const IT2& prev, dimi n) -> decltype(floatingNorm(I,n))
  {
    return process(I,n);
  }
};

#endif
