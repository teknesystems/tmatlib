// Important symbolic constants
//TODO: Integrate with symbolics system in symbolics.hpp

// Header guard. Skips over entire header if included
#ifndef _T_SYMB_CONST_INCLUDED_
#define _T_SYMB_CONST_INCLUDED_

#include "symbolics.hpp"

struct symbolicConstant
{
};

struct staticSymbolicConstant
{
};

template<class RT>
struct trigonometricConstants
{
  struct pi
  {
    static constexpr RT value = 3.14159265358979323846264338327950288419716939937510582;
    static constexpr RT get_value() {return value;}
    template<class IT>
    inline static constexpr auto get_multiple(const IT& X) -> decltype(X*value) {return X*value;}
    template<class IT>
    inline static constexpr auto get_fraction(const IT& X) -> decltype(value/X) {return value/X;}
  };
  struct tau
  {
    static constexpr RT value = 2 * pi::value;
    template<class IT>
    inline static constexpr auto get_multiple(const IT& X) -> decltype(X*value) {return X*value;}
    template<class IT>
    inline static constexpr auto get_fraction(const IT& X) -> decltype(value/X) {return value/X;}
  };
};

#endif
