// 1D array operations for sparse data, including, for example:
// Vectors representing polynomials as (element,dimension(power))
// Vectors representing exponentials as (element,dimension(power))
// using both the same type (e.g. a simple array of doubles), compound type (e.g. std::pair or monomial) or dual arrays.

// Header guard. Skips over entire header if already included
#ifndef _T_VEC_SPRS_OPS_INCLUDED_
#define _T_VEC_SPRS_OPS_INCLUDED_

#include "array_utils.hpp"

// Segmented addition: where an array is given, and every other element of the array is a dimensional indicator
// OR: where a dimensional data source outside the type is given

template<class order = std::less<void>>
struct sortedSegmentedAddition
{
  template<class CT, class IT>
  inline static void op_to(CT& A, IT B, IT BEND)
  {
    //reserve_container(A,std::distance(B,BEND)+A.size()); // May cause overuse of memory, but may also increase speed. Consider using...
    sorted_insert_pair_array(A,B,BEND,order());
  }
  template<class CT1, class CT2>
  inline static void op_to(CT1& A, const CT2& B)
  {
    auto BB = container_beginning(B);
    auto BE = container_end(B);
    op(A,BB,BE);
  }
  template<class CT1, class CT2>
  inline static void op_to(CT1& A, const CT2& B, basis* basis_A, basis* basis_B, basis* outputBasis)
  {
    op_to(A,B); //Ignore bases for now...
  }
  template<class CT1, class CT2, class CT3>
  inline static void op(CT1& OUT, const CT2& A, const CT3& B)
  {
    auto PA = container_beginning(A);
    auto PAE = container_end(A);
    auto PB = container_beginning(B);
    auto PBE = container_end(B);

    auto PAEXP = std::next(PA,1);
    auto PBEXP = std::next(PB,1);

    reserve_container(OUT, A.size() + B.size());
    auto OIT = std::back_inserter(OUT);
    
    while((PAEXP<PAE)&&(PBEXP<PBE))
      {
	if(*PAEXP == *PBEXP)
	  {
	    *OIT = *PA + *PB;
	    *OIT = *PAEXP;
	    std::advance(PA,2);
	    std::advance(PAEXP,2);
	    std::advance(PB,2);
	    std::advance(PBEXP,2);
	  }
	else if((order())(*PAEXP,*PBEXP))
	  {
	    *OIT = *PA;
	    *OIT = *PAEXP;
	    std::advance(PA,2);
	    std::advance(PAEXP,2);
	  }
	else
	  {
	    *OIT = *PB;
	    *OIT = *PBEXP;
	    std::advance(PB,2);
	    std::advance(PBEXP,2);
	  }
      }
    while(PBEXP < PBE)
      {
	*OIT = *PB;
	*OIT = *PBEXP;
	std::advance(PB,2);
	std::advance(PBEXP,2);
      }
    while(PAEXP < PAE)
      {
	*OIT = *PA;
	*OIT = *PAEXP;
	std::advance(PA,2);
	std::advance(PAEXP,2);
      }
    std::advance(PA,2);
    std::advance(PB,2);
    if(PA < PAE)
      {
	*OIT = *PA;
      }
    else if(PB < PAE)
      {
	*OIT = *PB;
      }
  }
};

struct unsortedSegmentedAddition
{
  template<class CT, class IT>
  inline static void op_to(CT& A, IT B, IT BEND)
  {
    auto BA = container_beginning(A); // To prevent recalculation except when necessary
    auto PEND = container_end(A);
    auto PA = BA;
    auto PE = std::next(PA,1);
    //reserve_container(A,std::distance(B,BEND)+A.size()); // May cause overuse of memory, but may also increase speed. Consider using...
    auto BE = std::next(B,1);
    bool toggle = false;
    while(BE < BEND)
      {
	while(PE < PEND)
	  {
	    if(*PE == *BE)
	      {
		*PA += *B;
		toggle = true;
		break;
	      }
	    std::advance(PE,2);
	    std::advance(PA,2);
	    {
	      if(toggle)
		{
		  toggle = false;
		}
	      else
		{
		  A.push_back(*B);
		  A.push_back(*BE);
		  PEND = container_end(A); // Iterator invalidated by push_back
		  BA = container_beginning(A);
		}
	      PA = BA;
	      PE = std::next(PA,1);
	      std::advance(B,2);
	      std::advance(BE,2);
	    }
	  }
      }
  }
  template<class CT1, class CT2>
  inline static void op_to(CT1& A, const CT2& B)
  {
    auto BB = container_beginning(B);
    auto BE = container_end(B);
    op_to(A,BB,BE);
  }
};

// Segmented operation: where an operation is performed only on each nth element
struct segmentedOperation
{
};

// Temporary
template<dimi N = 2>
struct segmentedScalarMultiplication: public elementwise_scalar<std::multiplies<>,N>
{
};

// Internal addition: Where dimension is an internal property of the type, e.g. std::pair<dataType,dimensionType> or monomial

struct sortedInternalAddition
{
};

struct unsortedInternalAddition
{
};


#endif
