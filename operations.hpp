// Handlers for operations on data (pointers/iterators) representing elements of a set (e.g. pointer to elements of a vector representing vectors in a vector space)

// Header guard. Skips over entire header if already included
#ifndef _T_OPS_INCLUDED_
#define _T_OPS_INCLUDED_


// Internal dependencies
#include "symbolics.hpp"
#include "utility.hpp"

// Modules
#include "operations/kernels/vector/elementwise_binary.hpp"
#include "operations/kernels/vector/elementwise_unary.hpp"
#include "operations/kernels/vector/elementwise_scalar.hpp"

// Interfaces
#include "interface/BLAS_interface.hpp"

// Assumes associativity and totality (valid inputs lead to valid output, in this context)
// Here, two contextual pieces of data are passed: the array length and basis pointer
// The basis pointer is used to handle the data (not yet implemented), and the array length is passed on to the kernel.
// More complicated operations (e.g. more contextual data, say, matrix multiplication with m, n and p parameters)
// require either more complicated basis handlers or using another operation interface/handler (taking in more complex types with more context)
template<
  bool strict = true, // Require the same length for both input arrays
  class kernel = iterator_elementwise_binary_kernel<std::plus<>>> // The kernel to perform the operation (here simply add each element of each array)
struct simple_binary_container_operation: public kernel
{
  // Importing operations from the kernel for use on iterators
  using kernel::op;
  using kernel::op_to;
  using kernel::op_range_to;
  
  // Handles container input from vector classes and passes iterator input through to kernel
  template<class CT1, class CT2>
  static inline bool op_to(CT1& A, const CT2& B)
  {
    auto PA = container_beginning(A);
    auto PB = container_beginning(B);
    _TMATLIB_CONST_IF_(strict)
      {
	auto n = A.size();
	if(n == B.size())
	  {
	    op_to(PA,PB,n);
	  }
	else
	  {
	    return false; // Indicate operation failure
	  }
      }
    else
      {
	op_to(PA,PB,std::min(A.size(),B.size()));
      }
    return true;
  }
  // Writing operation result to third container
  template<class CT1, class CT2, class containerThree>
  static inline bool op(containerThree& C, const CT1& A, const CT2& B)
  {
    // Getting pointers to A and B
    auto PA = container_beginning(A);
    auto PB = container_beginning(B);
    auto n = A.size();
    _TMATLIB_CONST_IF_(strict)
      {
	if(n != B.size())
	  {
	    return false;
	  }
      }
    else
      {
	if(n > B.size())
	  {
	    n = B.size();
	  }
      }
    // Resizing C appropriately
    resize_container(C,get_fill_size(C,n));
    // Getting pointer to C (AFTER resize, as resize may invalidate iterators/pointers)
    auto PC = container_beginning(C);
    op(PC,PA,PB,n);
    return true;
  }


  // Operation On of two vectors with output and success indicator
  template<class CT1, class CT2, class OCT = CT1>
  static inline OCT op(const CT1& containeradded, const CT2& containeradding)
  {
    OCT result = containeradded;
    bool success = op_to(result,containeradding);
    if(!success)
      clear_container(result);
    return result;
  }
  
  // Basis-rectifying elementwise operations

  // Operation result to another container
  template<class CT1, class CT2>
  static inline bool op_to(CT1& A, const CT2& B, basis*& modbasis, const basis* rightbasis)
  {
    //TODO: add proper basis check and functionality
    bool success = op_to(A,B);
    if(!success)
      modbasis = &VECTOR_ERROR;
    return success;
  }
  template<class CT1, class CT2, class OCT = CT1>
  static inline bool op(OCT& C, const CT1& A,  const CT2& B, basis* basis1, basis* basis2, basis*& outputBasis)
  {
    //TODO: rectify basis
    bool success = op(C,A,B);
    if(!success)
      outputBasis = &VECTOR_ERROR;
    return success;
  }
  template<bool not_permitted = true, class CT1, class CT2, class OCT = CT1>
  static inline OCT op(const CT1& A, const CT2& B,  const basis* basis1, const basis* basis2, basis*& outputBasis)
  {
    //TODO: rectify basis, add proper handling for empty input (maybe)
    OCT result = op<CT1,CT2,OCT>(A,B);
    if(result.empty())
      outputBasis = &VECTOR_ERROR;
    return result;
  }
};

template<class kernel = iterator_elementwise_unary_kernel<std::negate<>>>
struct simple_unary_container_operation: public kernel
{
  using kernel::op;
  template<class CT>
  static inline void op(CT& A)
  {
    op(container_beginning(A),container_end(A));
  }
  template<class CT, class CT2>
  static inline void op(CT& O, const CT2& A)
  {
    if(O.size()<A.size())
      {
	resize_container(O,A.size());
      }
    op(container_beginning(O),container_beginning(A),container_end(A));
  }
};

template<dimi defaultStride = 1, class kernel = iterator_elementwise_scalar_operation_kernel<defaultStride,std::multiplies<>>>
struct simple_scalar_container_operation: public kernel
{
  using kernel::op;
  template<class containerOne, class scalar>
  static inline void op(containerOne& A, const scalar& B)
  {
    op(container_beginning(A),container_end(A),B,defaultStride);
  }
};

template<class operation, bool strict = true, class iterator_kernel = iterator_elementwise_binary_kernel<operation>,
	 class kernel = simple_binary_container_operation<strict,iterator_kernel>>
struct elementwise_binary: public kernel
{
  using kernel::op;
  using kernel::op_to;
  using kernel::op_range;
  using kernel::op_range_to;
};

template<class operation, class iterator_kernel = iterator_elementwise_unary_kernel<operation>, class kernel = simple_unary_container_operation<iterator_kernel>>
struct elementwise_unary: public kernel
{
  using kernel::op;
};

template<class operation, dimi defaultStride = 1, class iterator_kernel = iterator_elementwise_scalar_operation_kernel<defaultStride,operation>, class kernel = simple_scalar_container_operation<defaultStride,iterator_kernel>>
struct elementwise_scalar: public kernel
{
  using kernel::op;
};

#endif
