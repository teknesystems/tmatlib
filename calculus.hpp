// Header guard.
#ifndef _T_CALCULUS_INCLUDED_
#define _T_CALCULUS_INCLUDED_

// Includes
#include "vectors.hpp"
#include "matrix.hpp"
#include "numerics.hpp"
#include "numerical_util.hpp"
#include "symbolic/elementary_functions.hpp"
//#include "algebra.hpp"
#include <cmath>

// Definition of symbolics

class container
{
};

class function: public container
{
};

class integral: public symbolicOperator
{
};

class singleintegral: public integral
{
};

class multiintegral: public integral
{
};

class differential: public symbolicOperator
{
  std::vector<variable> respect;
};

class partialdifferential: public differential
{
};

// A few useful mathematical containers which are NOT vectors (hence not defined in vectors.hpp)
template <class elementType>
struct binomial
{
  std::vector<elementType> elements;
  void operator*=(binomial<elementType> input)
  {
    elements.reserve(elements.size() + input.elements.size());
    for(int iter = 0; iter<input.elements.size(); iter++)
      {
	elements.push_back(input.elements[iter]);
      }
  }
};

// Numerical functions

// Integration method enums
/*
enum class integration_method: char {
  right, left, trapezoid, Simons
  };*/

// TEMPORARY!

// Numerical integration
template<class functionType, class STR, class INCR>
auto numint(functionType function, STR start, INCR increment, long unsigned int steps) -> decltype(function(start))
{
  //std::cout<<"STEPS: "<<steps<<"\n";
  if(steps > 0)
    {
      auto result = increment*function(start);
      start+=increment;
      for(dimi iter = 1; iter < steps; iter++)
	{
	  //std::cout<<"INCREMENTING "<<function(start)<<"\n";
	  result+=(increment*function(start));
	  start+=increment;
	}
      return result;
    }
  else if(steps == 0)
    {
      return 0;
    }
  else
    {
      return numint(function,start,(0-increment),(0-steps));
    }
}

template<class BT1, class BT2, class INCR>
inline long int finiteIncrements(BT1 lower, BT2 upper, INCR increment)
{
  //std::cout<<"STEPS: "<<((upper-lower)/increment)<<"\n";
  return (upper-lower)/increment;
}

template<class functionType, class BT1, class BT2, class INCR, class IT = BT1>
auto numint(functionType function, BT1 lower, BT2 upper, INCR increment, bool method) -> decltype(function(lower))
{
  //std::cout<<"INTEGRATION CALLED ON "<<function.print()<<"\n";
  return numint(function,lower,increment,finiteIncrements(lower,upper,increment));
}

//End of header guard
#endif
