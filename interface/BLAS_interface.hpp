//! Interface for connecting BLAS (using a cblas header) to tmatlib.
/*!
 BLAS is not a dependency, but can be used in default functions automatically if included to improve speed. Then tmatlib will use built in functions where corresponding BLAS functions do not exist
 or do not efficiently support certain types.

 Wraps axpy to FMA and vector addition (latter can be switched off using macros), wraps GEMM to matrix multiplication (including for symmetric and hermitian matrices), wraps efficient dot product
 and inner product functions and wraps efficient norm functions from BLAS, among other things.
*/
//#include <cblas.h>

#ifdef CBLAS_H // Makes sure CBLAS is included before this wrapper is compiled
#ifndef _TMATLIB_DISABLE_BLAS_INTERFACE_ //!< In case one wants to use BLAS in their program, but does NOT want TMATLIB to include a BLAS interface. Included just in case.
#ifndef _TMATLIB_BLAS_INTERFACE_INCLUDED_ //!< Defined whenever BLAS is wrapped, used to add vector function wrapper overloads
#define _TMATLIB_BLAS_INTERFACE_INCLUDED_

namespace BLAS_interface
{
  //! Level 1 (vector-vector, vector-scalar) BLAS operations
  //! Wraps AXPY, DOT, DOTU, DOTC, NRM2, ASUM, AMAX
  namespace vector
  {
    
    //! BLAS scalar multiplication kernels in tmatlib "op" format
    struct scalar_multiplication_kernel
    {
      template<class scalarType>
      static inline void op(double* A, scalarType B, dimi n, dimi stride = 1)
      {
	cblas_dscal(n,B,A,stride);
      }
      template<class scalarType>
      static inline void op(float* A, scalarType B, dimi n, dimi stride = 1)
      {
	cblas_sscal(n,B,A,stride);
      }
      static inline void op(std::complex<double>* A, std::complex<double> B, dimi n, dimi stride = 1)
      {
	cblas_zscal(n,&B,A,stride);
      }
      static inline void op(std::complex<float>* A, std::complex<float> B, dimi n, dimi stride = 1)
      {
	cblas_cscal(n,&B,A,stride);
      }
      static inline void op(std::complex<double>* A, double B, dimi n, dimi stride = 1)
      {
	cblas_zdscal(n,B,A,stride);
      }
      static inline void op(std::complex<float>* A, float B, dimi n, dimi stride = 1)
      {
	cblas_csscal(n,B,A,stride);
      }
    };
    
    //! axpy, or vector FMA (y = ax + y)
    //! Wraps xAXPY
    //TODO: change standard FMA interface to match
    struct FMA_kernel
    {
      template<class ST>
      inline static void op(double* A, double* B, ST S, int N, int SX = 1, int SY = 1)
      {
	cblas_daxpy(N,S,A,SX,B,SY);
      }
      template<class ST>
      inline static void op(float* A, float* B, ST S, int N, int SX = 1, int SY = 1)
      {
	cblas_saxpy(N,S,A,SX,B,SY);
      }
      template<class ST>
      inline static void op(std::complex<double>* A, std::complex<double>* B, ST S, int N, int SX = 1, int SY = 1)
      {
	auto F = std::complex<double>(S);
	cblas_zaxpy(N,&F,A,SX,B,SY);
      }
      template<class ST>
      inline static void op(std::complex<float>* A, std::complex<float>* B, ST S, int N, int SX = 1, int SY = 1)
      {
	auto F = std::complex<float>(S);
	cblas_caxpy(N,&F,A,SX,B,SY);
      }
      
      template<class ST>
      inline static void op(double* A, double* AE, double* B, ST S)
      {
        op(A,B,S,AE-A);
      }
      template<class ST>
      inline static void op(float* A, float* AE, float* B, ST S)
      {
	op(A,B,S,AE-A);
      }
      template<class ST>
      inline static void op(std::complex<double>* A, std::complex<double>* AE, std::complex<double>* B, ST S)
      {
	op(A,B,S,AE-A);
      }
      template<class ST>
      inline static void op(std::complex<float>* A, std::complex<float>* AE, std::complex<float>* B, ST S)
      {
	op(A,B,S,AE-A);
      }
    };
    
    //! The inner product, NOT the simple dot product for complex numbers (e.g. taking complex conjugates)
    //! Wraps xDOT and xDOTC
    struct inner_product_kernel
    {
    };
    
    //! Uses simple dot product functions for complex numbers instead of inner product functions. Leaves inner product over real numbers unchanged (as it is equivalent to the dot product)
    //! Wraps xDOTU
    struct dot_product_kernel: public inner_product_kernel
    {
    };
    
    //! BLAS norm kernels in tmatlib "op" format. Provides both squared norm and norm. By default, tmatlib functions should be used for the vector norm, as they produce the norm directly and hence are
    //! harder to overflow.
    struct norm_kernel
    {
    };
    
  }
  //! Level 2  (vector-matrix) BLAS operations
  //! Wraps GEMV, GBMV, HEMV, HBMV, HPMV, SYMV, SBMV, SPMV, TRMV, TBMV, TPMV, TRSV, TBSV, TPSV, GER, GERU, GERC, HER, HPR, HER2, HPR2, SYR, SPR, SYR2, SPR2
  namespace vector_matrix
  {
    struct matrix_vector_multiplication_kernel
    {
      
    };
  }

  //! Level 3 (matrix-matrix) BLAS operations
  //! Wraps GEMM, SYMM, HEMM, SYRK, HERK, SYR2K, HER2K, TRMM, TRSM
  namespace matrix
  {
    struct matrix_multiplication_kernel
    {
      template<class ST1, class ST2, class ST3>
      inline static void op(double* C, double* A, double* B, ST1 m, ST2 n, ST3 p, bool dir_A, bool dir_B, bool dir_C, double alpha = 1.0, double beta = 1.0)
      {
	cblas_dgemm(dir_C?CblasRowMajor:CblasColMajor,(dir_A==dir_C)?CblasNoTrans:CblasTrans,(dir_B==dir_C)?CblasNoTrans:CblasTrans,
		    m,p,n,
		    alpha,
		    A, n,
		    B, p,
		    beta,
		    C, p);    
      }
      template<class ST1, class ST2, class ST3>
      inline static void op(float* C, float* A, float* B, ST1 m, ST2 n, ST3 p, bool dir_A, bool dir_B, bool dir_C, float alpha = 1.0, float beta = 1.0)
      {
	cblas_sgemm(dir_C?CblasRowMajor:CblasColMajor,(dir_A==dir_C)?CblasNoTrans:CblasTrans,(dir_B==dir_C)?CblasNoTrans:CblasTrans,
		    m,p,n,
		    alpha,
		    A, n,
		    B, p,
		    beta,
		    C, p);    
      }
      template<class ST1, class ST2, class ST3>
      inline static void op(std::complex<double>* C, std::complex<double>* A, std::complex<double>* B, ST1 m, ST2 n, ST3 p, bool dir_A, bool dir_B, bool dir_C,
			    std::complex<double> alpha = 1.0, std::complex<double> beta = 1.0)
      {
	cblas_zgemm(dir_C?CblasRowMajor:CblasColMajor,(dir_A==dir_C)?CblasNoTrans:CblasTrans,(dir_B==dir_C)?CblasNoTrans:CblasTrans,
		    m,p,n,
		    &alpha,
		    A, n,
		    B, p,
		    &beta,
		    C, p);    
      }
      template<class ST1, class ST2, class ST3>
      inline static void op(std::complex<float>* C, std::complex<float>* A, std::complex<float>* B, ST1 m, ST2 n, ST3 p, bool dir_A, bool dir_B, bool dir_C,
			    std::complex<float> alpha = 1.0, std::complex<float> beta = 1.0)
      {
	cblas_cgemm(dir_C?CblasRowMajor:CblasColMajor,(dir_A==dir_C)?CblasNoTrans:CblasTrans,(dir_B==dir_C)?CblasNoTrans:CblasTrans,
		    m,p,n,
		    &alpha,
		    A, n,
		    B, p,
		    &beta,
		    C, p);    
      }
    };
  }
}

#endif
#endif
#endif

