// Header guard. Skips over entire header if already included
#ifndef _T_SYMB_INCLUDED_
#define _T_SYMB_INCLUDED_

// Include statement block
#include <utility>
#include <tuple>
#include <vector>
#include <typeinfo>
#include "utility.hpp"

#ifdef _T_SYMB_SCAFFOLDING_ON_
#include <iostream>
#endif

#ifdef _T_SYMB_ENABLE_ALL_SCAFFOLDING_
#define _T_SYMB_OPER_SCAFFOLDING_
#endif

#ifdef _T_SYMB_OPER_SCAFFOLDING_
#define _T_SYMB_SCAFFOLDING_ON_
#endif

// Superclasses
// The basic symbolic container and operator
class Symbolic
{
};

// The basic unit of symbolic processing and storage
class Symbol: public Symbolic
{
};

// For wrapping and static polymorphism purposes
template <class objectType>
struct symbolicObject: public Symbolic
{
  // Useful in defining symbolic operations and other such symbolic containers.
  const objectType& get_ref() const
  {
    return static_cast<const objectType&>(*this);
  }
};

class symbolicContainer
{
};

// A set.
class Set: public symbolicContainer
{
  bool infinite; // Is the set infinite? If so, are a finite number of elements written (set size to this number OR zero, depends...) or not (set size to minimum size for adjustable set, 0 for no minimum e.g. always infinite)
  unsigned int size; // The amount of elements in the set
  //Set(bool infinite);
  //Set(unsigned int size, bool infinite);
public:
  Set();
  Set(bool input1, unsigned int input2);
};

// Set constructors
// Default constructor for sets
Set::Set()
{
  infinite = true;
  size = 0;
}

// Size/Infinite constructor for sets
Set::Set(bool input1, unsigned int input2)
{
  infinite = input1;
  size = input2;
}

struct space
{
};

// The code for an arbitrary field
class field
{
};

// The code for an arbitrary basis
class basis
{
public:
  virtual dimi dimension() {return 0;}
  //virtual space& getSpace()
  //virtual const space& getConstSpace();
  bool isvs;
  bool is_vector_space() {return isvs;}
  dimi counter; // The target vectorspace is that of which vector?
};

// Defining external bases
basis VECTOR_ERROR;


// Some predefined, useful sets
Set Reals (true, 0);
Set Complex_Numbers (true, 0);
Set Polynomials (true, 0);
Set Integers (true, 0);
Set Rational_Numbers (true, 0);
Set Functions (true, 0);
Set Differentiable_Functions (true, 0);
Set Irrational_Numbers (true, 0);
Set Square_Numbers (true, 0);
Set Negative_Numbers (true, 0);
Set Whole_Numbers (true, 0);
Set Vector_Spaces (true, 0);
Set Vectors (true, 0);
Set Vector_Space (true, 1);
Set Matrices (true, 0);
Set nxnMatrices (true, 0);
Set emptySet (false, 0);
Set Unspecified_Set_For_Type (true, 0);
Set Dynamic (true, 0);

struct symbolicProperty
{
};

template <Set* element_of_set>
struct elementOf: public symbolicProperty
{
  const Set* getSet() {return element_of_set;}
};

template<>
struct elementOf<&Dynamic>
{
  const Set& element_of_set;
  elementOf(): element_of_set(emptySet) {}
  elementOf(Set input): element_of_set(input) {}
  elementOf(Set* input): element_of_set(*input) {}
  const Set& getSet() {return element_of_set;}
};

// Generally, a set of sets. Like, for example, Rn as a WHOLE (so R0, R1, R2...)
class Superset: public Set
{
};

// Beginning of symbolics.
class variable: public Symbol
{
  Symbolic* value;
public:
  std::string name;
  // Constructors
  variable(std::string namein);
  variable(variable &input);
  variable();
  // Manipulation functions
  void arbitrary();
  Symbolic* evaluate();
  // Operators
  void operator=(variable &input);
  void operator=(Symbolic *input);
  bool operator==(variable &input); // Variable equality checker
  bool operator==(Symbolic *input); // Symbolic value checker
};

// Variable constructors

variable::variable(std::string namein) {value = NULL; name = namein;}
variable::variable(variable &variablein) {value = variablein.value; name = variablein.name;}
variable::variable() {value = NULL;}

// Variable manipulation functions
void variable::arbitrary() {value = NULL;}
Symbolic* variable::evaluate() {return value;} //TODO: work on this, potentially.


// Variable operators
// Variable to variable assignment:
void variable::operator=(variable &input) {value = input.value;}
// Variable to variable checking:
bool variable::operator==(variable &input)
{
  return true; // TEMPORARY!
}
// Symbolic to variable assignment:
void variable::operator=(Symbolic* input) {value = input;}
// Symbolic to variable checking:
bool variable::operator==(Symbolic* input) {return (value == input); /*Temporary*/}



// A symbolic operator/function. A purely abstract class
class symbolicOperator: public Symbol
{
};

struct symbolicOperation: public Symbolic
{
};

struct symbolicFunction: public symbolicOperator
{
};

// Binary operators, on operands a and b

// Basic operators
symbolicOperator exponential; // a^b
symbolicOperator add; // a + b
symbolicOperator multiply; // a * b
symbolicOperator divide; // a / b
symbolicOperator subtract; // a - b
symbolicOperator limit; // limit of a as b happens (b MUST be a condition)
symbolicFunction log_a; // log_a (b)

// Vector specific operators
symbolicOperator elementwise_addition; // <a1 + b1, a2 + b2, a3 + b3>
symbolicOperator elementwise_multiplication; // <a1 * b, a2 * b, a3 * b> OR <a1 * b1, a2 * b2, a3 * b3> (if ambiguous, use outer_product for option 1)
symbolicOperator dot_product; // a . b
symbolicOperator inner_product; // <a|b>
symbolicOperator outer_product; // a (x) b
symbolicOperator cross_product; // a x b

// Unary operators, on a

// Basic operators
symbolicOperator invert; // 1/a
symbolicFunction log_10; //log_10 (a)
symbolicFunction ln; //ln (a) (log_e (a))

// Vector specific operators

// Matrix specific operators
symbolicOperator determinant; //det(A)
symbolicOperator trace; //tr(A)
symbolicOperator rank; //rank(A)
symbolicOperator transpose; //A^T

// n-nary operators, on a1, a2, ... an

// Basic operators
extern symbolicOperator sum; // Sum of a1, a2, ... an
extern symbolicOperator setsum; // Union of/addition to set of a1, a2, ... an
extern symbolicOperator fsum; // Functional sum of a1, a2, ... an. That is, a1 operator a2 operator a3...

// Vector specific operators
extern symbolicOperator n_product;

struct staticOperation
{
};

struct identity
{
};

template <class T>
struct additiveIdentityOfType
{
  static const T standard = 0;
  static T ofobject(const T& input) {return standard;}
  static const bool multiple = false;
};

template <class T>
struct multiplicativeIdentityOfType
{
  static const T standard = 1;
  static T ofobject(const T& input) {return standard;}
  static const bool multiple = false;
};

template <class T>
static inline const T additiveIdentity(const T& input)
{
  return additiveIdentityOfType<T>::ofobject(input);
}

template<class input1, class input2>
struct binaryOperation: public symbolicObject<binaryOperation<input1,input2>>
{
  const input1& left;
  const input2& right;
  const symbolicOperator& operation;
};

template<class input1, class input2, symbolicOperator* operation>
struct staticBinaryOperation: public symbolicObject<staticBinaryOperation<input1,input2,operation>>, public staticOperation //To allow static polymorphism
{
  const input1& left;
  const input2& right;
  typedef input1 rightType;
  typedef input2 leftType;
  staticBinaryOperation(const input1& A, const input2&B): left (A), right (B)
  {
  };
  staticBinaryOperation(const staticBinaryOperation& X): left(X.left), right(X.right)
  {
  };
};

template<class input1>
struct unaryOperation: public symbolicObject<unaryOperation<input1>>
{
  const input1& operand;
  const symbolicOperator& operation;
};

template<class input1, symbolicOperator* operation>
struct staticUnaryOperation: public symbolicObject<staticUnaryOperation<input1,operation>>, public staticOperation
{
  const input1& operand;
  staticUnaryOperation(const input1& input): operand (input) {}
};

// For symbolic operations on arbitrary numbers

struct symbolicValue: public symbolicObject<symbolicValue>
{
};

struct Number: public symbolicObject<Number>, public symbolicValue
{
};

template<class dataType>
struct dataValue: public symbolicValue, public symbolicObject<dataValue<dataType>>
{
  dataType data;
  dataType& getData() {return data;}
  const dataType& getConstData() const{return data;}
  dataValue() {}
};

struct exactValue: public dataValue<std::string>, public symbolicObject<exactValue>
{
  exactValue(std::string name) {(this->data) = name;}
};

// Symbolic depth checkers

// Static, compile time checkers

// Right-side operator depth
template<class rightType, symbolicOperator* operation>
struct op_rdepth
{
  static const unsigned int num = 0;
};

template<class rightType, class leftType, symbolicOperator* operation>
struct op_rdepth<staticBinaryOperation<rightType,leftType,operation>,operation>
{
  static const unsigned int num = 1 + op_rdepth<rightType,operation>::num;
};

// Left-side operator depth
template<class leftType, symbolicOperator* operation>
struct op_ldepth
{
  static const unsigned int num = 0;
};

template<class rightType, class leftType, symbolicOperator* operation>
struct op_ldepth<staticBinaryOperation<rightType,leftType,operation>,operation>
{
  static const unsigned int num = 1 + op_rdepth<leftType,operation>::num;
};

// Get pointers to single type static operations
template<class rightType, class leftType, class targetType>
struct op_ptrs
{
  static const unsigned int num = -1;
  template<class anyType>
  inline static void get_ptrs(const targetType** ptrs, const anyType& X)
  {
    #ifdef _T_SYMB_OPER_SCAFFOLDING
    std::cout<<"END OF TREE REACHED!, FAIL";
    #endif
    /*if(typeid(examinedType)==typeid(targetType))
      {
	std::cout<<"TRUE!";
	}*/
    //ptrs[0] = reinterpret_cast<const targetType*>(&X);
  }
};

struct symbolicNullType
{
  template<class T>
  T& handle(T& input) {return input;}
};

template<class rightType, class targetType>
struct op_ptrs<targetType,rightType,targetType>
{
  static const unsigned int num = 0;
  inline static void get_ptrs(const targetType** ptrs, const targetType& X)
  {
#ifdef _T_SYMB_OPER_SCAFFOLDING_
    std::cout<<"END OF TREE REACHED!";
    #endif
    /*
    if(typeid(examinedType)==typeid(targetType))
      {
	std::cout<<"TRUE!";
	}*/
    ptrs[0] = reinterpret_cast<const targetType*>(&X);
  }
};

// This template specialization is called when op_ptrs receives a staticBinaryOperation as an argument with the *right* element being of the target type
// It will then traverse the operation tree from right to left as long as the same OPERATION is maintained and another targettype is visible to the RIGHT, returning pointers to each element along the line
template<class targetType>
struct op_ptrs<targetType,targetType,targetType>
{
  static const unsigned int num = 1 + op_ptrs<targetType,symbolicNullType,targetType>::num;
  template <class inType>
  inline static void get_ptrs(const targetType** in_ptrs, const inType& X)
  {
    #ifdef _T_SYMB_OPER_SCAFFOLDING_
    std::cout<<"RIGHT ASSIGNED! DOUBLE MATCH!";
    #endif
    op_ptrs<targetType,symbolicNullType,targetType>::get_ptrs(in_ptrs, X.left);
    in_ptrs[num] = reinterpret_cast<const targetType*>(&X.right);
  }
};

// This template specialization is called when op_ptrs receives a staticBinaryOperation as an argument with the *right* element being of the target type
// It will then traverse the operation tree from right to left as long as the same OPERATION is maintained and another targettype is visible to the RIGHT, returning pointers to each element along the line
template<class leftType, class targetType>
struct op_ptrs<leftType,targetType,targetType>
{
  static const unsigned int num = 1 + op_ptrs<typename leftType::leftType,typename leftType::rightType,targetType>::num;
  template <class inType>
  inline static void get_ptrs(const targetType** in_ptrs, const inType& X)
  {
#ifdef _T_SYMB_OPER_SCAFFOLDING_
    std::cout<<"RIGHT ASSIGNED!";
    #endif
    op_ptrs<typename leftType::leftType,typename leftType::rightType,targetType>::get_ptrs(in_ptrs, X.left);
    in_ptrs[num] = reinterpret_cast<const targetType*>(&X.right);
  }
};

/*
// This template specialization is the same as above, but INVERTED (e.g. if the LEFT type is being searched for, traverses only towards the RIGHT, returns pointers)
template<class rightType, class leftType, symbolicOperation* operation>
struct op_ptrs<staticBinaryOperation<rightType,leftType,operation>,leftType>
{
  static const unsigned int num = 1 + op_ptrs<rightType,leftType>::num;
  inline static void get_ptrs(const leftType** in_ptrs, const staticBinaryOperation<rightType,leftType,operation>& X)
  {
    std::cout<<"LEFT ASSIGNED!";
    op_ptrs<rightType,leftType>::get_ptrs(in_ptrs, X.right);
    in_ptrs[num] = reinterpret_cast<const leftType*>(&X.left);
  }
};
*/

// Symbolic operations
// Using classes from symbolics.hpp and templates to create optimized, semi-symbolic and symbolic operations.

// Semi-symbolic addition:
template<class rightType, class leftType>
const staticBinaryOperation<rightType,leftType,&add> operator+ (const symbolicObject<rightType>& right, const symbolicObject<leftType>& left)
{
  return staticBinaryOperation<rightType,leftType,&add> (right.get_ref(), left.get_ref());
}

// Semi-symbolic multiplication:
template<class rightType, class leftType>
const staticBinaryOperation<rightType,leftType,&multiply> operator* (const symbolicObject<rightType>& right, const symbolicObject<leftType>& left)
{
  return staticBinaryOperation<rightType,leftType,&multiply> (right.get_ref(), left.get_ref());
}

// Semi-symbolic division:
template<class rightType, class leftType>
const staticBinaryOperation<rightType,leftType,&divide> operator/ (const symbolicObject<rightType>& right, const symbolicObject<leftType>& left)
{
  return staticBinaryOperation<rightType,leftType,&multiply> (right.get_ref(), left.get_ref());
}

// Semi-symbolic subtraction:
template<class rightType, class leftType>
const staticBinaryOperation<rightType,leftType,&subtract> operator- (const symbolicObject<rightType>& right, const symbolicObject<leftType>& left)
{
  return staticBinaryOperation<rightType,leftType,&multiply> (right.get_ref(), left.get_ref());
}

// Adjustable, semi-symbolic functions:
template<class inputType>
const staticUnaryOperation<inputType,&determinant> det(const symbolicObject<inputType>& input)
{
  return staticUnaryOperation<inputType,&determinant> (input.get_ref());
}



// Array and container handlers:

struct arrayHandler
{
};

// Converts the first element of an array to the first dimension, etc...
// In printing, iff there are less than 4 dimensions, represents the first four dimensions as x,y,z,t
// Else, represents dimension n as tn
struct arrayHandler_elemToDim: public arrayHandler
{
};

#endif
