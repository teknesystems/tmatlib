// Generalized kernel for an elementwise unary operation, e.g. negation

#ifndef _TMATLIB_KERNEL_ELEM_UNARY_OP_
#define _TMATLIB_KERNEL_ELEM_UNARY_OP_

template<class functor = std::negate<>, dimi defaultStride = 1>
struct iterator_elementwise_unary_kernel
{
  template<class OIT, class IT>
  static inline void op(OIT O, IT A, IT AE, const dimi stride = defaultStride, const dimi resultStride = defaultStride, functor f = functor())
  {
    if(stride==1)
      {
	while(A!=AE)
	  {
	    *O = f(*A);
	    std::advance(A,1);
	    std::advance(O,resultStride);
	  }
      }
    else
      {
	while(A<AE)
	  {
	    *O = f(*A);
	    std::advance(A,stride);
	  }
      }
  }
  template<class IT>
  static inline void op(IT A, IT AE, const dimi stride = defaultStride, functor f = functor())
  {
    if(stride==1)
      {
	while(A!=AE)
	  {
	    *A = f(*A);
	    std::advance(A,1);
	  }
      }
    else
      {
	while(A<AE)
	  {
	    *A = f(*A);
	    std::advance(A,stride);
	  }
      }
  }
  template<class IT>
  static inline void op(IT A, dimi n, const dimi stride = defaultStride)
  {
    op(A,std::next(A,n),stride);
  }
  template<class OIT, class IT>
  static inline void op(OIT O, IT A, dimi n, const dimi stride = defaultStride)
  {
    op(O,A,std::next(A,n),stride);
  }
};

#endif
