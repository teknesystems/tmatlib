// Generalized elementwise scalar-vector operation kernel on iterators

#ifndef _TMATLIB_KERNEL_ELEM_SCALAR_OP_
#define _TMATLIB_KERNEL_ELEM_SCALAR_OP_

template<dimi defaultStride = 1, class functor = std::multiplies<>, class assignment_functor = binary_assignment<functor>>
struct iterator_elementwise_scalar_operation_kernel
{
  // STL style (iterator, end iterator)
  template<class iter1, class scalar>
  static inline void op(iter1 B, iter1 E, const scalar& S, const dimi stride = defaultStride, assignment_functor assign = assignment_functor())
  {
    while(B < E)
      {
	assign((*B),S);
	std::advance(B,stride);
      }
  }
  
  // Size style
  template<class iter1, class scalar>
  static inline void op(iter1 A, const scalar& B, const dimi n, const dimi stride = defaultStride, assignment_functor assign = assignment_functor())
  {
    for(dimi iter = 0; iter < n; iter++)
      {
	assign((*A),B);
	std::advance(A,stride);
      }
  }
};

#endif
