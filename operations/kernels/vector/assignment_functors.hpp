// Assignment functors corresponding to operation functors (e.g. += corresponding to +), generated automatically for any operation functor and with special overloads for some functors

// Header guard. Skips over entire header if already included
#ifndef _TMATLIB_ASSIG_FUNCT_
#define _TMATLIB_ASSIG_FUNCT_

template<class binaryFunctor>
struct binary_assignment
{
  template<class IT1, class IT2>
  inline IT1& operator() (IT1& A, IT2& B, binaryFunctor f = binaryFunctor()) {A = f(A,B); return A;}
};

//TODO: check if +=, -=, *=, /= are defined
template<>
struct binary_assignment<std::plus<>>
{
  template<class IT1, class IT2>
  inline IT1& operator() (IT1& A, const IT2& B) {A+=B; return A;}
};
template<>
struct binary_assignment<std::minus<>>
{
  template<class IT1, class IT2>
  inline IT1& operator() (IT1& A, const IT2& B) {A-=B; return A;}
};
template<>
struct binary_assignment<std::multiplies<>>
{
  template<class IT1, class IT2>
  inline IT1& operator() (IT1& A, const IT2& B) {A*=B; return A;}
};
template<>
struct binary_assignment<std::divides<>>
{
  template<class IT1, class IT2>
  inline IT1& operator() (IT1& A, const IT2& B) {A/=B; return A;}
};
template<>
struct binary_assignment<void>
{
  template<class IT1, class IT2>
  inline IT1& operator() (IT1& A, const IT2& B) {A=B; return A;}
};

#endif
