// Boilerplate wrapper for arbitrary elementwise binary operation kernels to a name (e.g. add/add_to/add_range_to/add_range, subtract/subtract_from/subtract_range_from/subtract_range, etc.)
// Can be #include'd multiple times for different names or the same name in different classes.

// Names which must be defined before call: _TMATLIB_OPERATION_NAME_, _TMATLIB_KERNEL_NAME_, _TMATLIB_RANGE_ASSIGNMENT_NAME_
// _TMATLIB_ASSIGNMENT_NAME_ (defaults to "to"), _TMATLIB_RANGE_NAME_ (defaults to "range"), _TMATLIB_STRIDE_PARAM_ (defaults to 1, can be a variable, macro or template e.g. defaultStride)
// So "subtract_range_from" is really _TMATLIB_OPERATION_NAME_ ## _ ## _TMATLIB_RANGE_NAME_ ## _ ## _TMATLIB_ASSIGNMENT_NAME_

// Configuration macros (optional, defaults to not defined. Undefined at the end if _TMATLIB_DONT_UNDEFINE_ is defined): _TMATLIB_DONT_WRAP_STRIDED_ (if defined, strided operations are not wrapped)
// Define the above if the operation wrapped does not support striding

// Names are undefined automatically unless _TMATLIB_DONT_UNDEFINE_ is defined. If this is the case, the names should be undefined manually

// If _TMATLIB_ASSIGNMENT_NAME_ is unset, it defaults to "to". If _TMATLIB_RANGE_NAME_ is unset, it defaults to "range"

// Automatically setting _TMATLIB_STRIDE_PARAM_ to 1 (no stride) if undefined
#ifndef _TMATLIB_STRIDE_PARAM_
#define _TMATLIB_STRIDE_PARAM_ 1
#endif

// Actual wrappers:

template<class IT1, class IT2>
inline static void _TMATLIB_RANGE_ASSIGNMENT_NAME_ (IT1 A, const IT1 AEND, IT2 B)
{
  _TMATLIB_KERNEL_NAME_ :: op_range_to(A,AEND,B);
}

#ifndef _TMATLIB_DONT_WRAP_STRIDED_
template<class IT1, class IT2>
inline static void _TMATLIB_RANGE_ASSIGNMENT_NAME_ (IT1 A, const IT1 AEND, IT2 B, const dimi SA, const dimi SB = _TMATLIB_STRIDE_PARAM_ )
{
  _TMATLIB_KERNEL_NAME_ :: op_range_to(A,AEND,B,SA,SB);
}
#endif

template<class IT1, class IT2>
inline static void  _TMATLIB_ASSIGNMENT_NAME_ (const IT1 A, const IT2 B, const dimi n)
{
  _TMATLIB_KERNEL_NAME_ :: op_to(A,B,n);
}

#ifndef _TMATLIB_DONT_WRAP_STRIDED_
template<class IT1, class IT2>
inline static void  _TMATLIB_ASSIGNMENT_NAME_ (const IT1 A, const IT2 B, const dimi n, const dimi SA, const dimi SB = _TMATLIB_STRIDE_PARAM_ )
{
  _TMATLIB_KERNEL_NAME_ :: op_to(A,B,n,SA,SB);
}
#endif

template<class OIT, class IT1, class IT2>
inline static void  _TMATLIB_RANGE_NAME_ (const OIT C, const IT1 A, const IT1 AE, const IT2 B)
{
  _TMATLIB_KERNEL_NAME_ :: op_range(C,A,AE,B);
}

#ifndef _TMATLIB_DONT_WRAP_STRIDED_
template<class OIT, class IT1, class IT2>
inline static void  _TMATLIB_RANGE_NAME_ (OIT C, IT1 A, const IT1 AE, IT2 B, const dimi SA, const dimi SB = _TMATLIB_STRIDE_PARAM_ , const dimi SC = _TMATLIB_STRIDE_PARAM_ )
{
  _TMATLIB_KERNEL_NAME_ :: op_range(C,A,AE,B,SA,SB,SC);
}
#endif

template<class OIT, class IT1, class IT2>
inline static void _TMATLIB_OPERATION_NAME_ (const OIT C, const IT1 A, const IT2 B, const dimi n)
{
  _TMATLIB_KERNEL_NAME_ :: op(C,A,B,n);
}

#ifndef _TMATLIB_DONT_WRAP_STRIDED_
template<class OIT, class IT1, class IT2>
inline static void TMATLIB_OPERATION_NAME_ (const OIT C, const IT1 A, const IT2 B, const dimi n, const dimi SA, const dimi SB = _TMATLIB_STRIDE_PARAM_ , const dimi SC = _TMATLIB_STRIDE_PARAM_ )
{
  _TMATLIB_KERNEL_NAME_ :: op(C,A,B,n,SA,SB,SC);
}
#endif


// These macros are undefined by default, but may not be (e.g #include'ing this file, then another, for wrapping a macro a certain way, without needing to redefine)

#ifndef _TMATLIB_DONT_UNDEFINE_
#undef _TMATLIB_OPERATION_NAME_
#undef _TMATLIB_KERNEL_NAME_
#undef _TMATLIB_RANGE_NAME_
#undef _TMATLIB_ASSIGNMENT_NAME_
#undef _TMATLIB_RANGE_ASSIGNMENT_NAME_
#undef _TMATLIB_STRIDE_PARAM_
#undef _TMATLIB_DONT_WRAP_STRIDED_
#endif
