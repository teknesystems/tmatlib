// Wrapper for binary operations on containers of the form Z = X op Y (returning or editing a reference to Z) and X = X op Y (or Y op X).

// Names which must be defined: _TMATLIB_OPERATION_NAME_ and _TMATLIB_ASSIGNMENT_NAME_ for the operations, and _TMATLIB_KERNEL_NAME_ for the source kernels
// Configuration: _TMATLIB_ADDITIONAL_DATA_ and _TMATLIB_ADDITIONAL_ASSGN_DATA, if defined with data, create a SECOND wrapper for each function with its value as data at the end.
// Functors can be part of this, if desired. _TMATLIB_ADDITIONAL_PASS_ and _TMATLIB_ADDTIONAL_ASSGN_PASS_ are passed into the functors
// If the assignment values are not defined, but normal values are, then the assignment values are automatically set to the normal values.
// _TMATLIB_WRAP_NO_ASSIGNMENT_ removes the X = X op Y wrapper and _TMATLIB_WRAP_NO_RETURN_ removes the X op Y return Z form

#ifndef _TMATLIB_ADDITIONAL_ASSGN_DATA_
#define _TMATLIB_ADDITIONAL_ASSGN_DATA_ _TMATLIB_ADDITIONAL_DATA_
#endif

#ifndef _TMATLIB_ADDITIONAL_ASSGN_PASS_
#define _TMATLIB_ADDITIONAL_ASSGN_PASS_ _TMATLIB_ADDITIONAL_PASS__
#endif

#ifndef _TMATLIB_WRAP_NO_ASSIGNMENT_
template<class CT1, class CT2>
inline static bool _TMATLIB_ASSIGNMENT_NAME_ (CT1& A, const CT2& B)
{
  return _TMATLIB_KERNEL_NAME_ :: op_to(A,B);
}
#endif

template<class CT1, class CT2, class containerThree>
static inline bool _TMATLIB_OPERATION_NAME_ (containerThree& C, const CT1& A, const CT2& B)
{
  return _TMATLIB_KERNEL_NAME_ :: op(C,A,B);
}

#ifndef _TMATLIB_WRAP_NO_RETURN_
template<class CT1, class CT2, class OCT = CT1>
static inline OCT _TMATLIB_OPERATION_NAME_ (const CT1& A, const CT2& B)
{
  return _TMATLIB_KERNEL_NAME_ :: op(A,B);
}
#endif

#ifdef _TMATLIB_ADDITIONAL_DATA_
#ifndef _TMATLIB_WRAP_NO_ASSIGNMENT_
template<class CT1, class CT2>
static inline bool _TMATLIB_ASSIGNMENT_NAME_ (CT1& A, const CT2& B, _TMATLIB_ADDITIONAL_ASSGN_DATA_ )
{
  return _TMATLIB_KERNEL_NAME_ :: op_to(A,B, _TMATLIB_ADDITIONAL_ASSGN_PASS_ );
}
#endif

template<class CT1, class CT2, class OCT = CT1>
static inline bool _TMATLIB_OPERATION_NAME_ (OCT& C, const CT1& A,  const CT2& B, _TMATLIB_ADDITIONAL_DATA_ )
{
  return _TMATLIB_KERNEL_NAME_ :: op(C,A,B, _TMATLIB_ADDITIONAL_PASS_ );
}

#ifndef _TMATLIB_WRAP_NO_RETURN_
template<bool not_permitted = true, class CT1, class CT2, class OCT = CT1>
static inline OCT _TMATLIB_OPERATION_NAME_ (const CT1& A, const CT2& B,  _TMATLIB_ADDITIONAL_DATA_)
{
  return _TMATLIB_KERNEL_NAME_ :: op(A,B, _TMATLIB_ADDITIONAL_PASS_ );
}
#endif

#endif
  
#ifndef _TMATLIB_DONT_UNDEFINE_
// Names:
#undef _TMATLIB_OPERATION_NAME_
#undef _TMATLIB_ASSIGNMENT_NAME_
#undef _TMATLIB_KERNEL_NAME_
// Configuration:
#undef _TMATLIB_ADDITIONAL_DATA_
#undef _TMATLIB_WRAP_NO_RETURN_
#undef _TMATLIB_WRAP_NO_ASSIGNMENT_
#endif
